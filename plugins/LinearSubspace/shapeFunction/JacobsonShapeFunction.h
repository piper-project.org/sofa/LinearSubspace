/******************************************************************************
*       SOFA, Simulation Open-Framework Architecture, development version     *
*                (c) 2006-2017 INRIA, USTL, UJF, CNRS, MGH                    *
*                                                                             *
* This program is free software; you can redistribute it and/or modify it     *
* under the terms of the GNU Lesser General Public License as published by    *
* the Free Software Foundation; either version 2.1 of the License, or (at     *
* your option) any later version.                                             *
*                                                                             *
* This program is distributed in the hope that it will be useful, but WITHOUT *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License *
* for more details.                                                           *
*                                                                             *
* You should have received a copy of the GNU Lesser General Public License    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.        *
*******************************************************************************
* Authors: The SOFA Team and external contributors (see Authors.txt)          *
*                                                                             *
* Contact information: contact@sofa-framework.org                             *
******************************************************************************/
#ifndef LINEARSUBSPACE_JacobsonShapeFunction_H
#define LINEARSUBSPACE_JacobsonShapeFunction_H

#include "LinearSubspace/config.h"
#include <Flexible/shapeFunction/BaseShapeFunction.h>

#include <sofa/core/topology/BaseMeshTopology.h>

#include <sofa/helper/rmath.h>
#include <sofa/helper/kdTree.h>
#include <sofa/helper/OptionsGroup.h>

#include <Eigen/Core>
#include <Eigen/Dense>

#include <limits>
#include <algorithm>

#define LINEARSUBSPACE_JacobsonShapeFunction_VERBOSE false


namespace Eigen {

template <typename RealT>
std::istream& operator>>( std::istream& in, Eigen::Matrix<RealT, Eigen::Dynamic, Eigen::Dynamic>& /*c*/ )
{
    msg_warning("Eigen::Matrix<RealT, Eigen::Dynamic, Eigen::Dynamic>>") << "Eigen::Matrix<RealT, Eigen::Dynamic, Eigen::Dynamic> is not yet readable";
    std::string bidon;
    in >> bidon;
    return in;
}
}

namespace sofa
{
namespace component
{
namespace shapefunction
{


template <class TLSShapeFunctionTypes>
class JacobsonShapeFunction : public core::behavior::BaseShapeFunction<TLSShapeFunctionTypes>
//class JacobsonShapeFunction : public core::behavior::BaseLSShapeFunction<TLSShapeFunctionTypes>
{
public:
    SOFA_CLASS(SOFA_TEMPLATE(JacobsonShapeFunction, TLSShapeFunctionTypes) , SOFA_TEMPLATE(core::behavior::BaseShapeFunction, TLSShapeFunctionTypes));
    typedef core::behavior::BaseShapeFunction<TLSShapeFunctionTypes> Inherit;

//    SOFA_CLASS(SOFA_TEMPLATE(JacobsonShapeFunction, TLSShapeFunctionTypes) , SOFA_TEMPLATE(core::behavior::BaseLSShapeFunction, TLSShapeFunctionTypes));
//    typedef core::behavior::BaseLSShapeFunction<TLSShapeFunctionTypes> Inherit;

    typedef typename Inherit::Real Real;
    typedef typename Inherit::VReal VReal;

    typedef typename Inherit::Coord Coord;
    typedef typename Inherit::VCoord VCoord;
    enum {spatial_dimensions=Inherit::spatial_dimensions};

    typedef uint Ref;
    typedef typename Inherit::VRef VRef;

    typedef typename Inherit::Gradient Gradient;
    typedef typename Inherit::VGradient VGradient;
    typedef typename Inherit::VecVGradient VecVGradient;

    typedef typename Inherit::Hessian Hessian;
    typedef typename Inherit::VHessian VHessian;
    typedef typename Inherit::VecVHessian VecVHessian;


    typedef typename Inherit::Cell Cell;



    //    typedef JacobsonShapeFunctionInternalData<TLSShapeFunctionTypes> InternalData;

    typedef sofa::defaulttype::Vec<2, Real> Vec2;

    //    typedef helper::vector<double> ParamTypes;
    //    typedef helper::ReadAccessor<Data< ParamTypes > > raParam;


    typedef sofa::helper::kdTree<Coord> kdTree;
    typedef typename kdTree::distanceToPoint distanceToPoint;
    typedef typename kdTree::distanceSet distanceSet;

    kdTree m_kdt;


    typedef Eigen::Matrix<Real, Eigen::Dynamic, Eigen::Dynamic> DsMat;
    typedef Eigen::Matrix<Real, 4, 4> Mat4;
    typedef Eigen::Matrix<Real, 3, 3> Mat3;

    Data<DsMat> d_W;
//    Data<VCoord> d_position;

    typedef core::topology::BaseMeshTopology::Ptr TopoPtr;
    TopoPtr m_topo;




    virtual void computeShapeFunction(const Coord& childPosition, VRef& ref, VReal& w, VGradient* dw=NULL,VHessian* ddw=NULL, const Cell cell=-1)
    {
        helper::ReadAccessor<Data<DsMat > > W_accessor(d_W);
        const DsMat& W = W_accessor.ref();

        uint nbParentInW = W.cols();

        VReal meshWeights;
        helper::vector<uint> meshParent;
        computeMeshCoordinates(childPosition, meshWeights, meshParent);

        VReal meshWeightsDx, meshWeightsDy, meshWeightsDz;
        helper::vector<uint> meshParentDx, meshParentDy, meshParentDz;
        Real dx = 0.001;
        Coord posDx = childPosition + Coord(dx, 0, 0);
        Coord posDy = childPosition + Coord(0, dx, 0);
        Coord posDz = childPosition + Coord(0, 0, dx);

        if(dw)
        {

//            std::cout << " Computing jacobson shape function with gradients !" << std::endl;
            (*dw).resize(nbParentInW);

            computeMeshCoordinates(posDx, meshWeightsDx, meshParentDx);
            computeMeshCoordinates(posDy, meshWeightsDy, meshParentDy);
            computeMeshCoordinates(posDz, meshWeightsDz, meshParentDz);
        }

        w.resize(nbParentInW);
        ref.resize(nbParentInW);
        for(uint i=0; i<nbParentInW; ++i)
        {
            ref[i] = i;
            w[i] = 0;


            for(uint j=0; j<meshParent.size(); ++j)
            {
                w[i] += meshWeights[j]*W(meshParent[j], i);
            }


            if(dw)
            {
                (*dw)[i][0] = 0;
                for(uint j=0; j<meshParentDx.size(); ++j)
                {
                    (*dw)[i][0] += meshWeightsDx[j]*W(meshParentDx[j], i);
                }
                (*dw)[i][0] = ((*dw)[i][0] - w[i])/dx;

                (*dw)[i][1] = 0;
                for(uint j=0; j<meshParentDy.size(); ++j)
                {
                    (*dw)[i][1] += meshWeightsDy[j]*W(meshParentDy[j], i);
                }
                (*dw)[i][1] = ((*dw)[i][1] - w[i])/dx;

                (*dw)[i][2] = 0;
                for(uint j=0; j<meshParentDz.size(); ++j)
                {
                    (*dw)[i][2] += meshWeightsDz[j]*W(meshParentDz[j], i);
                }
                (*dw)[i][2] = ((*dw)[i][2] - w[i])/dx;

//                std::cout << "computing gradients !" << std::endl;

//                (*dw)[i][0] = 0;
//                (*dw)[i][1] = 0;
//                (*dw)[i][2] = 0;
            }
        }



        uint nbParentMax = this->f_nbRef.getValue();
        uint nbParent = std::min(nbParentInW, nbParentMax);
        if(nbParent < nbParentInW)
        {

            std::sort(ref.begin(), ref.end(),
                      [&w](const int& a, const int& b) {
                return (w[a] > w[b]);
            }
            );

            ref.resize(nbParent);

            VReal w_temp(nbParent);
            VGradient dw_temp;
            if(dw)
            {
                dw_temp.resize(nbParent);
            }

            Real wSum=0.0;
            for(uint i=0; i<nbParent; ++i)
            {
                w_temp[i] = w[ref[i]];
                if(dw)
                {
                    dw_temp[i] = (*dw)[ref[i]];
                }
                wSum += w_temp[i];
            }

            for(uint i=0; i<nbParent; ++i)
            {
                w_temp[i] /= wSum;
                if(dw)
                {
                    dw_temp[i] /= wSum;
                }
            }

            w = w_temp;
            if(dw)
            {
                *dw = dw_temp;
            }
        }

//        this->normalize(w, dw, ddw);
        this->normalize(w, dw, nullptr);
    }


    void computeMeshCoordinates(const Coord& targetPosition, VReal& w, helper::vector<uint>& vertID)
    {
        if(LINEARSUBSPACE_JacobsonShapeFunction_VERBOSE)
            std::cout << "Computing mesh coordinates at location p=" << targetPosition << std::endl;

        /// retrieving mesh vertices positions
        helper::ReadAccessor<Data<VCoord > > position_accessor(this->f_position);
        VCoord position = position_accessor.ref();


        if(LINEARSUBSPACE_JacobsonShapeFunction_VERBOSE)
            std::cout << "  Looking for closest vertex..." << std::endl;

        uint closestVertID = m_kdt.getClosest(targetPosition, position);

        if(LINEARSUBSPACE_JacobsonShapeFunction_VERBOSE)
        {
            std::cout << "    ... found V#" << closestVertID << std::endl;
            std::cout << "  Looking for enclosing tetrahedron..." << std::endl;
        }

        uint closestTetID;
        bool found_tetra = false;
        core::topology::BaseMeshTopology::TetrahedraAroundVertex tets= m_topo->getTetrahedraAroundVertex(closestVertID);
        VReal w_temp_min(4);

        for(uint i=0; i<tets.size(); ++i)
        {
            VReal w_temp(4);
            computeBarycentricCoordinates(targetPosition, position[m_topo->getTetra(tets[i])[0]], position[m_topo->getTetra(tets[i])[1]], position[m_topo->getTetra(tets[i])[2]], position[m_topo->getTetra(tets[i])[3]], w_temp[0], w_temp[1], w_temp[2], w_temp[3]);
            if(w_temp[0] > 0 && w_temp[1] > 0 && w_temp[2] > 0 && w_temp[3] > 0)
            {
                closestTetID = tets[i];
                found_tetra = true;
                w_temp_min = w_temp;
                break;
            }
        }
        if(found_tetra)
        {
            if(LINEARSUBSPACE_JacobsonShapeFunction_VERBOSE)
            {
                std::cout << "    ...found Tet#" << closestTetID << std::endl;
                std::cout << "    barycentric coordinates = " << w_temp_min[0] << ", " << w_temp_min[1] << ", " << w_temp_min[2] << ", " << w_temp_min[3] << std::endl;
            }

            w.resize(4);
            vertID.resize(4);

            for(uint j=0; j<4; ++j)
            {
                w[j] = w_temp_min[j];
                vertID[j] += m_topo->getTetra(closestTetID)[j];
            }

            if(LINEARSUBSPACE_JacobsonShapeFunction_VERBOSE)
                std::cout << "Tetrahedron case : SUCCESS." << std::endl;

            return;
        }


        /// targetPosition is outside the tetrahedral mesh

        if(LINEARSUBSPACE_JacobsonShapeFunction_VERBOSE)
        {
            std::cout << "    ...no enclosing tetrahedron found." << std::endl;
            std::cout << "  Looking for closest triangle..." << std::endl;
        }

        uint closestTriID;
        Real minTriDist = std::numeric_limits<Real>::infinity();
        Real minBaryCoord[3];
        bool found_tri = false;
        core::topology::BaseMeshTopology::TetrahedraAroundVertex tri= m_topo->getTrianglesAroundVertex(closestVertID);
        for(uint i=0; i<tri.size(); ++i)
        {
            /// checking that tri[i] is on the border
            if(m_topo->getTetrahedraAroundTriangle(tri[i]).size() != 1)
            {
                continue;
            }

            uint v0 = m_topo->getTriangle(tri[i])[0];
            uint v1 = m_topo->getTriangle(tri[i])[1];
            uint v2 = m_topo->getTriangle(tri[i])[2];

            Coord p0 = position[v0];
            Coord p1 = position[v1];
            Coord p2 = position[v2];


            Real baryCoord[3];
            computeBarycentricCoordinates(targetPosition, p0, p1, p2, baryCoord[0], baryCoord[1], baryCoord[2]);

            if(baryCoord[0] > 0 && baryCoord[1] > 0 && baryCoord[2] > 0)
            {
                Coord targetPositionProjected= p0 * baryCoord[0]+ p1 * baryCoord[1] + p2 * baryCoord[2];
                Real dist = (targetPosition - targetPositionProjected).norm();
                if(dist < minTriDist)
                {
                    found_tri = true;
                    minTriDist = dist;
                    closestTriID = tri[i];
                    minBaryCoord[0] = baryCoord[0];
                    minBaryCoord[1] = baryCoord[1];
                    minBaryCoord[2] = baryCoord[2];
                }
            }
        }

        if(found_tri)
        {

            if(LINEARSUBSPACE_JacobsonShapeFunction_VERBOSE)
            {
                std::cout << "    ...found closest triangle : Tri#" << closestTriID << std::endl;
                std::cout << "    barycentric coordinates = " << minBaryCoord[0] << ", " << minBaryCoord[1] << ", " << minBaryCoord[2] << std::endl;
            }

            w.resize(3);
            vertID.resize(3);
            for(uint j=0; j<3; ++j)
            {
                w[j] = minBaryCoord[j];
                vertID[j] = m_topo->getTriangle(closestTriID)[j];
            }

            if(LINEARSUBSPACE_JacobsonShapeFunction_VERBOSE)
            {
                std::cout << "Triangle case : SUCCESS." << std::endl;
            }
            return;
        }



        if(LINEARSUBSPACE_JacobsonShapeFunction_VERBOSE)
        {
            std::cout << "    ...no closest triangle found" << std::endl;
            std::cout << "  Looking for closest edge..." << std::endl;
        }

        uint closestEdgeID;
        Real minEdgeDist = std::numeric_limits<Real>::infinity();
        Real minAlpha = 0.0;
        bool found_edge = false;
        core::topology::BaseMeshTopology::EdgesAroundVertex edges= m_topo->getEdgesAroundVertex(closestVertID);
        for(uint i=0; i<edges.size(); ++i)
        {
            if(!isEdgeAtBorder(edges[i]))
            {
                continue;
            }

            Coord p0 = position[m_topo->getEdge(edges[i])[0]];
            Coord p1 = position[m_topo->getEdge(edges[i])[1]];

            Real alpha = (targetPosition-p0)*(p1-p0) / (p1-p0).norm2();

            if(alpha > 0.0 && 1.0-alpha > 0.0)
            {
                Coord targetPositionProjected = p0 * (1.0-alpha) + p1*alpha;
                Real dist = (targetPosition - targetPositionProjected).norm();
                if(dist < minEdgeDist)
                {
                    found_edge = true;
                    minEdgeDist = dist;
                    closestEdgeID = edges[i];
                    minAlpha = alpha;
                }
            }
        }
        if(found_edge)
        {

            if(LINEARSUBSPACE_JacobsonShapeFunction_VERBOSE)
            {
                std::cout << "    ...found closest edge : E#" << closestEdgeID << std::endl;
                std::cout << "    barycentric coordinates = " << 1.0-minAlpha << ", " << minAlpha << std::endl;
            }

            w.resize(2);
            w[0] = 1.0-minAlpha;
            w[1] = minAlpha;

            vertID.resize(2);
            vertID[0] = m_topo->getEdge(closestEdgeID)[0];
            vertID[1] = m_topo->getEdge(closestEdgeID)[1];

            if(LINEARSUBSPACE_JacobsonShapeFunction_VERBOSE)
            {
                std::cout << "Edge case : SUCCESS." << std::endl;
            }
            return;
        }

        if(LINEARSUBSPACE_JacobsonShapeFunction_VERBOSE)
        {
            std::cout << "    ...no closest edge found" << std::endl;
            std::cout << "  Falling back on closest point..." << std::endl;
        }

        w.resize(1);
        w[0] = 1.0;

        vertID.resize(1);
        vertID[0] = closestVertID;

        if(LINEARSUBSPACE_JacobsonShapeFunction_VERBOSE)
        {
            std::cout << "Point case : SUCCESS." << std::endl;
        }
        return;
    }



    /// inspired from http://steve.hollasch.net/cgindex/geometry/ptintet.html
    bool isPointInsideTetra(const Coord& p, const Coord& p0, const Coord& p1, const Coord& p2, const Coord& p3)
    {
        Real c0, c1, c2, c3;
        computeBarycentricCoordinates(p, p0, p1, p2, p3, c0, c1, c2, c3);

        /// checking signs
        return c0 > 0 && c1 > 0 && c2 > 0 && c3 > 0;
    }

    /// Computes barycentric coordinates (c0, c1, c2, c3) of p inside tetrahedron (p0, p1, p2, p3)
    void computeBarycentricCoordinates(const Coord& p, const Coord& p0, const Coord& p1, const Coord& p2, const Coord& p3, Real& c0, Real& c1, Real& c2, Real& c3)
    {
        Real d =tetraDeterminant(p0, p1, p2, p3);
        Real d0=tetraDeterminant(p , p1, p2, p3);
        Real d1=tetraDeterminant(p0, p , p2, p3);
        Real d2=tetraDeterminant(p0, p1, p , p3);
        Real d3=tetraDeterminant(p0, p1, p2, p );

        c0 = d0/d;
        c1 = d1/d;
        c2 = d2/d;
        c3 = d3/d;
    }

    /// Computes barycentric coordinates (c0, c1, c2) of p inside triangle (p0, p1, p2)
    void computeBarycentricCoordinates(const Vec2& p, const Vec2& p0, const Vec2& p1, const Vec2& p2, Real& c0, Real& c1, Real& c2)
    {
        Real d =triDeterminant(p0, p1, p2);
        Real d0=triDeterminant(p , p1, p2);
        Real d1=triDeterminant(p0, p , p2);
        Real d2=triDeterminant(p0, p1, p );

        c0 = d0/d;
        c1 = d1/d;
        c2 = d2/d;
    }

    /// see http://math.stackexchange.com/questions/544946/determine-if-projection-of-3d-point-onto-plane-is-within-a-triangle
    void computeBarycentricCoordinates(const Coord& p, const Coord& p0, const Coord& p1, const Coord& p2, Real& c0, Real& c1, Real& c2)
    {
        Coord u = p1-p0;
        Coord v = p2-p0;
        Coord w = p-p0;
        Coord n = u.cross(v);

        Real n2 = n.norm2();

        c1=((w.cross(v)) * n)/n2;
        c2=((u.cross(w)) * n)/n2;
        c0=1-c1-c2;
    }

    /// signed volume computation
    Real tetraDeterminant(const Coord& p0, const Coord& p1, const Coord& p2, const Coord& p3)
    {
        Mat4 m;
        m  << p0[0] , p0[1] , p0[2] , 1.0
                , p1[0] , p1[1] , p1[2] , 1.0
                , p2[0] , p2[1] , p2[2] , 1.0
                , p3[0] , p3[1] , p3[2] , 1.0;

        return m.determinant();
    }

    /// signed area computation
    Real triDeterminant(const Vec2& p0, const Vec2& p1, const Vec2& p2)
    {
        Mat3 m;
        m  << p0[0] , p0[1] , 1.0
                , p1[0] , p1[1] , 1.0
                , p2[0] , p2[1] , 1.0;

        return m.determinant(); // equivalent with (p1-p0).cross(p2-p0)
    }

    bool isTriangleAtBorder(const uint tetID)
    {
        return m_topo->getTetrahedraAroundTriangle(tetID).size() == 1;
    }

    bool isEdgeAtBorder(const uint edgeID)
    {
        core::topology::BaseMeshTopology::TrianglesAroundEdge t = m_topo->getTrianglesAroundEdge(edgeID);
        for(uint i=0; i<t.size(); ++i)
        {
            if(isTriangleAtBorder(t[i]))
                return true;
        }
        return false;
    }

    void init()
    {
        //        this->addInput(d_W);

        /// Initialize the KD-Tree
        m_kdt.build(this->f_position.getValue());

        /// get the topology
        m_topo = this->getContext()->getMeshTopology();

        reinit();
    }

    void reinit()
    {
    }

protected:
    JacobsonShapeFunction() : Inherit()
      , d_W(initData(&d_W, DsMat(), "W", "Weight matrix"))
//      , d_position(initData(&d_position, VCoord(), "position", "positions of the mesh vertices"))
    {
    }

    virtual ~JacobsonShapeFunction()
    {

    }
};


}
}
}


#endif
