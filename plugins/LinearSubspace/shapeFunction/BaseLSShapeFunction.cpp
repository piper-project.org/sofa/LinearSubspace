/******************************************************************************
*       SOFA, Simulation Open-Framework Architecture, development version     *
*                (c) 2006-2017 INRIA, USTL, UJF, CNRS, MGH                    *
*                                                                             *
* This program is free software; you can redistribute it and/or modify it     *
* under the terms of the GNU Lesser General Public License as published by    *
* the Free Software Foundation; either version 2.1 of the License, or (at     *
* your option) any later version.                                             *
*                                                                             *
* This program is distributed in the hope that it will be useful, but WITHOUT *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License *
* for more details.                                                           *
*                                                                             *
* You should have received a copy of the GNU Lesser General Public License    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.        *
*******************************************************************************
* Authors: The SOFA Team and external contributors (see Authors.txt)          *
*                                                                             *
* Contact information: contact@sofa-framework.org                             *
******************************************************************************/
#define LINEARSUBSPACE_LSBaseShapeFunction_CPP

#include <LinearSubspace/config.h>

#include <sofa/defaulttype/TemplatesAliases.h>

#include "../shapeFunction/BaseLSShapeFunction.h"


namespace sofa
{
namespace core
{
namespace behavior
{

#ifndef SOFA_FLOAT
template class SOFA_LinearSubspace_API BaseLSShapeFunction<LSShapeFunctiond>;
template class SOFA_LinearSubspace_API BaseLSShapeFunction<LSShapeFunction2d>;
#endif
#ifndef SOFA_DOUBLE
template class SOFA_LinearSubspace_API BaseLSShapeFunction<LSShapeFunctionf>;
template class SOFA_LinearSubspace_API BaseLSShapeFunction<LSShapeFunction2f>;
#endif


}
}

namespace defaulttype {
#ifndef SOFA_FLOAT
RegisterTemplateAlias LSShapeFunctionAlias("LSShapeFunction", "LSShapeFunctiond");
RegisterTemplateAlias LSShapeFunctionAlias2("LSShapeFunction2", "LSShapeFunction2d");
#else
RegisterTemplateAlias LSShapeFunctionAlias("LSShapeFunction", "LSShapeFunctionf");
RegisterTemplateAlias LSShapeFunctionAlias2("LSShapeFunction2", "LSShapeFunction2f");
#endif

}

}
