This plugin computes tet mesh constraint-based deformations based on the following paper:
    Linear subspace design for real-time shape deformation
    Wang, Y.; Jacobson, A.; Barbič, J. & Kavan, L.
    ACM Transactions on Graphics (TOG), ACM, 2015, 34, 57
