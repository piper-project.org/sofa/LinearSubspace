import Sofa
import SofaPython.Tools
import random

def createSceneAndController(rootNode):
    rootNode.createObject("RequiredPlugin", pluginName="Flexible")
    rootNode.createObject("RequiredPlugin", pluginName="Image")
    rootNode.createObject("RequiredPlugin", pluginName="LinearSubspace")
    rootNode.createObject("RequiredPlugin", pluginName="SofaTetGen")
    

    rootNode.createObject("VisualStyle", displayFlags="showVisualModels showBehaviorModels")

    rootNode.createObject("EulerImplicitSolver")
    rootNode.createObject("CGLinearSolver", iterations="25", tolerance="1e-05", threshold="1e-05")

    rootNode.createObject("MeshObjLoader", name="loader", filename="/home/ulysse/Documents/temp/plate.obj")
    inDataVisuNode = rootNode.createChild("visu")
    inDataVisuNode.createObject("VisualModel", src="@../loader")

    rootNode.createObject("MeshTetrahedrizationEngine", template="Vec3", name="tetrahedrizer", in_position="@loader.position", in_triangle="@loader.triangles", plc=True, quality=True, minratio=1.414)
    rootNode.createObject("MeshTopology", name="topology", position="@tetrahedrizer.out_position", tetrahedra="@tetrahedrizer.out_tetra")


    handle_nodes = [0, 2, 172, 173, 279, 314]
#    handle_nodes = [0, 1, 2, 3]
    handle_regions = [[]]

    rootNode.createObject("PointsFromIndices", template="Vec3d", name="posConstraintValues", position="@loader.position", indices=SofaPython.Tools.listToStr(handle_nodes))


    ssNode = rootNode.createChild("Sub-space")
    ssNode.createObject("LinearAttributeComputationEngine", template="Vec3d", name="Stiffness", position="@../tetrahedrizer.out_position", axis="0 1 0", min=0.5, max=1.0)
#    ssNode.createObject("JacobsonEnergyComputationEngine", template="Vec3d", name="Energy", position="@../tetrahedrizer.out_position")
    ssNode.createObject("JacobsonEnergyComputationEngine", template="Vec3d", name="Energy", position="@../tetrahedrizer.out_position", stiffness="@Stiffness.attribute")
    ssNode.createObject("JacobsonWeightComputationEngine", template="Vec3d", name="Weights", position="@../tetrahedrizer.out_position", Q="@Energy.Q", posConstraints=SofaPython.Tools.listToStr(handle_nodes), regionConstraints=SofaPython.Tools.listToStr(handle_regions))
    ssNode.createObject("WeightMultiTranslationEngine", template="double", name="SplitWeights", W="@Weights.W", nbPosConstraints=len(handle_nodes))


    nodeVec = ssNode.createChild("vec")
    nodeVec.createObject("MechanicalObject", template="Vec3", name="dofs", size=len(handle_nodes), position="@../../posConstraintValues.indices_position", showObject=True, showObjectScale=10.)


    nodeAffine = ssNode.createChild("affine")
    nodeAffine.createObject("MechanicalObject", template="Affine", name="dofs", size=len(handle_regions), showObject=False, showObjectScale=1.)


    mappedNode = nodeVec.createChild("mapped")
    nodeAffine.addChild(mappedNode)
    mappedNode.createObject("MechanicalObject", template="Vec3", name="dofs", position="@/loader.position")
    mappedNode.createObject("LSMultiMapping", template="Vec3,Affine,Vec3", name="mapping", input1="@../../vec", input2="@../../affine", output="@./", index1="@/Sub-space/SplitWeights.index1", index2="@/Sub-space/SplitWeights.index2", weight1="@/Sub-space/SplitWeights.weight1", weight2="@/Sub-space/SplitWeights.weight2")

    mappedNode.createObject("MeshTopology", name="visual", drawTetrahedra=True, drawTriangles=True, position="@dofs.position", tetrahedra="@/tetrahedrizer.out_tetra")









