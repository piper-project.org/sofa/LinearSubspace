import Sofa

def createScene(rootNode):
    rootNode.createObject("RequiredPlugin", pluginName="Flexible")
    rootNode.createObject("RequiredPlugin", pluginName="LinearSubspace")
    
    nodeVec = rootNode.createChild("vec")
    nodeVec.createObject("MechanicalObject", template="Vec3", name="dofs", size=4)
    
    nodeAffine = rootNode.createChild("affine")
    nodeAffine.createObject("MechanicalObject", template="Affine", name="dofs", size=2)
    
    mappedNode = nodeVec.createChild("mapped")
    nodeAffine.addChild(mappedNode)
    mappedNode.createObject("MechanicalObject", template="Vec3", name="dofs", size=12)
    mappedNode.createObject("LSMultiMapping", template="Vec3,Affine,Vec3", name="mapping", input1="@/vec", input2="@/affine", output="@./")
    
    
