import Sofa
import SofaPython.Tools

def createSceneAndController(rootNode):
    rootNode.createObject("RequiredPlugin", pluginName="Flexible")
    rootNode.createObject("RequiredPlugin", pluginName="Image")
    rootNode.createObject("RequiredPlugin", pluginName="LinearSubspace")
    
    rootNode.createObject("MeshObjLoader", name="loader", filename="mesh/Armadillo_simplified.obj")
#    rootNode.createObject("MeshObjLoader", name="loader", filename="/home/ulysse/Documents/temp/tetra/child_skin.obj")
    

    tetraNode = rootNode.createChild("tetra")
    
    tetraNode.createObject("MeshVTKLoader", name="loader", filename="mesh/Armadillo_Tetra_4406.vtu")
    posConstraintIndices = [668, 262, 732, 726, 1253, 465];
#    posConstraintIndices = [1424, 1353, 1410];
    
#    tetraNode.createObject("MeshVTKLoader", name="loader", filename="/home/ulysse/Documents/temp/tetra/Cube.vtu")
#    posConstraintIndices = [0, 7];
    
#    tetraNode.createObject("MeshVTKLoader", name="loader", filename="/home/ulysse/Documents/temp/tetra/Tetra.vtu")
#    posConstraintIndices = [0, 1];


#    tetraNode.createObject("MeshVTKLoader", name="loader", filename="/home/ulysse/Documents/temp/tetra/child_skin.1.vtk")
#    posConstraintIndices = [719, 6128, 3611, 7688, 4594];
    
    tetraNode.createObject("PointsFromIndices", template="Vec3d", name="posConstraintValues", position="@loader.position", indices=SofaPython.Tools.listToStr(posConstraintIndices))
    
    
    linearSSNode = tetraNode.createChild("linearSS")
    linearSSNode.createObject("MeshTopology", name="topology", src="@../loader", drawTetrahedra=False)
    linearSSNode.createObject("EulerImplicitSolver")
    linearSSNode.createObject("CGLinearSolver")
    linearSSNode.createObject("JacobsonEnergyComputationEngine", template="Vec3d", name="Energy", position="@../loader.position", laplacianScheme="2", massScheme="1")
    linearSSNode.createObject("JacobsonWeightComputationEngine", template="double", name="Weights", Q="@Energy.Q", posConstraints=SofaPython.Tools.listToStr(posConstraintIndices), position="@../loader.position")
    
#    linearSSNode.createObject("WeightTranslationEngine", template="double", name="SkinningWeights", W="@Weights.W")
    
    linearSSNode.createObject("MechanicalObject", template="Vec3", name="dofs", position="@../posConstraintValues.indices_position", showObject=True, showObjectScale=5.)
    
    linearSSNode.createObject("JacobsonShapeFunction", template="ShapeFunctiond", name="jacobsonSF", position="@../loader.position", W="@Weights.W", nbRef="6")
    
    visualNode = linearSSNode.createChild("deformedMesh")
    visualNode.createObject("VisualModel", name="visual", src="@../../../loader")
    visualNode.createObject("LinearMapping", name="mapping", template="Vec3d,ExtVec3f", shapeFunction="jacobsonSF", printLog="true")
    
    
    
    
    
    
    
    
