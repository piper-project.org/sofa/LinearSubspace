import Sofa
import SofaPython.Tools

import LinearSubspace.API as LS

def createScene(rootNode):
    rootNode.createObject("RequiredPlugin", pluginName="Flexible")
    rootNode.createObject("RequiredPlugin", pluginName="LinearSubspace")
    rootNode.createObject("RequiredPlugin", pluginName="SofaTetGen")

    rootNode.gravity=[0, -10, 0]

    rootNode.createObject("VisualStyle", displayFlags="showVisualModels showBehaviorModels")

    rootNode.createObject("EulerImplicitSolver")
    rootNode.createObject("CGLinearSolver", iterations="25", tolerance="1e-05", threshold="1e-05")

    dataNode = rootNode.createChild("data")
    mesh = dataNode.createObject("MeshObjLoader", name="mesh", filename="/home/ulysse/Documents/temp/plate.obj")

#    stiffness = dataNode.createObject("LinearAttributeComputationEngine", template="Vec3d", name="Stiffness", position="@mesh.position", axis="1 0 0", min=0.1, max=1.0)
    stiffness = dataNode.createObject("LinearAttributeComputationEngine", template="Vec3d", name="Stiffness", position="@mesh.position", axis="0 0 1", min=1.0, max=1.0, interPos="0.33 0.33 0.66 0.66", interVal="1.0 10 10 1.0")

    lsNode = rootNode.createChild("ls")

    sfMesh = LS.ShapeFunctionMesh(lsNode)
    sfMesh.addEnvelop("mesh", mesh)
    sfMesh.addTetGenTetrahedrization()

    lsDof = LS.LSDof(sfMesh)

    lsDof.addPointsNear([[-2, 0, -1]])
    lsDof.addPointsNear([[-2, 1,  0]])
    lsDof.addPointsNear([[-2, 0,  1]])
    lsDof.addPointsNear([[-1, 1, -1]])
    lsDof.addPointsNear([[-1, 0,  0]])
    lsDof.addPointsNear([[-1, 1,  1]])
    lsDof.addPointsNear([[ 0, 0, -1]])
    lsDof.addPointsNear([[ 0, 1,  0]])
    lsDof.addPointsNear([[ 0, 0,  1]])
    lsDof.addPointsNear([[ 1, 1, -1]])
    lsDof.addPointsNear([[ 1, 0,  0]])
    lsDof.addPointsNear([[ 1, 1,  1]])
    lsDof.addPointsNear([[ 2, 0, -1]])
    lsDof.addPointsNear([[ 2, 1,  0]])
    lsDof.addPointsNear([[ 2, 0,  1]])

    lsDof.addMechanicalObjects()
    pointDof = lsDof.pointNode.getObject("dof")
    pointDof.showObject = True
    pointDof.showObjectScale = 0.1
    pointDof.showColor="1 0 0 1"
    pointDof.drawMode = 1
    lsDof.pointNode.createObject("FixedConstraint", indices="0 1 2 3 4 5")

    affineDof = lsDof.affineNode.getObject("dof")
    affineDof.showObject = True
    affineDof.showObjectScale = 1.0
#    lsDof.affineNode.createObject("FixedConstraint")

    sf = LS.ShapeFunction(sfMesh, lsDof)
    sf.addJaconsonShapeFunction("@"+stiffness.getPathName()+".attribute")

    collisionNode = lsDof.createChild("collision")
    collisionNode.createObject("MechanicalObject", template="Vec3", position="@/data/mesh.position")
    lsDof.insertLSMapping(collisionNode, isMechanical=False)
    visualNode = collisionNode.createChild("visual")
#    visualNode.createObject("VisualModel", src="@/data/mesh")
    visualNode.createObject("DataDisplay", src="@/data/mesh", pointData="@"+stiffness.getPathName()+".attribute")
    visualNode.createObject("ColorMap", colorScheme="Blue to Red")
    visualNode.createObject("IdentityMapping")

    behaviorNode = lsDof.createChild("behavior")
    behaviorNode.createObject("MeshTopology", name="topology", src="@"+sfMesh.topology.getPathName())
    behaviorNode.createObject("MechanicalObject", template="Vec3")
    behaviorNode.createObject("UniformMass", name="mass", totalmass=10)
    behaviorNode.createObject("TetrahedronFEMForceField", name="ff", youngModulus=500, poissonRatio=0.45)
    lsDof.insertLSMapping(behaviorNode)



