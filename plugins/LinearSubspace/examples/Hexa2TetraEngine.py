import Sofa
import SofaPython.Tools
import random

def createSceneAndController(rootNode):
    rootNode.createObject("RequiredPlugin", pluginName="Flexible")
    rootNode.createObject("RequiredPlugin", pluginName="Image")
    rootNode.createObject("RequiredPlugin", pluginName="LinearSubspace")

    rootNode.createObject("MeshObjLoader", name="loader", filename="mesh/Armadillo_simplified.obj")

    meshVisualNode = rootNode.createChild("meshVisual")
    meshVisualNode.createObject("VisualModel", src="@../loader", color="0.75 0.75 0.75 0.5")

    rasterNode = rootNode.createChild("raster")
    rasterNode.createObject("MeshToImageEngine", template="ImageUC", name="image", src="@../loader", voxelSize=0.5, value="1", fillInside=True, insideValue="2")

    imageVisualNode = rootNode.createChild("imageVisual")
    imageVisualNode.createObject("ImageViewer", template="ImageUC",  name="viewer", src="@/raster/image")

    hexaNode = rootNode.createChild("hexa")
    hexaNode.createObject("ImageSampler", template="ImageUC",  name="sampler", src="@/raster/image")

    tetraNode = rootNode.createChild("tetra")
    tetraNode.createObject("Hexa2TetraEngine", name="converter", hexaIndex="@/hexa/sampler.hexahedra" )
    tetraNode.createObject("MeshTopology", name="topology", position="@/hexa/sampler.position", tetrahedra="@converter.tetraIndex", drawTetrahedra=True)




