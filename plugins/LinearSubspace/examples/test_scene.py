import Sofa
import SofaPython.Tools
import random

def createSceneAndController(rootNode):
    rootNode.createObject("RequiredPlugin", pluginName="Flexible")
    rootNode.createObject("RequiredPlugin", pluginName="Image")
    rootNode.createObject("RequiredPlugin", pluginName="LinearSubspace")
    
    rootNode.createObject("MeshObjLoader", name="loader", filename="mesh/Armadillo_simplified.obj")
#    rootNode.createObject("MeshObjLoader", name="loader", filename="/home/ulysse/Documents/temp/tetra/child_skin.obj")
    
    meshVisualNode = rootNode.createChild("meshVisual")
    meshVisualNode.createObject("VisualModel", src="@../loader", color="0.75 0.75 0.75 0.5")
    meshVisualNode.activated = False
    
    
    
    rasterNode = rootNode.createChild("raster")
    rasterNode.createObject("MeshToImageEngine", template="ImageUC", name="image", src="@../loader", voxelSize=0.5, value="1", fillInside=True, insideValue="2")
    
    imageVisualNode = rasterNode.createChild("imageVisual")
    imageVisualNode.createObject("ImageViewer", template="ImageUC",  name="viewer", src="@../image")
    
    voronoiSFNode = rasterNode.createChild("voronoiSF")
    voronoiSFNode.createObject("VoronoiShapeFunction", template="ShapeFunctiond,ImageD", name="SF_vor", position="0 1 0 0 2 0", src="@../image", method="0", nbRef="2")
    
    rasterNode.activated = False
    
    
    
    tetraNode = rootNode.createChild("tetra")
        
    tetraNode.createObject("MeshVTKLoader", name="loader", filename="mesh/Armadillo_Tetra_4406.vtu", createSubelements=True)
    posConstraintIndices = [668, 262, 732, 726, 1253, 465];
#    posConstraintIndices = [668, 262, 732, 726, 1253, 465, 872, 491, 383, 845, 881, 180, 472, 464];

#    tetraNode.createObject("MeshVTKLoader", name="loader", filename="/home/ulysse/Documents/temp/truc/truc_HD.1.vtk", createSubelements=True)

    
#    nbConstraints = 400;
#    posConstraintIndices = random.sample(xrange(1445), nbConstraints)
#    posConstraintIndices = [640, 628, 821, 92, 1256, 809, 938, 753, 953, 205, 1025, 920, 1414, 1023, 1404, 675, 747, 1348, 1082, 237]
    
#    tetraNode.createObject("MeshVTKLoader", name="loader", filename="/home/ulysse/Documents/temp/tetra/Cube.vtu")
#    posConstraintIndices = [0, 7];
    
#    tetraNode.createObject("MeshVTKLoader", name="loader", filename="/home/ulysse/Documents/temp/tetra/Tetra.vtu")
#    posConstraintIndices = [0, 1];

#    tetraNode.createObject("MeshVTKLoader", name="loader", filename="/home/ulysse/Documents/temp/tetra/child_skin.1.vtk")
#    posConstraintIndices = [719, 6128, 3611, 7688, 4594];


#    tetraNode.createObject("MeshVTKLoader", name="loader", filename="/home/ulysse/Documents/temp/tetra/icos.vtk")
#    posConstraintIndices = [0, 1];

#    tetraNode.createObject("MeshVTKLoader", name="loader", filename="/home/ulysse/Documents/temp/truc/truc_HD.1.vtk", createSubelements=True)
#    posConstraintIndices = [19, 761, 656, 1014]
#    nbConstraints = 400;
#    posConstraintIndices = random.sample(xrange(1445), nbConstraints)


#    tetraNode.createObject("MeshVTKLoader", name="loader", filename="/home/ulysse/Documents/temp/armadillo/armadillo_LD.1.vtk", createSubelements=True)
    
#    posConstraintIndices = [513, 515, 38, 41, 644, 13, 318, 1516, 501, 519]
#    nbConstraints = 100;
#    posConstraintIndices = random.sample(xrange(1445), nbConstraints)


#    tetraNode.createObject("MeshVTKLoader", name="loader", filename="/home/ulysse/Documents/temp/armadillo/armadillo_LD_2.1.vtk", createSubelements=True)
    
#    nbConstraints = 100;
#    posConstraintIndices = random.sample(xrange(1000), nbConstraints)




    
    tetraNode.createObject("MeshTopology", name="topology", src="@loader", drawTetrahedra=False)
    
    tetraNode.createObject("MeshSampler", template="Vec3", name="dofsampler", position="@topology.position", edges="@topology.edges", number="100", maxIter="1000", printLog="1")
    
    
    tetraNode.createObject("PointsFromIndices", template="Vec3d", name="posConstraintValues", position="@loader.position", indices=SofaPython.Tools.listToStr(posConstraintIndices))
    
    
    linearSSNode = tetraNode.createChild("linearSS")
    
    
    linearSSNode.createObject("EulerImplicitSolver")
    linearSSNode.createObject("CGLinearSolver", iterations="25", tolerance="1e-05", threshold="1e-05")
    linearSSNode.createObject("JacobsonEnergyComputationEngine", template="Vec3d", name="Energy", position="@../loader.position", laplacianScheme="3", massScheme="0")
#    linearSSNode.createObject("JacobsonWeightComputationEngine", template="double", name="Weights", Q="@Energy.Q", posConstraints=SofaPython.Tools.listToStr(posConstraintIndices))
    linearSSNode.createObject("JacobsonWeightComputationEngine", template="double", name="Weights", Q="@Energy.Q", posConstraints="@../dofsampler.outputIndices")
    
    linearSSNode.createObject("WeightTranslationEngine", template="double", name="SkinningWeights", W="@Weights.W")
    
#    linearSSNode.createObject("MechanicalObject", template="Vec3", name="dofs", position="@../posConstraintValues.indices_position", showObject=True, showObjectScale=5.)
    linearSSNode.createObject("MechanicalObject", template="Vec3", name="dofs", position="@../dofsampler.outputPosition", showObject=True, showObjectScale=5.)
    
    
    
    
    visualNode = linearSSNode.createChild("deformedMesh")

    visualNode.createObject("MechanicalObject", template="Vec3", name="proxyPosition", showObject=False, showObjectScale=10., position="@../../loader.position")
    visualNode.createObject("MeshTopology", name="visual", drawTetrahedra=True, drawTriangles=True, position="@proxyPosition.position", tetrahedra="@../../loader.tetrahedra")
#    visualNode.createObject("VisualModel", name="visual", src="@../../loader")

#    visualNode.createObject("MechanicalObject", template="Vec3", name="proxyPosition", showObject=False, showObjectScale=10., position="@../TransformedPositions.position")
#    visualNode.createObject("MeshTopology", name="visual", drawTetrahedra=True, drawTriangles=True, position="@../TransformedPositions.position", tetrahedra="@../../loader.tetrahedra")
    
    visualNode.createObject("LinearMapping", name="mapping", indices="@../SkinningWeights.indices", weights="@../SkinningWeights.weights", restPosition="@../../loader.position")
    
