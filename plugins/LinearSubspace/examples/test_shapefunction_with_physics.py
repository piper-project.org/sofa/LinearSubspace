import Sofa
import SofaPython.Tools
import random

def createSceneAndController(rootNode):
    rootNode.createObject("RequiredPlugin", pluginName="Flexible")
    rootNode.createObject("RequiredPlugin", pluginName="Image")
    rootNode.createObject("RequiredPlugin", pluginName="LinearSubspace")
    
    rootNode.createObject("MeshObjLoader", name="loader", filename="mesh/Armadillo_simplified.obj")
#    rootNode.createObject("MeshObjLoader", name="loader", filename="/home/ulysse/Documents/temp/tetra/child_skin.obj")
    
    
    meshVisualNode = rootNode.createChild("meshVisual")
    meshVisualNode.createObject("VisualModel", src="@../loader", color="0.75 0.75 0.75 0.5")
    meshVisualNode.activated = False
    
    
    
    rasterNode = rootNode.createChild("raster")
    rasterNode.createObject("MeshToImageEngine", template="ImageUC", name="image", src="@../loader", voxelSize=0.5, value="1", fillInside=True, insideValue="2")
    
    imageVisualNode = rasterNode.createChild("imageVisual")
    imageVisualNode.createObject("ImageViewer", template="ImageUC",  name="viewer", src="@../image")
    
    voronoiSFNode = rasterNode.createChild("voronoiSF")
    voronoiSFNode.createObject("VoronoiShapeFunction", template="ShapeFunctiond,ImageD", name="SF_vor", position="0 1 0 0 2 0", src="@../image", method="0", nbRef="2")
    
    rasterNode.activated = False
    
    
    
    tetraNode = rootNode.createChild("tetra")
    
    tetraNode.createObject("MeshVTKLoader", name="loader", filename="mesh/Armadillo_Tetra_4406.vtu")
#    posConstraintIndices = [668, 262, 732, 726, 1253, 465];

    nbConstraints = 50;
    posConstraintIndices = random.sample(xrange(1445), nbConstraints)
    
#    tetraNode.createObject("MeshVTKLoader", name="loader", filename="/home/ulysse/Documents/temp/tetra/Cube.vtu")
#    posConstraintIndices = [0, 7];
    
#    tetraNode.createObject("MeshVTKLoader", name="loader", filename="/home/ulysse/Documents/temp/tetra/Tetra.vtu")
#    posConstraintIndices = [0, 1];


#    tetraNode.createObject("MeshVTKLoader", name="loader", filename="/home/ulysse/Documents/temp/tetra/child_skin.1.vtk")
#    posConstraintIndices = [719, 6128, 3611, 7688, 4594];
    
    tetraNode.createObject("PointsFromIndices", template="Vec3d", name="posConstraintValues", position="@loader.position", indices=SofaPython.Tools.listToStr(posConstraintIndices))
    
    
    linearSSNode = tetraNode.createChild("linearSS")
    linearSSNode.createObject("MeshTopology", name="topology", src="@../loader", drawTetrahedra=False)
    linearSSNode.createObject("EulerImplicitSolver")
    linearSSNode.createObject("CGLinearSolver")
    linearSSNode.createObject("JacobsonEnergyComputationEngine", template="Vec3d", name="Energy", position="@../loader.position", laplacianScheme="2", massScheme="0")
    linearSSNode.createObject("JacobsonWeightComputationEngine", template="double", name="Weights", Q="@Energy.Q", posConstraints=SofaPython.Tools.listToStr(posConstraintIndices), position="@../loader.position")
    
#    linearSSNode.createObject("WeightTranslationEngine", template="double", name="SkinningWeights", W="@Weights.W")
    
    linearSSNode.createObject("MechanicalObject", template="Vec3", name="dofs", position="@../posConstraintValues.indices_position", showObject=True, showObjectScale=5.)
    linearSSNode.createObject("UniformMass", totaMass="20")
    linearSSNode.createObject("FixedConstraint", indices="0")
    
    linearSSNode.createObject("JacobsonShapeFunction", template="ShapeFunctiond", name="jacobsonSF", position="@../loader.position", W="@Weights.W", nbRef="5")
    
    visualNode = linearSSNode.createChild("deformedMesh")
    visualNode.createObject("VisualModel", name="visual", src="@../../../loader")
    visualNode.createObject("LinearMapping", name="mapping", template="Vec3d,ExtVec3f", shapeFunction="jacobsonSF", printLog="true")
    
    physNode = linearSSNode.createChild("deformedMeshPhys")
    physNode.createObject("MechanicalObject", name="visual", showObject=True, src="@../../loader")
    physNode.createObject("MeshTopology", name="topology", src="@../../loader", drawTriangles=True, position="@visual.position")
    physNode.createObject("TetrahedronFEMForceField", name="fem", youngModulus="500")
    physNode.createObject("LinearMapping", name="mapping", shapeFunction="jacobsonSF", printLog="true")
    
    
    
    
    
    
    
