import Sofa
import SofaPython.Tools

def createSceneAndController(rootNode):
    rootNode.createObject("RequiredPlugin", pluginName="Flexible")
    rootNode.createObject("RequiredPlugin", pluginName="Image")
    rootNode.createObject("RequiredPlugin", pluginName="LinearSubspace")
    
    rootNode.createObject("MeshObjLoader", name="loader", filename="mesh/Armadillo_simplified.obj")
    
    meshVisualNode = rootNode.createChild("meshVisual")
    meshVisualNode.createObject("VisualModel", src="@../loader", color="0.75 0.75 0.75 0.5")
    meshVisualNode.activated = False
    
    
    
    rasterNode = rootNode.createChild("raster")
    rasterNode.createObject("MeshToImageEngine", template="ImageUC", name="image", src="@../loader", voxelSize=0.5, value="1", fillInside=True, insideValue="2")
    
    imageVisualNode = rasterNode.createChild("imageVisual")
    imageVisualNode.createObject("ImageViewer", template="ImageUC",  name="viewer", src="@../image")
    
    voronoiSFNode = rasterNode.createChild("voronoiSF")
    voronoiSFNode.createObject("VoronoiShapeFunction", template="ShapeFunctiond,ImageD", name="SF_vor", position="0 1 0 0 2 0", src="@../image", method="0", nbRef="2")
    
    rasterNode.activated = False
    
    
    
    tetraNode = rootNode.createChild("tetra")
    
    tetraNode.createObject("MeshVTKLoader", name="loader", filename="mesh/Armadillo_Tetra_4406.vtu")
    posConstraintIndices = [668, 262, 732, 726, 1253, 465];
    
#    tetraNode.createObject("MeshVTKLoader", name="loader", filename="/home/ulysse/Documents/temp/Cube.vtu")
#    posConstraintIndices = [0, 7];
    
#    tetraNode.createObject("MeshVTKLoader", name="loader", filename="/home/ulysse/Documents/temp/Tetra.vtu")
#    posConstraintIndices = [0, 1];
    
    tetraNode.createObject("PointsFromIndices", template="Vec3d", name="posConstraintValues", position="@loader.position", indices=SofaPython.Tools.listToStr(posConstraintIndices))
    
    
    linearSSNode = tetraNode.createChild("linearSS")
    linearSSNode.createObject("MeshTopology", name="topology", src="@../loader", drawTetrahedra=False)
    linearSSNode.createObject("EulerImplicitSolver")
    linearSSNode.createObject("CGLinearSolver")
    linearSSNode.createObject("JacobsonEnergyComputationEngine", template="Vec3d", name="EnergyUmbrella", position="@../loader.position", laplacianScheme="0", massScheme="0")
    linearSSNode.createObject("JacobsonWeightComputationEngine", template="double", name="WeightsUmbrella", Q="@EnergyUmbrella.Q", posConstraints=SofaPython.Tools.listToStr(posConstraintIndices))
    
    linearSSNode.createObject("JacobsonEnergyComputationEngine", template="Vec3d", name="EnergyCotan", position="@../loader.position", laplacianScheme="1", massScheme="0")
    linearSSNode.createObject("JacobsonWeightComputationEngine", template="double", name="WeightsCotan", Q="@EnergyCotan.Q", posConstraints=SofaPython.Tools.listToStr(posConstraintIndices))
    
    linearSSNode.createObject("JacobsonEnergyComputationEngine", template="Vec3d", name="EnergyNormDev", position="@../loader.position", laplacianScheme="2", massScheme="0")
    linearSSNode.createObject("JacobsonWeightComputationEngine", template="double", name="WeightsNormDev", Q="@EnergyNormDev.Q", posConstraints=SofaPython.Tools.listToStr(posConstraintIndices))
    
    linearSSNode.createObject("WeightTranslationEngine", template="double", name="SkinningWeightsUmbrella", W="@WeightsUmbrella.W")
    linearSSNode.createObject("WeightTranslationEngine", template="double", name="SkinningWeightsCotan", W="@WeightsCotan.W")
    linearSSNode.createObject("WeightTranslationEngine", template="double", name="SkinningWeightsNormDev", W="@WeightsNormDev.W")
    
    linearSSNode.createObject("MechanicalObject", template="Vec3", name="dofs", position="@../posConstraintValues.indices_position", showObject=True, showObjectScale=5.)
    
    
    UmbrellaNode = linearSSNode.createChild("UmbrellaDeformedMesh")
    UmbrellaNode.createObject("MechanicalObject", template="Vec3", name="proxyPosition", showObject=False, showObjectScale=10., position="@../../loader.position")
    UmbrellaNode.createObject("MeshTopology", name="visual", drawTetrahedra=True, drawTriangles=True, position="@proxyPosition.position", tetrahedra="@../../loader.tetrahedra")
    UmbrellaNode.createObject("LinearMapping", name="mapping", indices="@../SkinningWeightsUmbrella.indices", weights="@../SkinningWeightsUmbrella.weights", restPosition="@../../loader.position")
    
    
    CotanNode = linearSSNode.createChild("CotanDeformedMesh")
    CotanNode.createObject("MechanicalObject", template="Vec3", name="proxyPosition", showObject=False, showObjectScale=10., position="@../../loader.position")
    CotanNode.createObject("MeshTopology", name="visual", drawTetrahedra=True, drawTriangles=True, position="@proxyPosition.position", tetrahedra="@../../loader.tetrahedra")
    CotanNode.createObject("LinearMapping", name="mapping", indices="@../SkinningWeightsCotan.indices", weights="@../SkinningWeightsCotan.weights", restPosition="@../../loader.position")
    
    
    NormDevNode = linearSSNode.createChild("NormDevDeformedMesh")
    NormDevNode.createObject("MechanicalObject", template="Vec3", name="proxyPosition", showObject=False, showObjectScale=10., position="@../../loader.position")
    NormDevNode.createObject("MeshTopology", name="visual", drawTetrahedra=True, drawTriangles=True, position="@proxyPosition.position", tetrahedra="@../../loader.tetrahedra")
    NormDevNode.createObject("LinearMapping", name="mapping", indices="@../SkinningWeightsNormDev.indices", weights="@../SkinningWeightsNormDev.weights", restPosition="@../../loader.position")
    
    
    
    
    
    
    
    
    
