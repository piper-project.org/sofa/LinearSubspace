/******************************************************************************
*       SOFA, Simulation Open-Framework Architecture, development version     *
*                (c) 2006-2017 INRIA, USTL, UJF, CNRS, MGH                    *
*                                                                             *
* This program is free software; you can redistribute it and/or modify it     *
* under the terms of the GNU Lesser General Public License as published by    *
* the Free Software Foundation; either version 2.1 of the License, or (at     *
* your option) any later version.                                             *
*                                                                             *
* This program is distributed in the hope that it will be useful, but WITHOUT *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License *
* for more details.                                                           *
*                                                                             *
* You should have received a copy of the GNU Lesser General Public License    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.        *
*******************************************************************************
* Authors: The SOFA Team and external contributors (see Authors.txt)          *
*                                                                             *
* Contact information: contact@sofa-framework.org                             *
******************************************************************************/

#include "LSMultiMapping.h"

#include "LSJacobianBlock_point.inl"
#include "LSJacobianBlock_affine.inl"

#include <Flexible/types/DeformationGradientTypes.h>

#include <Flexible/quadrature/BaseGaussPointSampler.h>

namespace sofa
{
namespace component
{
namespace mapping
{

template <class TIn1, class TIn2, class TOut>
LSMultiMapping<TIn1, TIn2, TOut>::LSMultiMapping()
    : Inherit ()
    , d_shapeFunctionName(initData(&d_shapeFunctionName,"shapeFunction","name of shape function (optional)"))
    , d_index1 ( initData ( &d_index1,"index1","parent1 indices for each child" ) )
    , d_index2 ( initData ( &d_index2,"index2","parent2 indices for each child" ) )
    , d_weight1 ( initData ( &d_weight1,"weight1","influence weights of parent1 for each child" ) )
    , d_weight2 ( initData ( &d_weight2,"weight2","influence weights of parent2 for each child" ) )
    , d_assemble ( initData ( &d_assemble,false, "assemble","Assemble the matrices (Jacobian/Geometric Stiffness) or use optimized Jacobian/vector multiplications" ) )
    , m_shapeFunction(nullptr)
    , fromModel1(nullptr)
    , fromModel2(nullptr)
    , toModel(nullptr)
{ }

template <class TIn1, class TIn2, class TOut>
void LSMultiMapping<TIn1, TIn2, TOut>::init()
{
    if(!this->fromModel1 && this->getFromModels1().size())     this->fromModel1 = this->getFromModels1()[0];
    if(!this->fromModel2 && this->getFromModels2().size())     this->fromModel2 = this->getFromModels2()[0];
    if(!this->toModel && this->getToModels().size())           this->toModel = this->getToModels()[0];

    if ( !this->toModel->toBaseMechanicalState())  this->setNonMechanical();

    baseMatrices.resize( 2 ); // just a wrapping for getJs()
    baseMatrices[0] = &eigenJacobian1;
    baseMatrices[1] = &eigenJacobian2;

    // check that all children particles got a parent
    const VecVRef& index1 = d_index1.getValue();
    const VecVRef& index2 = d_index2.getValue();
    for (std::size_t i=0; i < index1.size(); ++i)
        if (index1[i].empty() && index2[i].empty())
            serr << "Particle " << i << " has no parent" << sendl;

    this->reinit();

    Inherit::init(); // shouldn't this be called first ?
}


template <class TIn1, class TIn2, class TOut>
void LSMultiMapping<TIn1, TIn2, TOut>::reinit()
{
    Inherit::reinit();

    if(d_index1.getValue().size() != 0
    && d_weight1.getValue().size() == d_index1.getValue().size()
    && d_index2.getValue().size() == d_index1.getValue().size()
    && d_weight2.getValue().size() == d_index2.getValue().size()) // we already have the needed data, we directly use them
    {
        sout<<"Using filled data" <<sendl;
    }
    else // if we do not have the needed data, and have a shape function, we use it to compute needed data (index, weights, etc.)
    {

        if( !m_shapeFunction )
        {
            sofa::core::objectmodel::BaseContext* context = this->getContext();
            std::vector<BaseLSShapeFunction*> sf;
            context->get<BaseLSShapeFunction>(&sf, core::objectmodel::BaseContext::SearchUp);

            if(sf.size() == 0)
            {
                msg_error("LSMultiMapping<...>::reinit()") << "No BaseLSShapeFunction found during up search.";
            }

            for(unsigned int i=0;i<sf.size();i++)
            {
                if(this->d_shapeFunctionName.isSet())
                {
                    if(this->d_shapeFunctionName.getValue().compare(sf[i]->getName()) == 0)
                    {
                        m_shapeFunction=sf[i];
                        msg_info("LSMultiMapping<...>::reinit()") << "Found LSShapefunction called '" << sf[i]->getName() << "'.";
                        break;
                    }
                    else if(i == sf.size()-1)
                    {
                        if(sf.size() == 0)
                        {
                            msg_warning("LSMultiMapping<...>::reinit()") << "No BaseLSShapeFunction called '" << d_shapeFunctionName.getValue() << "' found during up search, falling back on first BaseLSShapeFunction.";
                        }
                        m_shapeFunction=sf[0];
                        break;
                    }
                }
                else
                {
                    m_shapeFunction=sf[0];
                    msg_info("LSMultiMapping<...>::reinit()") << "Found LSShapefunction.";
                    break;
                }
            }
        }


        if( !m_shapeFunction )
        {
            msg_error("LSMultiMapping<...>::reinit()") << "Missing shape function.";
            return;
        }



        sout<<"Using  "<<m_shapeFunction->getName()<<sendl;
        msg_info("LSMultiMapping<...>::reinit()") << "Proceeding to build data based on '" << m_shapeFunction->getName() << "'.";
//        OutVecCoord pos;
        sofa::helper::vector<sofa::defaulttype::Vec3d > pos;
        pos.resize(getToSize());
#pragma omp parallel for
        for(size_t i=0; i<getToSize(); ++i)
        {
//            defaulttype::StdVectorTypes<OutCoord,OutCoord>::set( pos[i], toModel->readRestPositions()[i][0] , toModel->readRestPositions()[i][1] , toModel->readRestPositions()[i][2]);
//            pos[i] = toModel->readRestPositions()[i];



            engine::BaseGaussPointSampler* sampler;
            this->getContext()->get(sampler,core::objectmodel::BaseContext::Local);

            if(sampler)
            {
                sout << "Sampler found, got " << sampler->getNbSamples() << " samples." << sendl;

                this->toModel = this->getToModels()[0];
                uint size = sampler->getNbSamples();

                this->toModel->resize(size);

//                for(size_t i=0; i<size; ++i)
//                    copy(this->toModel[i], sampler->getTransforms()[i]);
            }
            else
            {

                sofa::defaulttype::StdVectorTypes<sofa::defaulttype::Vec3d,sofa::defaulttype::Vec3d>::set( pos[i], toModel->readRestPositions()[i][0] , toModel->readRestPositions()[i][1] , toModel->readRestPositions()[i][2]);
            }
        }



//        std::cout << "beginning shape function computation on " << pos.size() << " positions." << std::endl;

        // interpolate weights at sample positions
        m_shapeFunction->computeShapeFunction(pos,*this->d_index1.beginWriteOnly(),*this->d_index2.beginWriteOnly(),*this->d_weight1.beginWriteOnly(),*this->d_weight2.beginWriteOnly());

        this->d_index1.endEdit();
        this->d_index2.endEdit();
        this->d_weight1.endEdit();
        this->d_weight2.endEdit();

    }

    // init jacobians
    initJacobianBlocks();
    if(d_assemble.getValue())
    {
        updateJ1();
        updateJ2();
    }

}

template <class TIn1, class TIn2, class TOut>
void LSMultiMapping<TIn1, TIn2, TOut>::initJacobianBlocks()
{
    // TODO compute this->jacobian1 and this->jacobian2
    // then apply is just a matter of sparse multiplication

    const VecVWeight& weight1 = this->d_weight1.getValue();
    const VecVAffineWeight& weight2 = this->d_weight2.getValue();

    helper::ReadAccessor<Data<In1VecCoord> > in1 (*this->fromModel1->read(core::ConstVecCoordId::restPosition()));
    helper::ReadAccessor<Data<In2VecCoord> > in2 (*this->fromModel2->read(core::ConstVecCoordId::restPosition()));
    helper::ReadAccessor<Data<OutVecCoord> > out (*this->toModel->read(core::ConstVecCoordId::position()));

    jacobian1.resize(getToSize());
    jacobian2.resize(getToSize());

    assert(out.size() == getToSize());
    assert(in1.size() == getFromSize1());
    assert(in2.size() == getFromSize2());

    assert(weight1.size() == getToSize());
    assert(weight2.size() == getToSize());

    for(size_t i=0; i<getToSize(); ++i)
    {
        assert(weight1[i].size() == getFromSize1());

        jacobian1[i].resize(getFromSize1());
        for(size_t j=0; j<getFromSize1(); ++j)
        {
            jacobian1[i][j].init(typename BlockType1::MaterialToSpatial(), weight1[i][j], typename BlockType1::Gradient(), typename BlockType1::Hessian());
        }

        assert(weight2[i].size() == getFromSize2());
        jacobian2[i].resize(getFromSize2());
        for(size_t j=0; j<getFromSize2(); ++j)
        {
            jacobian2[i][j].init(typename BlockType2::MaterialToSpatial(), weight2[i][j], typename BlockType2::Gradient(), typename BlockType2::Hessian());
        }
    }
}

template <class TIn1, class TIn2, class TOut>
void LSMultiMapping<TIn1, TIn2, TOut>::apply(
    const core::MechanicalParams * /*mparams*/ ,
    Data<OutVecCoord>& dOut,
    const Data<In1VecCoord>& dIn1,
    const Data<In2VecCoord>& dIn2)
{
    OutVecCoord& out = *dOut.beginWriteOnly();
    const In1VecCoord& in1 = dIn1.getValue();
    const In2VecCoord& in2 = dIn2.getValue();

    const VecVRef& index1 = this->d_index1.getValue();
    const VecVRef& index2 = this->d_index2.getValue();

    for(std::size_t i=0; i<jacobian1.size(); i++)
    {
        out[i]=OutCoord();
        for(size_t j=0; j<jacobian1[i].size(); j++)
        {
            size_t index=index1[i][j];
            jacobian1[i][j].addapply(out[i],in1[index]);
        }
        for(size_t j=0; j<jacobian2[i].size(); j++)
        {
            size_t index=index2[i][j];
            jacobian2[i][j].addapply(out[i],in2[index]);
        }
    }

    dOut.endEdit();

    if(d_assemble.getValue())
    {
        if(!BlockType1::constant) eigenJacobian1.resize(0,0);
        if(!BlockType2::constant) eigenJacobian2.resize(0,0);
    };
}

template <class TIn1, class TIn2, class TOut>
void LSMultiMapping<TIn1, TIn2, TOut>::applyJ(const core::MechanicalParams * /*mparams*/ , Data<OutVecDeriv>& dOut, const Data<In1VecDeriv>& dIn1, const Data<In2VecDeriv>& dIn2)
{
    if(this->d_assemble.getValue())
    {
        if( !eigenJacobian1.rows() ) updateJ1();
        if( !eigenJacobian2.rows() ) updateJ2();

//        if( this->maskTo[0]->isActivated() )
//        {
//            updateMaskedJ();
//            maskedEigenJacobian1.mult(dOut,dIn1);
//            maskedEigenJacobian2.mult(dOut,dIn2);
//        }
//        else
        {
            eigenJacobian1.mult(dOut,dIn1);
            eigenJacobian2.addMult(dOut,dIn2);
        }
    }
    else
    {
        OutVecDeriv& out = *dOut.beginWriteOnly();
        const In1VecDeriv& in1 = dIn1.getValue();
        const In2VecDeriv& in2 = dIn2.getValue();

        const VecVRef& index1 = this->d_index1.getValue();
        const VecVRef& index2 = this->d_index2.getValue();

        for( size_t i=0 ; i<this->maskTo[0]->size() ; ++i)
        {
            if( !this->maskTo[0]->isActivated() || this->maskTo[0]->getEntry(i) )
            {
                out[i]=OutDeriv();
                for(size_t j=0; j<jacobian1[i].size(); j++)
                {
                    size_t index=index1[i][j];
                    jacobian1[i][j].addmult(out[i],in1[index]);
                }
                for(size_t j=0; j<jacobian2[i].size(); j++)
                {
                    size_t index=index2[i][j];
                    jacobian2[i][j].addmult(out[i],in2[index]);
                }
            }
        }

        dOut.endEdit();
    }
}

template <class TIn1, class TIn2, class TOut>
void LSMultiMapping<TIn1, TIn2, TOut>::applyJT(
        const core::MechanicalParams * /*mparams*/ ,
        Data<In1VecDeriv>& dIn1, Data<In2VecDeriv>& dIn2,
        const Data<OutVecDeriv>& dOut)
{
    if(d_assemble.getValue())
    {
        if( !eigenJacobian1.rows() ) updateJ1();
        if( !eigenJacobian2.rows() ) updateJ2();

        eigenJacobian1.addMultTranspose(dIn1,dOut);
        eigenJacobian2.addMultTranspose(dIn2,dOut);
    }
    else
    {
        In1VecDeriv& in1 = *dIn1.beginEdit();
        In2VecDeriv& in2 = *dIn2.beginEdit();
        const OutVecDeriv& out = dOut.getValue();

        const VecVRef& index1 = this->d_index1.getValue();
        const VecVRef& index2 = this->d_index2.getValue();

        for( size_t i=0 ; i<this->maskTo[0]->size() ; ++i)
        {
            if( this->maskTo[0]->getEntry(i) )
            {
                for(size_t j=0; j<jacobian1[i].size(); j++)
                {
                    size_t index=index1[i][j];
                    jacobian1[i][j].addMultTranspose(in1[index],out[i]);
                }
                for(size_t j=0; j<jacobian2[i].size(); j++)
                {
                    size_t index=index2[i][j];
                    jacobian2[i][j].addMultTranspose(in2[index],out[i]);
                }
            }
        }

        dIn1.endEdit();
        dIn2.endEdit();
    }

}

template <class TIn1, class TIn2, class TOut>
void LSMultiMapping<TIn1, TIn2, TOut>::applyDJT(
    const core::MechanicalParams* /*mparams*/,
    core::MultiVecDerivId /*parentDfId*/,
    core::ConstMultiVecDerivId )
{
//    serr<<"applyDJT is not implemented" << sendl;
    return;
}

template <class TIn1, class TIn2, class TOut>
void LSMultiMapping<TIn1, TIn2, TOut>::draw(const core::visual::VisualParams* /*vparams*/)
{

}


template <class TIn1, class TIn2, class TOut>
void LSMultiMapping<TIn1, TIn2, TOut>::updateJ1()
{
    eigenJacobian1.resizeBlocks(jacobian1.size(),getFromSize1());

    const VecVRef& index1 = d_index1.getValue();

    for( size_t i=0 ; i<getToSize() ; ++i)
    {
        eigenJacobian1.beginBlockRow(i);
        for(size_t j=0; j<jacobian1[i].size(); j++)
            eigenJacobian1.createBlock( index1[i][j], jacobian1[i][j].getJ());
        eigenJacobian1.endBlockRow();
    }

    eigenJacobian1.finalize();

//    maskedEigenJacobian1.resize(0,0);
}

template <class TIn1, class TIn2, class TOut>
void LSMultiMapping<TIn1, TIn2, TOut>::updateJ2()
{
    eigenJacobian2.resizeBlocks(jacobian2.size(),getFromSize2());

    const VecVRef& index2 = d_index2.getValue();

    for( size_t i=0 ; i<getToSize() ; ++i)
    {
        eigenJacobian2.beginBlockRow(i);
        for(size_t j=0; j<jacobian2[i].size(); j++)
            eigenJacobian2.createBlock( index2[i][j], jacobian2[i][j].getJ());
        eigenJacobian2.endBlockRow();
    }

    eigenJacobian2.finalize();
}

template <class TIn1, class TIn2, class TOut>
const helper::vector<sofa::defaulttype::BaseMatrix*>* LSMultiMapping<TIn1, TIn2, TOut>::getJs()
{
    if( !d_assemble.getValue() ) {
        updateJ1();
        updateJ2();
    }

    if( !eigenJacobian1.rows() ) updateJ1();
    if( !eigenJacobian2.rows() ) updateJ2();

    return &baseMatrices;
}



//template <class TIn1, class TIn2, class TOut>
//void LSMultiMapping<TIn1, TIn2, TOut>::resizeOut()
//{
//    {
//        // TODO this must be done before resizeOut() but is done again in Inherit::init();
//        // also clean the numerous calls to apply
//        this->maskFrom1.resize( this->fromModels1.size() );
//        for( unsigned i=0 ; i<this->fromModels1.size() ; ++i )
//            if (core::behavior::BaseMechanicalState* stateFrom = this->fromModels1[i]->toBaseMechanicalState()) this->maskFrom1[i] = &stateFrom->forceMask;
//        this->maskFrom2.resize( this->fromModels2.size() );
//        for( unsigned i=0 ; i<this->fromModels2.size() ; ++i )
//            if (core::behavior::BaseMechanicalState* stateFrom = this->fromModels2[i]->toBaseMechanicalState()) this->maskFrom2[i] = &stateFrom->forceMask;
//        this->maskTo.resize( this->toModels.size() );
//        for( unsigned i=0 ; i<this->toModels.size() ; ++i )
//            if (core::behavior::BaseMechanicalState* stateTo = this->toModels[i]->toBaseMechanicalState()) this->maskTo[i] = &stateTo->forceMask;
//            else this->setNonMechanical();
//    }

//    helper::ReadAccessor<Data<OutVecCoord> > out (*this->toModel->read(core::ConstVecCoordId::position()));

//    helper::WriteOnlyAccessor<Data<VecCoord> > pos0 (this->f_pos0);
//    helper::WriteOnlyAccessor<Data< VMaterialToSpatial > >  F0(this->f_F0);
//    this->missingInformationDirty=true; this->KdTreeDirty=true; // need to update mapped spatial positions if needed for visualization

//    size_t size;

//    engine::BaseGaussPointSampler* sampler;
//    this->getContext()->get(sampler,core::objectmodel::BaseContext::Local);
//    bool restPositionSet=false;
//    helper::ReadAccessor<Data< OutVecCoord > >  rest(*this->toModel->read(core::ConstVecCoordId::restPosition()));

//    if(sampler) // retrieve initial positions from gauss point sampler (deformation gradient types)
//    {
//        size = sampler->getNbSamples();
//        if(rest.size()==size && size!=1)  restPositionSet=true;

//        this->toModel->resize(size);
//        pos0.resize(size);  for(size_t i=0; i<size; i++) pos0[i]=sampler->getSamples()[i];
//        F0.resize(size);
//        if(restPositionSet)     // use custom rest positions defined in state (to set material directions or set residual deformations)
//        {
//            for(size_t i=0; i<rest.size(); ++i) F0[i]=OutDataTypesInfo<Out>::getF(rest[i]);
//            sout<<rest.size()<<" rest positions imported "<<sendl;
//        }
//        else
//        {
//            for(size_t i=0; i<size; ++i) copy(F0[i],sampler->getTransforms()[i]);
//        }
//        sout<<size <<" gauss points imported"<<sendl;
//    }
//    else  // retrieve initial positions from children dofs (vec types)
//    {
//        size = out.size();
//        pos0.resize(size);  for(size_t i=0; i<size; i++ )  Out::get(pos0[i][0],pos0[i][1],pos0[i][2],out[i]);
//    }

//    // init shape function
//    sofa::core::objectmodel::BaseContext* context = this->getContext();
//    std::vector<BaseShapeFunction*> sf; context->get<BaseShapeFunction>(&sf,core::objectmodel::BaseContext::SearchUp);
//    for(unsigned int i=0;i<sf.size();i++)
//    {
//        if(this->f_shapeFunction_name.isSet()) {if(this->f_shapeFunction_name.getValue().compare(sf[i]->getName()) == 0) _shapeFunction=sf[i];}
//        else if(sf[i]->f_position.getValue().size() == this->fromModel1->getSize()+this->fromModel2->getSize()) _shapeFunction=sf[i];
//    }

//    if(0 != f_index.getValue().size() && pos0.size() == f_index.getValue().size() && f_w.getValue().size() == f_index.getValue().size()) // we already have the needed data, we directly use them
//    {
//        sout<<"using filled data"<<sendl;
//    }
//    else if (_shapeFunction) // if we do not have the needed data, and have a shape function, we use it to compute needed data (index, weights, etc.)
//    {
//        sout<<"found shape function "<<_shapeFunction->getName()<<sendl;
//        helper::vector<mCoord> mpos0;
//        mpos0.resize(pos0.size());
//        for(size_t i=0; i<pos0.size(); ++i)  defaulttype::StdVectorTypes<mCoord,mCoord>::set( mpos0[i], pos0[i][0] , pos0[i][1] , pos0[i][2]);

//        // interpolate weights at sample positions
//        if(this->f_cell.getValue().size()==size) _shapeFunction->computeShapeFunction(mpos0,*this->f_index.beginWriteOnly(),*this->f_w.beginWriteOnly(),*this->f_dw.beginWriteOnly(),*this->f_ddw.beginWriteOnly(),this->f_cell.getValue());
//        else _shapeFunction->computeShapeFunction(mpos0,*this->f_index.beginWriteOnly(),*this->f_w.beginWriteOnly(),*this->f_dw.beginWriteOnly(),*this->f_ddw.beginWriteOnly());
//        this->f_index.endEdit();    this->f_w.endEdit();        this->f_dw.endEdit();        this->f_ddw.endEdit();
//    }
//    else // if the prerequisites are not fulfilled we print an error
//    {
//        serr << "ShapeFunction<"<<ShapeFunctionType::Name()<<"> component not found" << sendl;
//    }

//    // update indices for each parent type
//    helper::WriteOnlyAccessor<Data<VecVRef > > wa_index1 (this->f_index1);   wa_index1.resize(size);
//    helper::WriteOnlyAccessor<Data<VecVRef > > wa_index2 (this->f_index2);   wa_index2.resize(size);
//    for(size_t i=0; i<size; i++ )
//    {
//        for(size_t j=0; j<this->f_index.getValue()[i].size(); j++ )
//        {
//            unsigned int index = this->f_index.getValue()[i][j];
//            if(index<this->getFromSize1()) wa_index1[i].push_back(index);
//            else wa_index2[i].push_back(index-this->getFromSize1());
//        }
//    }

//    // init jacobians
//    initJacobianBlocks();

//    // clear forces
//    if(this->toModel->write(core::VecDerivId::force())) { helper::WriteOnlyAccessor<Data< OutVecDeriv > >  f(*this->toModel->write(core::VecDerivId::force())); for(size_t i=0;i<f.size();i++) f[i].clear(); }
//    // clear velocities
//    if(this->toModel->write(core::VecDerivId::velocity())) { helper::WriteOnlyAccessor<Data< OutVecDeriv > >  vel(*this->toModel->write(core::VecDerivId::velocity())); for(size_t i=0;i<vel.size();i++) vel[i].clear(); }

//    reinit();
//}


}
}
}
