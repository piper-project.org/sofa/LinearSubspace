/******************************************************************************
*       SOFA, Simulation Open-Framework Architecture, development version     *
*                (c) 2006-2017 INRIA, USTL, UJF, CNRS, MGH                    *
*                                                                             *
* This program is free software; you can redistribute it and/or modify it     *
* under the terms of the GNU Lesser General Public License as published by    *
* the Free Software Foundation; either version 2.1 of the License, or (at     *
* your option) any later version.                                             *
*                                                                             *
* This program is distributed in the hope that it will be useful, but WITHOUT *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License *
* for more details.                                                           *
*                                                                             *
* You should have received a copy of the GNU Lesser General Public License    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.        *
*******************************************************************************
* Authors: The SOFA Team and external contributors (see Authors.txt)          *
*                                                                             *
* Contact information: contact@sofa-framework.org                             *
******************************************************************************/
#ifndef LINEARSUBSPACE_LinearJacobianBlock_affine_INL
#define LINEARSUBSPACE_LinearJacobianBlock_affine_INL

#include "LSJacobianBlock.h"

#include <sofa/defaulttype/Vec.h>
#include <sofa/defaulttype/Mat.h>
#include <sofa/defaulttype/VecTypes.h>
#include <Flexible/types/AffineTypes.h>
#include <Flexible/deformationMapping/LinearJacobianBlock_affine.inl>

namespace sofa
{

namespace defaulttype
{

//////////////////////////////////////////////////////////////////////////////////
////  Affine3 -> Vec3
//////////////////////////////////////////////////////////////////////////////////

template<class InReal,class OutReal>
class LSJacobianBlock< Affine3(InReal) , V3(OutReal) > :
    public  BaseJacobianBlock< Affine3(InReal) , V3(OutReal) >
{
public:
    typedef Affine3(InReal) In;
    typedef V3(OutReal) Out;

    enum { dim = Out::spatial_dimensions };

    typedef BaseJacobianBlock<In,Out> Inherit;
    typedef typename Inherit::InCoord InCoord;
    typedef typename Inherit::InDeriv InDeriv;
    typedef typename Inherit::OutCoord OutCoord;
    typedef typename Inherit::OutDeriv OutDeriv;
    typedef typename Inherit::MatBlock MatBlock;
    typedef typename Inherit::KBlock KBlock;
    typedef typename Inherit::Real Real;
    typedef defaulttype::Vec<dim+1,typename Inherit::Real> Weight;
    typedef defaulttype::Vec<dim,typename Inherit::Real> AWeight;


    typedef Vec<dim+1,Real> Gradient;
    typedef Mat<dim,dim,Real> Hessian;
    typedef Vec<dim, Real> SpatialCoord;
    typedef Mat<dim,dim,Real> MaterialToSpatial;

    /**
     * TODO
     */

    static const bool constant=true;

    Real w;
    AWeight aw;

    void init( const LSJacobianBlock<In,Out>& block) // copy
    {
        w  = block.w;
        aw = block.aw;
    }

    void init( const MaterialToSpatial& /*M*/, const Weight& affineWeight, const Gradient& /*dw*/, const Hessian& /*ddw*/)
    {
        w=affineWeight[dim];
        for (std::size_t i=0; i<dim; ++i)
            aw[i] = affineWeight[i];
    }

    void addapply( OutCoord& result, const InCoord& data )
    {
        result +=  data.getCenter() * w + data.getAffine() * aw;
    }

    void addmult( OutDeriv& result, const InDeriv& data )
    {
        result +=  data.getVCenter() * w + data.getVAffine() * aw;
    }

    void addMultTranspose( InDeriv& result, const OutDeriv& data )
    {
        result.getVCenter() += data * w ;

        for (unsigned int i = 0; i < dim; ++i)
            result.getVAffine()[i] += aw * data[i];
    }

    MatBlock getJ()
    {
        // TODO
        MatBlock J = MatBlock();
        for(unsigned int i=0; i<dim; ++i)
            J(i,i)=w;
        for(unsigned int i=0; i<dim; ++i)
            for (unsigned int j=0; j<dim; ++j)
                J(j,i+(j+1)*dim)=aw[i];
        return J;
    }

    // no geometric striffness (constant J)
    KBlock getK(const OutDeriv& /*childForce*/, bool=false) {return KBlock();}
    void addDForce( InDeriv& /*df*/, const InDeriv& /*dx*/,  const OutDeriv& /*childForce*/, const SReal& /*kfactor */) {}
};

//////////////////////////////////////////////////////////////////////////////////
////  Affine3 -> ExtVec3  same as Vec3 -> Factorize using partial instanciation ?
//////////////////////////////////////////////////////////////////////////////////

template<class InReal,class OutReal>
class LSJacobianBlock< Affine3(InReal) , EV3(OutReal) > :
    public  BaseJacobianBlock< Affine3(InReal) , EV3(OutReal) >
{
public:
    typedef Affine3(InReal) In;
    typedef EV3(OutReal) Out;

    enum { dim = Out::spatial_dimensions };

    typedef BaseJacobianBlock<In,Out> Inherit;
    typedef typename Inherit::InCoord InCoord;
    typedef typename Inherit::InDeriv InDeriv;
    typedef typename Inherit::OutCoord OutCoord;
    typedef typename Inherit::OutDeriv OutDeriv;
    typedef typename Inherit::MatBlock MatBlock;
    typedef typename Inherit::KBlock KBlock;
    typedef typename Inherit::Real Real;
    typedef defaulttype::Vec<dim+1,Real> Weight;
    typedef defaulttype::Vec<dim,Real> AWeight;


//    typedef Vec<dim,Real> Gradient;
    typedef Vec<dim+1,Real> Gradient;
    typedef Mat<dim,dim,Real> Hessian;
    typedef Vec<dim, Real> SpatialCoord;
    typedef Mat<dim,dim,Real> MaterialToSpatial;

    /**
     * TODO
     */

    static const bool constant=true;

    Real w;
    AWeight aw;

    void init( const LSJacobianBlock<In,Out>& block) // copy
    {
        w  = block.w;
        aw = block.aw;
    }

    void init( const MaterialToSpatial& /*M*/, const Weight& affineWeight, const Gradient& /*dw*/, const Hessian& /*ddw*/)
    {
        w=affineWeight[dim];
        for (std::size_t i=0; i<dim; ++i)
            aw[i] = affineWeight[i];
    }

    void addapply( OutCoord& result, const InCoord& data )
    {
        result +=  data.getCenter() * w + data.getAffine() * aw;
    }

    void addmult( OutDeriv& result, const InDeriv& data )
    {
        result +=  data.getVCenter() * w + data.getVAffine() * aw;
    }

    void addMultTranspose( InDeriv& result, const OutDeriv& data )
    {
        result.getVCenter() += data * w ;

        for (unsigned int i = 0; i < dim; ++i)
            result.getVAffine()[i] += aw * data[i];
    }

    MatBlock getJ()
    {
        // TODO
        MatBlock J = MatBlock();
        for(unsigned int i=0; i<dim; ++i)
            J(i,i)=w;
        for(unsigned int i=0; i<dim; ++i)
            for (unsigned int j=0; j<dim; ++j)
                J(j,i+(j+1)*dim)=aw[i];
        return J;
    }

    // no geometric striffness (constant J)
    KBlock getK(const OutDeriv& /*childForce*/, bool=false) {return KBlock();}
    void addDForce( InDeriv& /*df*/, const InDeriv& /*dx*/,  const OutDeriv& /*childForce*/, const SReal& /*kfactor */) {}
};



//////////////////////////////////////////////////////////////////////////////////
////  Affine3 -> F331
//////////////////////////////////////////////////////////////////////////////////

template<class InReal,class OutReal>
class LSJacobianBlock< Affine3(InReal) , F331(OutReal) > :
    public  BaseJacobianBlock< Affine3(InReal) , F331(OutReal) >
{
public:
    typedef Affine3(InReal) In;
    typedef F331(OutReal) Out;

    enum { dim = Out::spatial_dimensions };

    typedef BaseJacobianBlock<In,Out> Inherit;
    typedef typename Inherit::InCoord InCoord;
    typedef typename Inherit::InDeriv InDeriv;
    typedef typename Inherit::OutCoord OutCoord;
    typedef typename Inherit::OutDeriv OutDeriv;
    typedef typename Inherit::MatBlock MatBlock;
    typedef typename Inherit::KBlock KBlock;
    typedef typename Inherit::Real Real;
    typedef defaulttype::Vec<dim+1,typename Inherit::Real> Weight;
    typedef defaulttype::Vec<dim,typename Inherit::Real> AWeight;


    typedef Vec<dim+1,Real> Gradient;
    typedef Mat<dim,dim,Real> Hessian;
    typedef Vec<dim, Real> SpatialCoord;
    typedef Mat<dim,dim,Real> MaterialToSpatial;

    /**
     * TODO
     */

    static const bool constant=true;

    Real w;
    AWeight aw;

    void init( const LSJacobianBlock<In,Out>& block) // copy
    {
        w  = block.w;
        aw = block.aw;
    }

    void init( const MaterialToSpatial& /*M*/, const Weight& affineWeight, const Gradient& /*dw*/, const Hessian& /*ddw*/)
    {
        w=affineWeight[dim];
        for (std::size_t i=0; i<dim; ++i)
            aw[i] = affineWeight[i];
    }

    void addapply( OutCoord& result, const InCoord& data )
    {
//        result.getF() +=  covMN(data.getCenter(),w) + data.getAffine()*aw;
    }

    void addmult( OutDeriv& result, const InDeriv& data )
    {
//        result.getF() +=  covMN(data.getCenter(),w) + data.getAffine()*aw;
    }

    void addMultTranspose( InDeriv& result, const OutDeriv& data )
    {
//        result.getVCenter() +=  covMN(data.getCenter(),w);

//        for (unsigned int i = 0; i < dim; ++i)
//            result.getVAffine()[i] += aw * data[i];
    }

    MatBlock getJ()
    {
        // TODO
        MatBlock J = MatBlock();
        for(unsigned int i=0; i<dim; ++i)
            J(i,i)=w;
        for(unsigned int i=0; i<dim; ++i)
            for (unsigned int j=0; j<dim; ++j)
                J(j,i+(j+1)*dim)=aw[i];
        return J;
    }

    // no geometric striffness (constant J)
    KBlock getK(const OutDeriv& /*childForce*/, bool=false) {return KBlock();}
    void addDForce( InDeriv& /*df*/, const InDeriv& /*dx*/,  const OutDeriv& /*childForce*/, const SReal& /*kfactor */) {}
};


}
}
#endif
