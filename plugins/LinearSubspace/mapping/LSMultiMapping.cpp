/******************************************************************************
*       SOFA, Simulation Open-Framework Architecture, development version     *
*                (c) 2006-2017 INRIA, USTL, UJF, CNRS, MGH                    *
*                                                                             *
* This program is free software; you can redistribute it and/or modify it     *
* under the terms of the GNU Lesser General Public License as published by    *
* the Free Software Foundation; either version 2.1 of the License, or (at     *
* your option) any later version.                                             *
*                                                                             *
* This program is distributed in the hope that it will be useful, but WITHOUT *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License *
* for more details.                                                           *
*                                                                             *
* You should have received a copy of the GNU Lesser General Public License    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.        *
*******************************************************************************
* Authors: The SOFA Team and external contributors (see Authors.txt)          *
*                                                                             *
* Contact information: contact@sofa-framework.org                             *
******************************************************************************/

#include <LinearSubspace/config.h>
#include "LSMultiMapping.inl"

#include <sofa/core/Multi2Mapping.inl>

#include <sofa/core/ObjectFactory.h>
#include <sofa/defaulttype/VecTypes.h>
#include <sofa/defaulttype/RigidTypes.h>
#include <Flexible/types/AffineTypes.h>
#include <Flexible/types/DeformationGradientTypes.h>

namespace sofa
{

namespace component
{
namespace mapping
{

using namespace defaulttype;
using namespace core::behavior;

SOFA_DECL_CLASS(LSMultiMapping)

// Register in the Factory
//int LSMultiMappingClass = core::RegisterObject("Map child positions as a linear combination of parents.")
int LSMultiMappingClass = core::RegisterObject("Computes compactly supported hat shape functions")
        .add< LSMultiMapping< Vec3Types, Affine3Types, Vec3Types > >(true)
//        .add< LSMultiMapping< Vec3Types, Affine3Types, ExtVec3fTypes > >()
        .add< LSMultiMapping< Vec3Types, Affine3Types, F331Types > >()
        ;

template class SOFA_LinearSubspace_API LSMultiMapping< Vec3Types, Affine3Types, Vec3Types >;
//template class SOFA_LinearSupspace_API LSMultiMapping< Vec3Types, Affine3Types, ExtVec3fTypes >;
template class SOFA_LinearSubspace_API LSMultiMapping< Vec3Types, Affine3Types, F331Types >;


}
}
}

