/******************************************************************************
*       SOFA, Simulation Open-Framework Architecture, development version     *
*                (c) 2006-2017 INRIA, USTL, UJF, CNRS, MGH                    *
*                                                                             *
* This program is free software; you can redistribute it and/or modify it     *
* under the terms of the GNU Lesser General Public License as published by    *
* the Free Software Foundation; either version 2.1 of the License, or (at     *
* your option) any later version.                                             *
*                                                                             *
* This program is distributed in the hope that it will be useful, but WITHOUT *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License *
* for more details.                                                           *
*                                                                             *
* You should have received a copy of the GNU Lesser General Public License    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.        *
*******************************************************************************
* Authors: The SOFA Team and external contributors (see Authors.txt)          *
*                                                                             *
* Contact information: contact@sofa-framework.org                             *
******************************************************************************/
#ifndef LINEARSUBSPACE_JacobsonWeightComputationEngine_H
#define LINEARSUBSPACE_JacobsonWeightComputationEngine_H


#include "LinearSubspace/config.h"

#include <sofa/helper/rmath.h>
//#include <sofa/helper/OptionsGroup.h>
#include <sofa/core/DataEngine.h>

#include <sofa/core/topology/BaseMeshTopology.h>

#include <algorithm>
#include <iostream>
#include <map>
#include <string>
#include <iostream>
#include <fstream>

#include <Eigen/Core>
#include <Eigen/Sparse>
#include <Eigen/Dense>

#include "LinearSubspace/utility/EigenStreams.h"
#include "LinearSubspace/utility/OctaveOutputs.h"



#define LINEARSUBSPACE_JacobsonWeightComputationEngine_EPSILON (1e-6)
#define LINEARSUBSPACE_JacobsonWeightComputationEngine_TERMINAL_DISPLAY    false

#if LINEARSUBSPACE_JacobsonWeightComputationEngine_TERMINAL_DISPLAY
  #define OUT   std::cout
  #define ENDL  std::endl
#else
  #define OUT   sout
  #define ENDL  sendl
#endif

namespace sofa
{
namespace component
{
namespace engine
{

using helper::vector ;

/**
    This Engine computes a deformation subspace (ie weights) for tetrahedral meshes based on a previously calculated energy
*/
template <typename DataTypes>
class SOFA_LinearSubspace_API JacobsonWeightComputationEngine : public core::DataEngine
{

public:
    typedef core::DataEngine Inherited;
    SOFA_CLASS(SOFA_TEMPLATE(JacobsonWeightComputationEngine, DataTypes) , Inherited);

    typedef typename DataTypes::VecCoord VecCoord;
    typedef typename DataTypes::Coord Coord;
    typedef typename DataTypes::Real Real;

    typedef Eigen::SparseMatrix<Real> SpMat;
    typedef Eigen::Matrix<Real, Eigen::Dynamic, Eigen::Dynamic> DsMat;

    typedef helper::vector<uint> VUint;
    typedef helper::vector<helper::SVector<uint>> VecVUint;

    /// Input
    Data<VecCoord> d_position;
    Data<SpMat> d_Q;
    Data<VUint> d_posConstraints;
    Data<VecVUint> d_regionConstraints;

    /// Switches
    Data<bool> d_verbose;
    Data<bool> d_display_constraint;
    Data<bool> d_perform_tests;
    Data<bool> d_display_matrix_S;
    Data<bool> d_export_matrix_S;
    Data<bool> d_display_matrix_T;
    Data<bool> d_export_matrix_T;
    Data<bool> d_display_matrix_W;
    Data<bool> d_export_matrix_W;
    Data<bool> d_display_matrix_J;
    Data<bool> d_export_matrix_J;

    /// Output
    Data<DsMat> d_W;
//    Data<uint> d_nbPosConstraints;
//    Data<uint> d_nbRegionConstraints;


    virtual std::string getTemplateName() const    { return templateName(this); }
    static std::string templateName(const JacobsonWeightComputationEngine<DataTypes>* = NULL) { return DataTypes::Name(); }

    virtual void init()
    {
        setDirtyValue();
    }

    virtual void reinit()
    {
        update();
    }

    void update()
    {
        if(d_verbose.getValue())
        {
            OUT << "Entering JacobsonWeightComputationEngine::update()." << ENDL;
        }

        helper::ReadAccessor< Data<SpMat>> Q_accessor(d_Q);
        const SpMat& Q = Q_accessor.ref();

        uint nbVerts = Q.cols();


        helper::ReadAccessor< Data<VecCoord>> position_accessor(d_position);
        const VecCoord& position = position_accessor.ref();

        assert(position.size() == nbVerts && "energy matrix size mismatch position list !");

        helper::ReadAccessor< Data<VUint>> posConstraints_accessor(d_posConstraints);
        const VUint& posConstraints = posConstraints_accessor.ref();

        for(uint i=0; i<posConstraints.size(); ++i)
        {
//            assert((posConstraints[i] < nbVerts) && "position constraint index out of bound !");
            if(posConstraints[i] >= nbVerts)
            {
                msg_error("JacobsonWeightComputationEngine::update()") << "Constrained index (" << i << ") not valid ! Max = " << nbVerts-1;
            }

            auto it = std::find(posConstraints.begin()+i+1, posConstraints.end(), posConstraints[i]);
            if(it != posConstraints.end())
            {
                msg_warning("JacobsonWeightComputationEngine::update()") << "Duplicated index " << posConstraints[i] << " at locations " << i << " and " << posConstraints.begin() - it;
            }
        }

//        d_nbPosConstraints.setValue(posConstraints.size());


        helper::ReadAccessor< Data<VecVUint>> regionConstraint_accessor(d_regionConstraints);
        const VecVUint& regionConstraints = regionConstraint_accessor.ref();

        OUT << regionConstraints.size() << ENDL;

        for(uint i=0; i<regionConstraints.size(); ++i)
        {
            for(uint j=0; j<regionConstraints[i].size(); ++j)
            {
//                assert((regionConstraints[i][j] < nbVerts) && "region constraint index out of bound !");
                if(regionConstraints[i][j] >= nbVerts)
                {
                    msg_error("JacobsonWeightComputationEngine::update()") << "Constrained index (" << i << ") not valid ! Max = " << nbVerts-1;
                }

                auto it = std::find(posConstraints.begin(), posConstraints.end(), regionConstraints[i][j]);
                if(it != posConstraints.end())
                {
                    msg_warning("JacobsonWeightComputationEngine::update()") << "Duplicated index " << regionConstraints[i][j] << " at locations "  << posConstraints.begin() - it << " in position constraints and " << j << " in region #" << i << ".";
                }

                it = std::find(regionConstraints[i].begin()+j+1, regionConstraints[i].end(), regionConstraints[i][j]);
                if(it != regionConstraints[i].end())
                {
                    msg_warning("JacobsonWeightComputationEngine::update()") << "Duplicated index " << regionConstraints[i][j] << " at locations "  << posConstraints.begin() - it << " and " << j << " in region #" << i << ".";
                }

                for(uint k=i+1; k<regionConstraints.size(); ++k)
                {
                    auto it = std::find(regionConstraints[k].begin(), regionConstraints[k].end(), regionConstraints[i][j]);
                    if(it != regionConstraints[k].end())
                    {
                        msg_warning("JacobsonWeightComputationEngine::update()") << "Duplicated index " << regionConstraints[i][j] << " at location "  << regionConstraints[k].begin() - it << " region #" << k << " and location " << j << " in region #" << i << ".";
                    }
                }
            }
        }

//        d_nbRegionConstraints.setValue(posConstraints.size());

        helper::WriteOnlyAccessor< Data<DsMat> > W_accessor(d_W);
        DsMat& W = W_accessor.wref();

        typedef Eigen::Triplet<Real> Tri;
        std::vector<Tri> tripletList;     /// used for filling sparse matrices


//        vector<uint> posConstraints;
//        posConstraints.push_back(rand()%nbVerts);
//        posConstraints.push_back(rand()%nbVerts);
//        posConstraints.push_back(rand()%nbVerts);
//        posConstraints.push_back(rand()%nbVerts);

        uint m_p = posConstraints.size();
        uint r = regionConstraints.size();
        uint h = m_p + r*4;
        uint m_r = 0;
        for(const vector<uint>& r_i : regionConstraints)
            m_r += r_i.size();

        uint m = m_p+m_r;


        if(d_verbose.getValue())
        {
            OUT << "Computing weights. NbVerts = " << nbVerts << ", m = " << m_p << " + " << m_r << " = " << m << ", r = " << r << ", h = " << h << "." << ENDL;
        }

        if(d_display_constraint.getValue())
        {
            if(posConstraints.size() == 0)
            {
                OUT << "No position constrain. " << ENDL;
            }
            else
            {
                OUT << "Position constrained indices: " << ENDL;
                for(auto& i : posConstraints)
                {
                    OUT << i << " ";
                }
                OUT << ENDL;
            }

            if(regionConstraints.size() == 0)
            {
                OUT << "No region constrain. " << ENDL;
            }
            else
            {
                OUT << "Region constrained indices: " << ENDL;
                for(auto& i : regionConstraints)
                {
                    for(auto& j : i)
                    {
                        OUT << j << " ";
                    }
                    OUT << ENDL;
                    OUT << ENDL;
                }
            }
        }


        /// Selector matrices computation

        if(d_verbose.getValue())
        {
            OUT << "  Filling matrix S..." << ENDL;
        }
        SpMat S(m, nbVerts);
        tripletList.reserve(m);
        for(uint i=0; i<m_p; i++)
        {
            tripletList.push_back(Tri(i, posConstraints[i], 1.0));
        }
        uint current_stride = m_p;
        for(uint i=0; i<r; i++)
        {
            for(uint j=0; j<regionConstraints[i].size(); j++)
            {
                tripletList.push_back(Tri(current_stride + j, regionConstraints[i][j], 1.0));
            }
            current_stride += regionConstraints[i].size();
        }
        S.setFromTriplets(tripletList.begin(), tripletList.end());
        tripletList.clear();


        if(d_display_matrix_S.getValue())
        {
            OUT << "  S = " << ENDL << S << ENDL;
        }
        if(d_export_matrix_S.getValue())
        {
            exportOctaveSparseMat<Real>(S, "../../../temp/matrix/S.mat", "S_cpp");
        }



        if(d_verbose.getValue())
        {
            OUT << "  Filling matrix T..." << ENDL;
        }
        SpMat T(nbVerts-m, nbVerts);
        tripletList.clear();
        tripletList.reserve(nbVerts-m);
        uint k=0;
        for(uint i=0; i<nbVerts; i++)
        {
            bool isFree = true;
            if(find(posConstraints.begin(), posConstraints.end(), i) != posConstraints.end())
            {
                isFree = false;
            }
            else
            {
                for(uint j=0; j<r; j++)
                {
                    if(find(regionConstraints[j].begin(), regionConstraints[j].end(), i) != regionConstraints[j].end())
                    {
                        isFree = false;
                        break;
                    }
                }
            }
            if(isFree)
            {
                tripletList.push_back(Tri(k, i, 1.0));
                if(k >= nbVerts-m)
                    msg_error("JacobsonWeightComputationEngine") << "Number of constrained and free mesh vertices mismatch total vert count. Is there duplicated entries?";
                k++;

            }
        }

//        OUT << "T0 " << tripletList.size() << ENDL;
        T.setFromTriplets(tripletList.begin(), tripletList.end());
//        OUT << "T1 " << ENDL;
        tripletList.clear();

        if(d_display_matrix_T.getValue())
        {
            OUT << "  T = " << ENDL << T << ENDL;
        }
        if(d_export_matrix_T.getValue())
        {
            exportOctaveSparseMat<Real>(T, "../../../temp/matrix/T.mat", "T_cpp");
        }




        if(d_verbose.getValue())
        {
            OUT << "  Filling matrix J..." << ENDL;
        }

        //*
        tripletList.reserve(m_p+4*m_r);

        for(uint i=0; i<m_p; i++)
        {
            tripletList.push_back(Tri(i, i, 1.0));
        }
        current_stride = m_p;
        for(uint i=0; i<r; i++)
        {
            for(uint j=0; j<regionConstraints[i].size(); j++)
            {
                for(uint k=0; k<3; k++)
                {
                    tripletList.push_back(Tri(current_stride + j, m_p+i*4 + k, position[regionConstraints[i][j]][k]));
                }
                tripletList.push_back(Tri(current_stride + j, m_p+i*4+3, 1.0));
            }
            current_stride += regionConstraints[i].size();
        }
        SpMat J(m, h);
        J.setFromTriplets(tripletList.begin(), tripletList.end());
        tripletList.clear();

        if(d_display_matrix_J.getValue())
        {
            OUT << "  J = " << ENDL << J << ENDL;
        }

        if(d_export_matrix_J.getValue())
        {
            exportOctaveSparseMat<Real>(J, "../../../temp/matrix/J.mat", "J_cpp");
        }
        //*/

        //*

        if(d_verbose.getValue())
        {
            OUT << "  Computing TQT'..." << ENDL;
        }
        SpMat TQTt = T*Q*T.transpose();

        if(d_perform_tests.getValue())
        {
            Eigen::SimplicialLDLT<SpMat> TQTtSolver(TQTt);
            unsigned int nb_null = 0;
            auto diag = TQTtSolver.vectorD();

            for(uint i=0; i<diag.size(); ++i)
            {
                Real d = std::abs(diag(i));
                if(d < LINEARSUBSPACE_JacobsonWeightComputationEngine_EPSILON)
                {
                    ++nb_null;
                }
            }
//            Real det = TQTtSolver.determinant();

//            if(abs(det) < LINEARSUBSPACE_JacobsonWeightComputationEngine_EPSILON)
            if(nb_null>0)
            {
                msg_warning("JacobsonWeightComputationEngine::update()") << "Matrix TQTt is non inversible ! Null entries: " << nb_null << " / " << diag.size();
            }
//            else
//            {
//                OUT << "det(TQTt) = " << det << ENDL;
//            }
        }

        Eigen::SimplicialCholesky<SpMat> TQTtSolver(TQTt);
//        Eigen::SimplicialLDLT<SpMat> TQTtSolver(TQTt);
//        Eigen::SimplicialLLT<SpMat> TQTtSolver(TQTt);

        if(d_verbose.getValue())
        {
            OUT << "  Computing TAS'J..." << ENDL;
        }
        SpMat TAStJ = T*Q*S.transpose()*J;

//        std::vector<Eigen::VectorXd> temp_columns;
//        temp_columns.resize(m_p);

        if(d_verbose.getValue())
        {
            OUT << "  Computing (TQT')inv.TAS'J..." << ENDL;
        }
        DsMat temp_mat =  DsMat::Zero(nbVerts-m, h);
        for(uint i=0; i<h; ++i)
        {
//            Eigen::VectorXd temp_vector = TAStJ.col(i);
//            Eigen::VectorXd temp_vector_2 = TQTtSolver.solve(temp_vector);
//            temp_mat.col(i) = temp_vector_2;

//            Eigen::VectorXd temp_vector_2 = TQTtSolver.solve(TAStJ.col(i));
//            temp_mat.col(i) = temp_vector_2;

            temp_mat.col(i) = TQTtSolver.solve(TAStJ.col(i));
        }


        if(d_verbose.getValue())
        {
            OUT << "  Computing T'(TQT')inv.TAS'J..." << ENDL;
        }
        W = T.transpose() * temp_mat;
//        W = S.transpose() * J - W;

        if(d_verbose.getValue())
        {
            OUT << "  Computing S'T..." << ENDL;
        }
        SpMat StJ;
        StJ = S.transpose() * J;
        DsMat StJ_;
        StJ_ = StJ;

        if(d_verbose.getValue())
        {
            OUT << "  Computing W=S'T-T'(TQT')inv.TAS'J..." << ENDL;
        }
        W = StJ_ - W;

        /*/

        /// Dense computation
        SpMat TQTt = T*Q*T.transpose();
        DsMat DsTQTt; DsTQTt = TQTt;
        SpMat StJ = S.transpose() * J;
        DsMat DsStJ; DsStJ = StJ;
        W = DsStJ - T.transpose() * DsTQTt.inverse() * T*Q*S.transpose()*J;

        //*/

        if(d_display_matrix_W.getValue())
        {
            OUT << "  W = " << ENDL << W << ENDL;
        }

        if(d_export_matrix_W.getValue())
        {
            exportOctaveDenseMat(W, "../../../temp/matrix/W.mat", "W_cpp");
        }


        if(d_perform_tests.getValue())
        {
//            if(d_verbose.getValue())
            {
                OUT << "  Performing weight tests..." << ENDL;
            }
            Real maxW = -std::numeric_limits<Real>::infinity();
            Real minW = std::numeric_limits<Real>::infinity();

            Real maxSum = -std::numeric_limits<Real>::infinity();
            Real minSum = std::numeric_limits<Real>::infinity();

            for(uint i=0; i<W.rows(); ++i)
            {

                Real sum = 0;

                for(uint j=0; j<m_p; ++j)
                {
                    maxW = std::max(maxW, W(i, j));
                    minW = std::min(minW, W(i, j));
                    sum += W(i, j);
                }

                for(uint j=m_p+3; j<h; j+=4)
                {
                    maxW = std::max(maxW, W(i, j));
                    minW = std::min(minW, W(i, j));
                    sum += W(i, j);
                }

                maxSum = std::max(maxSum, sum);
                minSum = std::min(minSum, sum);

            }

            OUT << "    minW : " << minW << ", maxW : " << maxW << ENDL;
            OUT << "    minSum : " << minSum << ", maxSum : " << maxSum << ENDL;

            if(abs(minSum-1.0)>LINEARSUBSPACE_JacobsonWeightComputationEngine_EPSILON || abs(maxSum-1.0)>LINEARSUBSPACE_JacobsonWeightComputationEngine_EPSILON )
            {
                msg_warning("JacobsonWeightComputationEngine::update()") << "Some weight are not normalized: min sum = " << minSum << ", max sum = " << maxSum << ".";
            }

//            if(d_verbose.getValue())
            {
                OUT << "  Performing rest-pose reproduction test..." << ENDL;
            }

            if(d_verbose.getValue())
            {
                OUT << "      Filling matrix H..." << ENDL;
            }

            DsMat H(h, 3);

            /// C1
            for(uint i=0; i<m_p; ++i)
            {
                for(uint j=0; j<3; ++j)
                {
                    H(i, j) = position[posConstraints[i]][j];
                }
            }

            /// H2
            for(uint i=0; i<r; ++i)
            {
                H(m_p + i*4+0, 0) = 1;
                H(m_p + i*4+0, 1) = 0;
                H(m_p + i*4+0, 2) = 0;

                H(m_p + i*4+1, 0) = 0;
                H(m_p + i*4+1, 1) = 1;
                H(m_p + i*4+1, 2) = 0;

                H(m_p + i*4+2, 0) = 0;
                H(m_p + i*4+2, 1) = 0;
                H(m_p + i*4+2, 2) = 1;

                H(m_p + i*4+3, 0) = 0;
                H(m_p + i*4+3, 1) = 0;
                H(m_p + i*4+3, 2) = 0;
            }


            if(d_verbose.getValue())
            {
                OUT << "      ... done." << ENDL;
                OUT << "      Computing reconstruction..." << ENDL;
            }
            DsMat V_reconstruct = W * H;

            if(d_verbose.getValue())
            {
                OUT << "      ... done." << ENDL;
                OUT << "      Filling V..." << ENDL;
            }

            DsMat V(nbVerts, 3);
            for(uint i=0; i<nbVerts; ++i)
            {
                for(uint j=0; j<3; ++j)
                {
                    V(i, j) = position[i][j];
                }
            }

            OUT << "      ... done." << ENDL;

            DsMat V_diff = V - V_reconstruct;
            Real result = (V_diff.transpose() * V_diff).trace();
//            OUT << "    (V-W.H)'.(V-W.H) = " << ENDL << V_diff.transpose() * V_diff << ENDL;
            OUT << "    tr((V-W.H)'.(V-W.H)) = " << result << ENDL;

            if(result > LINEARSUBSPACE_JacobsonWeightComputationEngine_EPSILON)
                msg_warning("JacobsonWeightComputationEngine::update()") << "Rest-pose reproduction test : failure.";
            else
                msg_info("JacobsonWeightComputationEngine::update()") << "Rest-pose reproduction test : success.";
        }


        cleanDirty();
    }




//    void exportOctaveDenseMat(const DsMat& M, const std::string& filename, const std::string& matrixName)
//    {
//        OUT << "Exporting sparse matrix into '" << filename << "'." << ENDL;
//        std::ofstream fs;
//        fs.open(filename);
//        fs << "# File written by JacobsonWeightComputationEngine::exportOctaveDenseMat() \n" ;
//        fs << "# name: " << matrixName << " \n" ;
//        fs << "# type: matrix \n" ;
//        fs << "# rows: " << M.rows() << " \n" ;
//        fs << "# columns: " << M.cols() << " \n" ;

//        for (int i=0; i<M.rows(); ++i)
//        {
//            for (int j=0; j<M.cols(); ++j)
//            {
//                fs << M(i,j) << " " ;
//            }
//            fs << " \n" ;
//        }
//        fs.close();
//    }



//    void exportOctaveSparseMat<Real>(const SpMat& M, const std::string& filename, const std::string& matrixName)
//    {
//        OUT << "Exporting sparse matrix into '" << filename << "'." << ENDL;
//        std::ofstream fs;
//        fs.open(filename);
//        fs << "# File written by JacobsonEnergyComputationEngine::exportOctaveSparseMat<Real>() \n" ;
//        fs << "# name: " << matrixName << " \n" ;
//        fs << "# type: sparse matrix \n" ;
//        fs << "# nnz: " << M.nonZeros() << " \n" ;
//        fs << "# rows: " << M.rows() << " \n" ;
//        fs << "# columns: " << M.cols() << " \n" ;

//        for (int k=0; k<M.outerSize(); ++k)
//        {
//          for(typename SpMat::InnerIterator it(M,k); it; ++it)
//          {
//            it.value();
//            it.row();   // row index
//            it.col();   // col index (here it is equal to k)
//            it.index(); // inner index, here it is equal to it.row()
//            fs << it.row()+1 << " " << it.col()+1 << " " << it.value() << " \n" ;

//          }
//        }
//        fs.close();
//    }


protected:
    JacobsonWeightComputationEngine() : Inherited()
      , d_position(initData(&d_position, VecCoord(), "position","Positions of the vertices of the mesh"))
      , d_Q(initData(&d_Q, SpMat(), "Q","Energy matrix"))
      , d_posConstraints(initData(&d_posConstraints, VUint(), "posConstraints", "List of indices of position-constrained vertices"))
      , d_regionConstraints(initData(&d_regionConstraints, VecVUint(), "regionConstraints", "List of lists of indices of affine-constrained vertex groups"))
      , d_verbose            (initData(&d_verbose,            true , "verbose", "verbose"))
      , d_display_constraint (initData(&d_display_constraint, true , "display_constraint", "display_constrain"))
      , d_perform_tests      (initData(&d_perform_tests,      true , "perform_tests", "perform_tests"))
      , d_display_matrix_S   (initData(&d_display_matrix_S,   false, "display_matrix_S", "display_matrix_S"))
      , d_export_matrix_S    (initData(&d_export_matrix_S,    true , "export_matrix_S", "export_matrix_S"))
      , d_display_matrix_T   (initData(&d_display_matrix_T,   false, "display_matrix_T", "display_matrix_T"))
      , d_export_matrix_T    (initData(&d_export_matrix_T,    true , "export_matrix_T", "export_matrix_T"))
      , d_display_matrix_W   (initData(&d_display_matrix_W,   false, "display_matrix_W", "display_matrix_W"))
      , d_export_matrix_W    (initData(&d_export_matrix_W,    true , "export_matrix_W", "export_matrix_W"))
      , d_display_matrix_J   (initData(&d_display_matrix_J,   false, "display_matrix_J", "display_matrix_J"))
      , d_export_matrix_J    (initData(&d_export_matrix_J,    true , "export_matrix_J", "export_matrix_J"))
      , d_W(initData(&d_W, DsMat(), "W", "Weight matrix"))
//      , d_nbPosConstraints(initData(&d_nbPosConstraints, 0, "nbPosConstraints", "Number of position constraints"))
//      , d_nbRegionConstraints(initData(&d_nbRegionConstraints, 0, "nbRegionConstraints", "Number of region constraints"))

    {

        addInput(&d_position);
        addInput(&d_Q);
        addInput(&d_posConstraints);
        addInput(&d_regionConstraints);

        addInput(&d_verbose);
        addInput(&d_display_constraint);
        addInput(&d_perform_tests);
        addInput(&d_display_matrix_S);
        addInput(&d_export_matrix_S);
        addInput(&d_display_matrix_T);
        addInput(&d_export_matrix_T);
        addInput(&d_display_matrix_W);
        addInput(&d_export_matrix_W);
        addInput(&d_display_matrix_J);
        addInput(&d_export_matrix_J);

        addOutput(&d_W);
//        addOutput(&d_nbPosConstraints);
//        addOutput(&d_nbRegionConstraints);
    }




    ~JacobsonWeightComputationEngine()
    {

    }

};




}
}
}


#endif
