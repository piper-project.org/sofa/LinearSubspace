/******************************************************************************
*       SOFA, Simulation Open-Framework Architecture, development version     *
*                (c) 2006-2017 INRIA, USTL, UJF, CNRS, MGH                    *
*                                                                             *
* This program is free software; you can redistribute it and/or modify it     *
* under the terms of the GNU Lesser General Public License as published by    *
* the Free Software Foundation; either version 2.1 of the License, or (at     *
* your option) any later version.                                             *
*                                                                             *
* This program is distributed in the hope that it will be useful, but WITHOUT *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License *
* for more details.                                                           *
*                                                                             *
* You should have received a copy of the GNU Lesser General Public License    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.        *
*******************************************************************************
* Authors: The SOFA Team and external contributors (see Authors.txt)          *
*                                                                             *
* Contact information: contact@sofa-framework.org                             *
******************************************************************************/
#ifndef LINEARSUBSPACE_WeightApplicationEngine_H
#define LINEARSUBSPACE_WeightApplicationEngine_H


#include "LinearSubspace/config.h"

#include <sofa/helper/rmath.h>
#include <sofa/helper/OptionsGroup.h>
#include <sofa/helper/vector.h>
#include <sofa/helper/SVector.h>

#include <sofa/core/DataEngine.h>
#include <sofa/core/topology/BaseMeshTopology.h>

#include <algorithm>
#include <iostream>
#include <map>
#include <string>

#include <Eigen/Core>
#include <Eigen/Sparse>
#include <Eigen/Dense>
#include <Eigen/Geometry>


namespace Eigen {

template <typename RealT>
std::istream& operator>>( std::istream& in, Eigen::Matrix<RealT, Eigen::Dynamic, Eigen::Dynamic>& /*c*/ )
{
    msg_warning("Eigen::Matrix<RealT, Eigen::Dynamic, Eigen::Dynamic>") << "Eigen::Matrix<RealT, Eigen::Dynamic, Eigen::Dynamic> is not yet readable";
    std::string bidon;
    in >> bidon;
    return in;
}

template <typename RealT>
std::istream& operator>>( std::istream& in, Eigen::Transform<RealT, 3, Eigen::AffineCompact>& /*c*/ )
{
    msg_warning(" Eigen::Transform<RealT, 3, Eigen::AffineCompact>") <<  "Eigen::Transform<RealT, 3, Eigen::AffineCompact> is not yet readable";
    std::string bidon;
    in >> bidon;
    return in;
}

template <typename RealT>
std::ostream& operator<<( std::ostream& out, const Eigen::Transform<RealT, 3, Eigen::AffineCompact>& c )
{
    msg_warning("Eigen::Transform<RealT, 3, Eigen::AffineCompact>") << "Eigen::Transform<RealT, 3, Eigen::AffineCompact> is not yet writeable";
//    out << c;
    return out;
}
}



namespace sofa
{
namespace component
{
namespace engine
{

/**
    This Engine translates weights from the weight matrix computed in JacobsonWeightComputationEngine to weights used by LinearMapping.
*/
template <typename VecTypes, typename AffineTypes>
class SOFA_LinearSubspace_API WeightApplicationEngine : public core::DataEngine
{

public:
    typedef core::DataEngine Inherited;
    SOFA_CLASS(SOFA_TEMPLATE2(WeightApplicationEngine, VecTypes, AffineTypes) , Inherited);

    typedef typename VecTypes::VecCoord VecVCoord;
    typedef typename VecTypes::Coord VCoord;
    typedef typename VecTypes::Real VReal;
    typedef typename sofa::helper::vector<VReal> VecVReal;

    typedef typename AffineTypes::VecCoord VecACoord;
    typedef typename AffineTypes::Coord ACoord;
    typedef typename AffineTypes::Real AReal;
    typedef typename sofa::helper::vector<AReal> VecAReal;

    typedef Eigen::Matrix<VReal, Eigen::Dynamic, Eigen::Dynamic> DsMat;
//    typedef Eigen::Transform<Real, 3, Eigen::AffineCompact> AffineTransform;
//    typedef helper::vector<AffineTransform> VecAffineTransform;


    /// Input
    Data<DsMat> d_W;
    Data<VecVCoord> d_posConstraints;
    Data<VecACoord> d_affineConstraints;

    /// Output
    Data<VecVCoord> d_position;


    virtual std::string getTemplateName() const    { return templateName(this); }
    static std::string templateName(const WeightApplicationEngine<VecTypes, AffineTypes>* = NULL) { return std::string(VecTypes::Name())+std::string(",")+std::string(AffineTypes::Name()); }

    virtual void init()
    {
        setDirtyValue();
        reinit();
    }

    virtual void reinit()
    {
        helper::ReadAccessor< Data<DsMat> > W_accessor(d_W);
        const DsMat& W = W_accessor.ref();

        helper::ReadAccessor< Data<VecVCoord> > posConstraints_accessor(d_posConstraints);
        const VecVCoord& posConstraints = posConstraints_accessor.ref();

        helper::WriteAccessor< Data<VecACoord> > affineConstraints_accessor(d_affineConstraints);
        VecACoord& affineConstraints = affineConstraints_accessor.wref();

        uint nbAffineConstaints = affineConstraints.size();
        uint nbAffineConstaintsInW = (W.cols() - posConstraints.size())/4;

        if(nbAffineConstaints < nbAffineConstaintsInW)
            affineConstraints.resize(nbAffineConstaintsInW);

        update();
    }

    void update()
    {
        helper::ReadAccessor< Data<DsMat> > W_accessor(d_W);
        const DsMat& W = W_accessor.ref();

        helper::ReadAccessor< Data<VecVCoord> > posConstraints_accessor(d_posConstraints);
        const VecVCoord& posConstraints = posConstraints_accessor.ref();

        helper::ReadAccessor< Data<VecACoord> > affineConstraints_accessor(d_affineConstraints);
        const VecACoord& affineConstraints = affineConstraints_accessor.ref();

        helper::WriteOnlyAccessor< Data<VecVCoord> > position_accessor(d_position);
        VecVCoord& position = position_accessor.wref();
        
        uint nbPosConstaints = posConstraints.size();
        uint nbAffineConstaints = affineConstraints.size();
        uint nbAffineConstaintsInW = (W.cols() - nbPosConstaints)/4;
        uint h = nbPosConstaints + 4*nbAffineConstaintsInW;
        uint nbPos = W.rows();

        DsMat H(h, 3);
        for(uint i=0; i<nbPosConstaints; ++i)
        {
            for(uint j=0; j<3; ++j)
            {
                H(i, j) = posConstraints[i][j];
            }
        }

//        if(nbAffineConstaints < nbAffineConstaintsInW)
//        {
//            for(uint i=0; i<nbAffineConstaintsInW; ++i)
//            {
//                for(uint j=0; j<3; ++j)
//                {
//                    for(uint k=0; k<4; ++k)
//                    {
//                        if(j == k)
//                            H(nbPosConstaints + 4*i+k, j) = 1;
//                        else
//                            H(nbPosConstaints + 4*i+k, j) = 0;
//                    }
//                }
//            }
//        }
//        else
//        {
            for(uint i=0; i<nbAffineConstaints; ++i)
            {
                for(uint j=0; j<3; ++j)
                {
//                    for(uint k=0; k<4; ++k)
//                        H(nbPosConstaints + 4*i+k, j) = affineConstraints[i](j, k);
                    for(uint k=0; k<3; ++k)
                    {
                        H(nbPosConstaints + 4*i+k, j) = affineConstraints[i].getAffine()(j, k);
                    }
                    H(nbPosConstaints + 4*i+3, j) = affineConstraints[i].getCenter()(j);
                }
            }
//        }

        DsMat V = W * H;

        position.resize(nbPos);
        for(uint i=0; i<nbPos; ++i)
        {
            position[i][0] = V(i,0);
            position[i][1] = V(i,1);
            position[i][2] = V(i,2);
        }

    }


protected:
    WeightApplicationEngine() : Inherited()
      , d_W(initData(&d_W, DsMat(), "W", "Weight matrix"))
      , d_posConstraints(initData(&d_posConstraints, VecVCoord(), "posConstraints", "position constraints"))
      , d_affineConstraints(initData(&d_affineConstraints, VecACoord(), "affineConstraints", "affine constraints"))
      , d_position(initData(&d_position, VecVCoord(), "position", "output positions"))
    {
        addInput(&d_W);
        addInput(&d_posConstraints);
        addInput(&d_affineConstraints);

        addOutput(&d_position);
    }

    ~WeightApplicationEngine()
    {

    }

};




}
}
}


#endif
