/******************************************************************************
*       SOFA, Simulation Open-Framework Architecture, development version     *
*                (c) 2006-2017 INRIA, USTL, UJF, CNRS, MGH                    *
*                                                                             *
* This program is free software; you can redistribute it and/or modify it     *
* under the terms of the GNU Lesser General Public License as published by    *
* the Free Software Foundation; either version 2.1 of the License, or (at     *
* your option) any later version.                                             *
*                                                                             *
* This program is distributed in the hope that it will be useful, but WITHOUT *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License *
* for more details.                                                           *
*                                                                             *
* You should have received a copy of the GNU Lesser General Public License    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.        *
*******************************************************************************
* Authors: The SOFA Team and external contributors (see Authors.txt)          *
*                                                                             *
* Contact information: contact@sofa-framework.org                             *
******************************************************************************/
#ifndef LINEARSUBSPACE_JacobsonEnergyComputationEngine_H
#define LINEARSUBSPACE_JacobsonEnergyComputationEngine_H


#include "LinearSubspace/config.h"

#include <sofa/helper/rmath.h>
#include <sofa/helper/OptionsGroup.h>
#include <sofa/core/DataEngine.h>

#include <sofa/core/topology/BaseMeshTopology.h>

#include <algorithm>
#include <iostream>
#include <map>
#include <string>
#include <iostream>
#include <fstream>

#include <Eigen/Core>
#include <Eigen/Sparse>
#include <Eigen/Dense>

#include "LinearSubspace/utility/EigenStreams.h"
#include "LinearSubspace/utility/OctaveOutputs.h"


#define LINEARSUBSPACE_JacobsonEnergyComputationEngine_EPSILON (1e-5)
#define LINEARSUBSPACE_JacobsonEnergyComputationEngine_TERMINAL_DISPLAY    false

#if LINEARSUBSPACE_JacobsonEnergyComputationEngine_TERMINAL_DISPLAY
  #define OUT   std::cout
  #define ENDL  std::endl
#else
  #define OUT   sout
  #define ENDL  sendl
#endif


namespace sofa
{
namespace component
{
namespace engine
{

/**
    This Engine computes a linear precision energy on an input tetrahedral mesh
*/
template <typename DataTypes>
class SOFA_LinearSubspace_API JacobsonEnergyComputationEngine : public core::DataEngine
{

public:
    typedef core::DataEngine Inherited;
    SOFA_CLASS(SOFA_TEMPLATE(JacobsonEnergyComputationEngine, DataTypes) , Inherited);

    typedef typename DataTypes::Coord Coord;
    typedef typename DataTypes::VecCoord VecCoord;
    typedef typename DataTypes::Real Real;
    typedef typename DataTypes::VecReal VecReal;

    typedef Eigen::SparseMatrix<Real> SpMat;
    typedef Eigen::Matrix<Real, Eigen::Dynamic, Eigen::Dynamic> DsMat;
    typedef Eigen::Matrix<Real, 4, 4> Mat4;
    typedef Eigen::Triplet<Real> Triplet;

//    typedef typename sofa::helper::vector<Real> VecReal;
    typedef typename sofa::helper::vector<Triplet> VecTriplet;

    typedef core::topology::BaseMeshTopology::Ptr TopoPtr;
    typedef core::topology::BaseMeshTopology::Tetra Tetra;
    typedef core::topology::BaseMeshTopology::Edge Edge;
    typedef core::topology::BaseMeshTopology::Triangle Triangle;
    typedef core::topology::BaseMeshTopology::VerticesAroundVertex VerticesAroundVertex;
    typedef core::topology::BaseMeshTopology::EdgesAroundVertex EdgesAroundVertex;
    typedef core::topology::BaseMeshTopology::TrianglesAroundVertex TrianglesAroundVertex;
    typedef core::topology::BaseMeshTopology::TetrahedraAroundVertex TetrahedraAroundVertex;
    typedef core::topology::BaseMeshTopology::TetrahedraAroundEdge TetrahedraAroundEdge;
    typedef core::topology::BaseMeshTopology::EdgesInTetrahedron EdgesInTetrahedron;
    typedef core::topology::BaseMeshTopology::TrianglesInTetrahedron TrianglesInTetrahedron;


    typedef typename sofa::helper::fixed_array<Real, 6> Vec6;
    typedef typename sofa::helper::vector<Vec6> VecVec6;

    enum LaplacianScheme
    {
        Umbrella,
        Cotangent,
        NormDevPos,
        NormDevNeg
    };

    enum MassScheme
    {
        Ones,
        Barycentric,
        Voronoi,
        Test
    };


    /// Input
    Data<VecCoord> d_position;
    Data<VecReal> d_stiffness;

    /// Options
    Data<helper::OptionsGroup> d_laplacianScheme;
    Data<helper::OptionsGroup> d_massScheme;

    /// Switches
    Data<bool> d_verbose;
    Data<bool> d_perform_tests;

    Data<bool> d_display_laplacian_result;
    Data<bool> d_display_laplacian_computation;
    Data<bool> d_display_laplacian_matrix;

    Data<bool> d_display_energy_matrix;
    Data<bool> d_export_energy_matrix;

    Data<bool> d_display_mass_matrix;
    Data<bool> d_export_mass_matrix;

    Data<bool> d_display_adjacency_matrix;
    Data<bool> d_export_adjacency_matrix;

    /// Output
    Data<SpMat> d_Q;


    virtual std::string getTemplateName() const    { return templateName(this); }
    static std::string templateName(const JacobsonEnergyComputationEngine<DataTypes>* = NULL) { return DataTypes::Name(); }

    virtual void init()
    {
        addInput(&d_position);
        addInput(&d_stiffness);

        addInput(&d_laplacianScheme);
        addInput(&d_massScheme);

        addInput(&d_verbose);
        addInput(&d_perform_tests);
        addInput(&d_display_laplacian_result);
        addInput(&d_display_laplacian_computation);
        addInput(&d_display_laplacian_matrix);
        addInput(&d_display_energy_matrix);
        addInput(&d_export_energy_matrix);
        addInput(&d_display_mass_matrix);
        addInput(&d_export_mass_matrix);
        addInput(&d_display_adjacency_matrix);
        addInput(&d_export_adjacency_matrix);

        addOutput(&d_Q);

        setDirtyValue();
    }

    virtual void reinit()
    {
//        update();
    }

    void update()
    {
        if(d_verbose.getValue())
        {
            OUT << "Initiated energy computation." << ENDL;
        }

        helper::ReadAccessor< Data<VecCoord>> position_accessor(d_position);
        const VecCoord& position = position_accessor.ref();

        helper::ReadAccessor< Data<VecReal>> stiffness_accessor(d_stiffness);
        const VecReal& stiffness= stiffness_accessor.ref();

        helper::WriteOnlyAccessor< Data<SpMat> > Q_accessor = d_Q;
        SpMat& Q = Q_accessor.wref();

        TopoPtr topo = getContext()->getMeshTopology();

        if(topo == nullptr)
        {
            msg_error("JacobsonEnergyComputationEngine::update()") << "No MeshTopology were found in the local search.";
        }



        checkMesh(topo);

        uint nbVerts = topo->getNbPoints();
        uint nbEdges = topo->getNbEdges();
        uint nbTri = topo->getNbTriangles();
        uint nbTets = topo->getNbTetrahedra();

        if(d_verbose.getValue())
        {
            OUT << "  MeshTopology loaded.\nV=" << nbVerts << ", E=" << nbEdges << ", Tri=" << nbTri << ", Tet=" << nbTets << ENDL;
        }


        SpMat K(nbTri, nbTri);      /// stores the modified (linearly precise) Laplacian matrix
        SpMat Minv(nbTri, nbTri);   /// stores the mass matrix
        SpMat A(nbTri, nbVerts);    /// stores face-vertex adjacency

        VecTriplet tripletList;     /// used for filling sparse matrices


        if(d_verbose.getValue())
        {
            OUT << "  Computing laplacian matrix..." << ENDL;
        }
//        if(d_laplacianScheme.getValue().getSelectedId() == LaplacianScheme::Umbrella)
//        {
//            if(d_verbose.getValue())
//            {
//                OUT << "    Scheme: Umbrella." << ENDL;
//            }

//            /// Umbrella laplacian matrix computation
//            tripletList.clear();
//            tripletList.reserve(nbEdges*4);
//            computeUmbrellaEdgeLaplacianEntries(topo, tripletList);
//        }
//        else
        {
//            tripletList.clear();
//            tripletList.reserve(4*3*2*nbTets);
            computeCotanFaceLaplacianEntries(topo, position, tripletList);

            computeFaceNormalDerivativeEntries(topo, position, tripletList);

//            if(d_laplacianScheme.getValue().getSelectedId() == LaplacianScheme::NormDevPos)
//            {
//                if(d_verbose.getValue())
//                {
//                    OUT << "    Scheme: Cotangent + Normal derivative." << ENDL;
//                }
//                computeEdgeNormalDerivativeEntries_pos(topo, position, tripletList);
//            }
//            else if(d_laplacianScheme.getValue().getSelectedId() == LaplacianScheme::NormDevNeg)
//            {
//                if(d_verbose.getValue())
//                {
//                    OUT << "    Scheme: Cotangent - Normal derivative." << ENDL;
//                }
//                computeEdgeNormalDerivativeEntries_neg(topo, position, tripletList);
//            }
//            else
            {
                if(d_verbose.getValue())
                {
                    OUT << "    Scheme: Cotangent." << ENDL;
                }
            }
        }

        K.setFromTriplets(tripletList.begin(), tripletList.end());
        tripletList.clear();

        if(d_display_laplacian_matrix.getValue())
            OUT << "  K = " << ENDL << K << ENDL;



        /// Adjacency matrix computation

        computeVertexFaceAdjacencyMatrix(topo, tripletList);
        A.setFromTriplets(tripletList.begin(), tripletList.end());
        tripletList.clear();

        if(d_display_adjacency_matrix.getValue())
            OUT << "  A = " << ENDL << A << ENDL;

        if(d_export_adjacency_matrix.getValue())
            exportOctaveSparseMat<Real>(A, "../../../temp/matrix/A.mat", "A");



        /// Mass matrix computation
        tripletList.reserve(nbTri);

//        if(d_massScheme.getValue().getSelectedId() == MassScheme::Barycentric)
//            msg_warning("JacobsonEnergyComputationEngine") << "Barycentric mass scheme not implemented yet, falling back on ones.";
        if(d_massScheme.getValue().getSelectedId() == MassScheme::Voronoi)
            msg_warning("JacobsonEnergyComputationEngine") << "Voronoi mass scheme not implemented yet, falling back on ones.";

        bool has_stiffness = stiffness.size() == position.size();
        if(has_stiffness)
        {
            OUT << "Using input stiffness." << ENDL;
        }
        else
        {
            OUT << "No valid stiffness were provided, using uniform stiffness distribution (default)." << ENDL;
        }

        for(uint i=0; i<nbTri; i++)
        {
            Real face_mass = 1.0;
            switch (d_massScheme.getValue().getSelectedId())
            {
            case MassScheme::Ones:
                break;
            case MassScheme::Barycentric:
                face_mass=0;
                for(uint j=0; j<topo->getTetrahedraAroundTriangle(i).size(); ++j)
                {
                    // getting vertices positions
                    Coord p0 = position[topo->getTetra(topo->getTetrahedraAroundTriangle(i)[j])[0]];
                    Coord p1 = position[topo->getTetra(topo->getTetrahedraAroundTriangle(i)[j])[1]];
                    Coord p2 = position[topo->getTetra(topo->getTetrahedraAroundTriangle(i)[j])[2]];
                    Coord p3 = position[topo->getTetra(topo->getTetrahedraAroundTriangle(i)[j])[3]];

                    Real vol = std::abs(computeTetraVolume(p0, p1, p2, p3));   // each vertex inherits a quarter of the tetrahdron volume
                    face_mass += vol*0.25;

                }

                break;
            case MassScheme::Voronoi:
                break;
            default:
                break;
            }


            if(has_stiffness)
            {
                Real faceStiffness = 0;
                for(uint j=0; j<3; ++j)
                {
                    Real vertStiffness = stiffness[topo->getTriangle(i)[j]];

                    faceStiffness += vertStiffness;
                }
                faceStiffness /= 3.0;
                face_mass *= faceStiffness;
            }

            tripletList.push_back(Triplet(i, i, 1.0/face_mass));
        }
        Minv.setFromTriplets(tripletList.begin(), tripletList.end());
        tripletList.clear();

        if(d_display_mass_matrix.getValue())
            OUT << "  M^(-1) = " << ENDL << Minv << ENDL;

        if(d_export_mass_matrix.getValue())
            exportOctaveSparseMat<Real>(A, "../../../temp/matrix/Minv.mat", "Minv");



        /// Energy matrix computation
        Q = A.transpose() * K.transpose() * Minv * K * A;

        if(d_display_energy_matrix.getValue())
            OUT << "Q = " << ENDL << Q << ENDL;

        if(d_export_energy_matrix.getValue())
            exportOctaveSparseMat<Real>(Q, "../../../temp/matrix/Q.mat", "Q_cpp");

        if(this->f_printLog.getValue())  OUT<<this->getName()<<" updated."<<ENDL;

        if(d_perform_tests.getValue())
        {
//            OUT << "L = " << ENDL << L << ENDL;
//            OUT << "Q = " << ENDL << Q << ENDL;

            Eigen::VectorXd Ones(nbVerts);
            Ones.setOnes(nbVerts);
            Eigen::VectorXd resultOnes = Q * Ones;
//            if(d_verbose.getValue())
//            {
//                OUT << "Q.1 = " << ENDL << resultOnes << ENDL;
//            }
//            else
            {
                Real result = resultOnes.transpose() * resultOnes;
                OUT << "  1'.Q'.Q.1 = " << result << ENDL;

                if(result > LINEARSUBSPACE_JacobsonEnergyComputationEngine_EPSILON)
                    msg_warning("JacobsonEnergyComputationEngine::update()") << "Constant precision test : failure.";
                else
                    msg_info("JacobsonEnergyComputationEngine::update()") << "Constant precision test : success.";
            }

            DsMat V(nbVerts, 3);
            for(uint i=0; i<(nbVerts); ++i)
            {
                V(i, 0) = topo->getPX(i);
                V(i, 1) = topo->getPY(i);
                V(i, 2) = topo->getPZ(i);
            }

            DsMat QV = Q * V;
//            if(d_verbose.getValue())
//            {
//                OUT << "    Q.V = " << ENDL << QV << ENDL;
//                OUT << "    Q.V = " << ENDL;
//                for(uint i=0; i<nbVerts; ++i)
//                {
//                    OUT << QV(i, 0) << "\t" << QV(i, 1) << "\t" << QV(i, 2);
//                    if(isAtBorder(topo, i))
//                        OUT << "\t*";
//                    OUT << ENDL;
//                }

//            }
//            else
//            {
//                OUT << "  V'.Q'.Q.V = " << ENDL << QV.transpose() * QV << ENDL;
                Real result = (QV.transpose() * QV).trace();
                OUT << "  tr(V'.Q'.Q.V) = " << result << ENDL;

                if(result > LINEARSUBSPACE_JacobsonEnergyComputationEngine_EPSILON)
                    msg_warning("JacobsonEnergyComputationEngine::update()") << "Linear precision test : failure.";
                else
                    msg_info("JacobsonEnergyComputationEngine::update()") << "Linear precision test : success.";
//            }
        }

        if(d_display_laplacian_result.getValue())
        {
            typedef Eigen::Matrix<Real, Eigen::Dynamic, Eigen::Dynamic> DsMat;
            DsMat V(nbVerts, 3);
            for(uint i=0; i<(nbVerts); ++i)
            {
                V(i, 0) = topo->getPX(i);
                V(i, 1) = topo->getPY(i);
                V(i, 2) = topo->getPZ(i);
            }

            DsMat KV = K * V;
            OUT << "  K.V = " << ENDL;
            OUT << KV << ENDL;
        }


        cleanDirty();
    }

    Real computeTetraVolume(const Coord& p0, const Coord& p1, const Coord& p2, const Coord& p3)
    {
        Mat4 m;
        m  << p0[0] , p0[1] , p0[2] , 1.0
            , p1[0] , p1[1] , p1[2] , 1.0
            , p2[0] , p2[1] , p2[2] , 1.0
            , p3[0] , p3[1] , p3[2] , 1.0;

        return m.determinant();
    }


    uint getOppositeEdge(TopoPtr topo, const uint target_ei, const EdgesInTetrahedron& edgeSet)
    {
        const Edge& target_edge = topo->getEdge(target_ei);
        for(uint ei : edgeSet)
        {
            const Edge& edge = topo->getEdge(ei);
            if( edge[0] != target_edge[0]
            &&  edge[0] != target_edge[1]
            &&  edge[1] != target_edge[0]
            &&  edge[1] != target_edge[1] )
                return ei;
        }

        msg_warning("JacobsonEnergyComputationEngine::getOppositeEdge()") << "opposite edge not found.";

        return 0;
    }


    uint getOppositeVertex(TopoPtr topo, const uint target_fi, const uint& target_ti)
    {
        uint vi = topo->getTetra(target_ti)[0];

        if(topo->getTriangle(target_fi)[0] == vi || topo->getTriangle(target_fi)[1] == vi || topo->getTriangle(target_fi)[2] == vi)
        {
            vi = topo->getTetra(target_ti)[1];
        }
        if(topo->getTriangle(target_fi)[0] == vi || topo->getTriangle(target_fi)[1] == vi || topo->getTriangle(target_fi)[2] == vi)
        {
            vi = topo->getTetra(target_ti)[2];
        }
        if(topo->getTriangle(target_fi)[0] == vi || topo->getTriangle(target_fi)[1] == vi || topo->getTriangle(target_fi)[2] == vi)
        {
            vi = topo->getTetra(target_ti)[3];
        }

        return vi;
    }

    Real computeDihedralAngle(TopoPtr topo, const VecCoord& position, const uint target_ei, const uint target_ti)
    {
        uint v0 = topo->getEdge(target_ei)[0];

        uint v1 = topo->getEdge(target_ei)[1];

        uint v2 = topo->getTetra(target_ti)[0];
        if(v2 == v0 || v2 == v1)
            v2 = topo->getTetra(target_ti)[1];
        if(v2 == v0 || v2 == v1)
            v2 = topo->getTetra(target_ti)[2];

        uint v3 = topo->getTetra(target_ti)[0];
        if(v3 == v0 || v3 == v1 || v3 == v2)
            v3 = topo->getTetra(target_ti)[1];
        if(v3 == v0 || v3 == v1 || v3 == v2)
            v3 = topo->getTetra(target_ti)[2];
        if(v3 == v0 || v3 == v1 || v3 == v2)
            v3 = topo->getTetra(target_ti)[3];

        Coord n012 = (position[v1] - position[v0]).cross(position[v2] - position[v0]).normalized();
        Coord n013 = (position[v1] - position[v0]).cross(position[v3] - position[v0]).normalized();

        if(n012*(position[v3] - (position[v0]+position[v1]+position[v2])/3.0) < 0.0)
        {
            n012 *= -1;
        }
        if(n013*(position[v2] - (position[v0]+position[v1]+position[v3])/3.0) < 0.0)
        {
            n013 *= -1;
        }

        Real theta2 = acos(n012*n013);

        Real theta = M_PI - theta2;

        return theta;
    }

    Real computeEdgeLength(TopoPtr topo, const VecCoord& position, const uint target_ei)
    {
        uint v0 = topo->getEdge(target_ei)[0];
        uint v1 = topo->getEdge(target_ei)[1];

        Real length = (position[v1] - position[v0]).norm();

        return length;
    }

    bool isVertexAtBorder(TopoPtr topo, const uint vertID)
    {
        core::topology::BaseMeshTopology::TrianglesAroundVertex t = topo->getTrianglesAroundVertex(vertID);
        for(uint i=0; i<t.size(); ++i)
        {
            if(isTriangleAtBorder(topo, t[i]))
                return true;
        }
        return false;
    }

    bool isTriangleAtBorder(TopoPtr topo, const uint triID)
    {
        return topo->getTetrahedraAroundTriangle(triID).size() == 1;
    }

    void computeUmbrellaEdgeLaplacianEntries(TopoPtr topo, VecTriplet& tripletList)
    {
        uint nbEdges = topo->getNbEdges();

        for(uint i=0; i<nbEdges; i++)
        {
            tripletList.push_back(Triplet(topo->getEdge(i)[0], topo->getEdge(i)[1],  1.0));
            tripletList.push_back(Triplet(topo->getEdge(i)[0], topo->getEdge(i)[0], -1.0));

            tripletList.push_back(Triplet(topo->getEdge(i)[1], topo->getEdge(i)[0],  1.0));
            tripletList.push_back(Triplet(topo->getEdge(i)[1], topo->getEdge(i)[1], -1.0));
        }
    }

    void computeCotanEdgeLaplacianEntries(TopoPtr topo, const VecCoord& position, VecTriplet& tripletList)
    {
        uint nbVerts = topo->getNbPoints();

        for(uint i=0; i<nbVerts; ++i)
        {
            TetrahedraAroundVertex tets = topo->getTetrahedraAroundVertex(i);

            if(d_display_laplacian_computation.getValue())
                OUT << "i : " << i << ENDL;

            for(uint tet_index=0; tet_index<tets.size(); ++tet_index)
            {
                uint tetID =  tets[tet_index];
                EdgesInTetrahedron edges = topo->getEdgesInTetrahedron(tetID);
                if(d_display_laplacian_computation.getValue())
                    OUT << "  tetra : " << tetID << ENDL;
                for(uint vert_index=0; vert_index<4; ++vert_index)
                {
                    uint j = topo->getTetra(tetID)[vert_index];
                    if(j == i)
                    {
                        continue;
                    }
                    if(d_display_laplacian_computation.getValue())
                        OUT << "    j : " << j << ENDL;

                    uint edge = topo->getEdgeIndex(i, j);

                    if(d_display_laplacian_computation.getValue())
                        OUT << "       edge : " << edge << ENDL;

                    uint oppositeEdge = getOppositeEdge(topo, edge, edges);

                    if(d_display_laplacian_computation.getValue())
                        OUT << "       opposite edge : " << oppositeEdge << ENDL;

                    Real gamma_ij = computeDihedralAngle(topo, position, oppositeEdge, tetID);
                    Real l_ij = computeEdgeLength(topo, position, oppositeEdge);
                    Real w_ij = l_ij / std::tan(gamma_ij) / 6.0;
                    if(d_display_laplacian_computation.getValue())
                    {
                        OUT << "       gamma_ij  : " << gamma_ij << ENDL;
                        OUT << "       l_ij : " << l_ij << ENDL;
                        OUT << "       w_ij : " << w_ij << ENDL;
                    }

                    tripletList.push_back(Triplet(i, i,  w_ij));
                    tripletList.push_back(Triplet(i, j, -w_ij));
                }
            }
        }
    }

    void computeCotanFaceLaplacianEntries(TopoPtr topo, const VecCoord& position, VecTriplet& tripletList)
    {
        uint nbTets = topo->getNbTetrahedra();

        tripletList.clear();
        tripletList.reserve(4*2*3*nbTets);

        for(uint i=0; i<nbTets; ++i)
        {
            TrianglesInTetrahedron tris = topo->getTrianglesInTetrahedron(i);

            for(uint tri_index_0=0; tri_index_0<tris.size(); ++tri_index_0)
            {
                for(uint tri_index_1=0; tri_index_1<tris.size(); ++tri_index_1)
                {
                    if(tri_index_1 == tri_index_0)
                        continue;

                    uint common_edge = findCommonEdge(topo, tris[tri_index_0], tris[tri_index_1]);

                    Real gamma_ij = computeDihedralAngle(topo, position, common_edge, i);
                    Real l_ij = computeEdgeLength(topo, position, common_edge);
                    Real w_ij = l_ij / std::tan(gamma_ij) / 6.0;

                    tripletList.push_back(Triplet(tris[tri_index_0], tris[tri_index_0],  w_ij));
                    tripletList.push_back(Triplet(tris[tri_index_0], tris[tri_index_1], -w_ij));
                }
            }
        }
    }

    uint findCommonEdge(TopoPtr topo, const uint& i_t0, const uint& i_t1)
    {
        if(i_t0 == i_t1)
            msg_warning("EnergyComputationEngine::findCommonEdge()") << "Same triangle in t0 and t1.";

        Triangle t0 = topo->getTriangle(i_t0);
        Triangle t1 = topo->getTriangle(i_t1);

        uint common_vertex_0 = t0[0];
        if(t1[0] != common_vertex_0 && t1[1] != common_vertex_0 && t1[2] != common_vertex_0)
            common_vertex_0 = t0[1];

        if(t1[0] != common_vertex_0 && t1[1] != common_vertex_0 && t1[2] != common_vertex_0)
            msg_warning("EnergyComputationEngine::findCommonEdge()") << "t0 and t1 have less than 2 vertices in common.";

        uint common_vertex_1 = t0[1];

        if(common_vertex_0 == common_vertex_1 || (t1[0] != common_vertex_1 && t1[1] != common_vertex_1 && t1[2] != common_vertex_1))
            common_vertex_1 = t0[2];

        if(t1[0] != common_vertex_1 && t1[1] != common_vertex_1 && t1[2] != common_vertex_1)
            msg_warning("EnergyComputationEngine::findCommonEdge()") << "t0 and t1 have less than 2 vertices in common.";

        return topo->getEdgeIndex(common_vertex_0, common_vertex_1);
    }


    void computeVertexFaceAdjacencyMatrix(TopoPtr topo, VecTriplet& tripletList)
    {
        uint nbTri = topo->getNbTriangles();

        tripletList.clear();
        tripletList.reserve(3*nbTri);

        for(uint i=0; i<nbTri; ++i)
        {
            for(uint j=0; j<3; ++j)
            {
                tripletList.push_back(Triplet(i, topo->getTriangle(i)[j],  1.0/3.0));
            }
        }
    }



    void computeEdgeNormalDerivativeEntries_pos(TopoPtr topo, const VecCoord& position, VecTriplet& tripletList)
    {
        uint nbVerts = topo->getNbPoints();

        for(uint i=0; i<nbVerts; i++)
        {

            if(d_display_laplacian_computation.getValue())
                OUT << "i : " << i << ENDL;

            TrianglesAroundVertex tris = topo->getTrianglesAroundVertex(i);
            for(uint tri_index=0; tri_index<tris.size(); ++tri_index)
            {
                uint F = tris[tri_index];
                if(!isTriangleAtBorder(topo, F))
                {
                    continue;
                }

                if(d_display_laplacian_computation.getValue())
                    OUT << "  tri : " << F << " [on border]" << ENDL;

                uint tetID = topo->getTetrahedraAroundTriangle(F)[0];
                if(d_display_laplacian_computation.getValue())
                    OUT << "    tet : " << tetID << ENDL;

                EdgesInTetrahedron edges = topo->getEdgesInTetrahedron(tetID);
                uint f = getOppositeVertex(topo, F, tetID);
                if(d_display_laplacian_computation.getValue())
                    OUT << "    f : " << f << ENDL;

                for(uint k=0; k<3; ++k)
                {
                    uint j = topo->getTriangle(F)[k];
                    if(d_display_laplacian_computation.getValue())
                        OUT << "      j : " << j << ENDL;

                    uint edge = topo->getEdgeIndex(f, topo->getTriangle(F)[k]);
                    uint oppositeEdge = getOppositeEdge(topo, edge, edges);

                    Real gamma_fj = computeDihedralAngle(topo, position, oppositeEdge, tetID);
                    Real l_fj = computeEdgeLength(topo, position, oppositeEdge);
                    Real w_fj = l_fj / std::tan(gamma_fj) / 6.0;
                    if(d_display_laplacian_computation.getValue())
                        OUT << "      w_fj : " << w_fj << ENDL;

                    tripletList.push_back(Triplet(i, j,  w_fj));
                    tripletList.push_back(Triplet(i, f, -w_fj));
                }
            }
        }
    }


    void computeEdgeNormalDerivativeEntries_neg(TopoPtr topo, const VecCoord& position, VecTriplet& tripletList)
    {
        uint nbVerts = topo->getNbPoints();

        for(uint i=0; i<nbVerts; i++)
        {

            if(d_display_laplacian_computation.getValue())
                OUT << "i : " << i << ENDL;

            TrianglesAroundVertex tris = topo->getTrianglesAroundVertex(i);
            for(uint tri_index=0; tri_index<tris.size(); ++tri_index)
            {
                uint F = tris[tri_index];
                if(!isTriangleAtBorder(topo, F))
                {
                    continue;
                }

                if(d_display_laplacian_computation.getValue())
                    OUT << "  tri : " << F << " [on border]" << ENDL;

                uint tetID = topo->getTetrahedraAroundTriangle(F)[0];
                if(d_display_laplacian_computation.getValue())
                    OUT << "    tet : " << tetID << ENDL;

                EdgesInTetrahedron edges = topo->getEdgesInTetrahedron(tetID);
                uint f = getOppositeVertex(topo, F, tetID);
                if(d_display_laplacian_computation.getValue())
                    OUT << "    f : " << f << ENDL;

                for(uint k=0; k<3; ++k)
                {
                    uint j = topo->getTriangle(F)[k];
                    if(d_display_laplacian_computation.getValue())
                        OUT << "      j : " << j << ENDL;

                    uint edge = topo->getEdgeIndex(f, topo->getTriangle(F)[k]);
                    uint oppositeEdge = getOppositeEdge(topo, edge, edges);

                    Real gamma_fj = computeDihedralAngle(topo, position, oppositeEdge, tetID);
                    Real l_fj = computeEdgeLength(topo, position, oppositeEdge);
                    Real w_fj = -l_fj / std::tan(gamma_fj) / 6.0;
                    if(d_display_laplacian_computation.getValue())
                        OUT << "      w_fj : " << w_fj << ENDL;

                    tripletList.push_back(Triplet(i, j,  w_fj));
                    tripletList.push_back(Triplet(i, f, -w_fj));
                }
            }
        }
    }


    void computeFaceNormalDerivativeEntries(TopoPtr topo, const VecCoord& position, VecTriplet& tripletList)
    {
        uint nbTri = topo->getNbTriangles();

        for(uint i=0; i<nbTri; i++)
        {
            if(!isTriangleAtBorder(topo, i))
            {
                continue;
            }

            uint tetID = topo->getTetrahedraAroundTriangle(i)[0];

            TrianglesInTetrahedron tri = topo->getTrianglesInTetrahedron(tetID);

            for(uint k=0; k<4; ++k)
            {
                if(tri[k] == i)
                    continue;

                uint common_edge = findCommonEdge(topo, i, tri[k]);

                Real gamma_ij = computeDihedralAngle(topo, position, common_edge, tetID);
                Real l_ij = computeEdgeLength(topo, position, common_edge);
                Real w_ij = -l_ij / std::tan(gamma_ij) / 6.0;

                tripletList.push_back(Triplet(i, i,  w_ij));
                tripletList.push_back(Triplet(i, tri[k], -w_ij));
            }
        }
    }





    void checkMesh(TopoPtr topo) const
    {
        uint nbVerts = topo->getNbPoints();
        uint nbEdges = topo->getNbEdges();
        uint nbTri = topo->getNbTriangles();
        uint nbTets = topo->getNbTetrahedra();

        for(uint i=0; i<nbVerts; ++i)
        {
            if(topo->getTetrahedraAroundVertex(i).size() == 0)
            {
                OUT << "vertex #" << i << " has no adjacent tetrahedra!" << ENDL;
            }
        }
    }


protected:
    JacobsonEnergyComputationEngine() : Inherited()
      , d_position(initData(&d_position, VecCoord(), "position", "Positions of the vertices of the mesh"))
      , d_stiffness(initData(&d_stiffness, VecReal(), "stiffness", "Per-vertex stiffness (optional)"))
      , d_laplacianScheme(initData(&d_laplacianScheme, "laplacianScheme", "Scheme used for computing the laplacian"))
      , d_massScheme(initData(&d_massScheme, "massScheme", "Scheme used for computing vertice masses"))
      , d_verbose                       (initData(&d_verbose                        , true , "verbose", "verbose"))
      , d_perform_tests                 (initData(&d_perform_tests                  , true , "perform_tests", "perform_tests"))
      , d_display_laplacian_result      (initData(&d_display_laplacian_result       , false, "display_laplacian_result", "display_laplacian_result"))
      , d_display_laplacian_computation (initData(&d_display_laplacian_computation  , false, "display_laplacian_computation", "display_laplacian_computation"))
      , d_display_laplacian_matrix      (initData(&d_display_laplacian_matrix       , false, "display_laplacian_matrix", "display_laplacian_matrix"))
      , d_display_energy_matrix         (initData(&d_display_energy_matrix          , false, "display_energy_matrix", "display_energy_matrix"))
      , d_export_energy_matrix          (initData(&d_export_energy_matrix           , true , "export_energy_matrix", "export_energy_matrix"))
      , d_display_mass_matrix           (initData(&d_display_mass_matrix            , false, "display_mass_matrix", "display_mass_matrix"))
      , d_export_mass_matrix            (initData(&d_export_mass_matrix             , true , "export_mass_matrix", "export_mass_matrix"))
      , d_display_adjacency_matrix      (initData(&d_display_adjacency_matrix       , false, "display_adjacency_matrix", "display_adjacency_matrix"))
      , d_export_adjacency_matrix       (initData(&d_export_adjacency_matrix        , true , "export_adjacency_matrix", "export_adjacency_matrix"))
      , d_Q(initData(&d_Q, SpMat(), "Q","Energy matrix of the mesh"))
    {
        helper::OptionsGroup laplacianSchemeOptions(4, "0 - Umbrella"
                                                     , "1 - Cotangent"
                                                     , "2 - Cotangent+normal derivative"
                                                     , "3 - Cotangent-normal derivative"
                                                     );
        laplacianSchemeOptions.setSelectedItem(LaplacianScheme::Umbrella);
        d_laplacianScheme.setValue(laplacianSchemeOptions);

        helper::OptionsGroup massSchemeOptions(4, "0 - Ones"
                                                , "1 - Barycentric"
                                                , "2 - Voronoi"
                                                , "3 - Test"
                                                );
        massSchemeOptions.setSelectedItem(MassScheme::Ones);
        d_massScheme.setValue(massSchemeOptions);
    }

    ~JacobsonEnergyComputationEngine()
    {

    }

};




}
}
}


#endif
