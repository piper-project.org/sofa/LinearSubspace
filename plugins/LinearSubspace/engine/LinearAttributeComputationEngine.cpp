/******************************************************************************
*       SOFA, Simulation Open-Framework Architecture, development version     *
*                (c) 2006-2017 INRIA, USTL, UJF, CNRS, MGH                    *
*                                                                             *
* This program is free software; you can redistribute it and/or modify it     *
* under the terms of the GNU Lesser General Public License as published by    *
* the Free Software Foundation; either version 2.1 of the License, or (at     *
* your option) any later version.                                             *
*                                                                             *
* This program is distributed in the hope that it will be useful, but WITHOUT *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License *
* for more details.                                                           *
*                                                                             *
* You should have received a copy of the GNU Lesser General Public License    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.        *
*******************************************************************************
* Authors: The SOFA Team and external contributors (see Authors.txt)          *
*                                                                             *
* Contact information: contact@sofa-framework.org                             *
******************************************************************************/
#define LINEARSUBSPACE_LinearEnergyComputationEngine_CPP

#include <LinearSubspace/config.h>
#include "LinearAttributeComputationEngine.h"
#include <sofa/core/ObjectFactory.h>

namespace sofa
{
namespace component
{
namespace engine
{

using namespace defaulttype;
using namespace core::behavior;

SOFA_DECL_CLASS(LinearAttributeComputationEngine)

// Register in the Factory
int LinearAttributeComputationEngineClass = core::RegisterObject("Computes linear attribute based on positions")

        .add< LinearAttributeComputationEngine<Vec3Types> >(true)
        ;

template class SOFA_LinearSubspace_API LinearAttributeComputationEngine<Vec3Types>;

}
}
}
