/******************************************************************************
*       SOFA, Simulation Open-Framework Architecture, development version     *
*                (c) 2006-2017 INRIA, USTL, UJF, CNRS, MGH                    *
*                                                                             *
* This program is free software; you can redistribute it and/or modify it     *
* under the terms of the GNU Lesser General Public License as published by    *
* the Free Software Foundation; either version 2.1 of the License, or (at     *
* your option) any later version.                                             *
*                                                                             *
* This program is distributed in the hope that it will be useful, but WITHOUT *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License *
* for more details.                                                           *
*                                                                             *
* You should have received a copy of the GNU Lesser General Public License    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.        *
*******************************************************************************
* Authors: The SOFA Team and external contributors (see Authors.txt)          *
*                                                                             *
* Contact information: contact@sofa-framework.org                             *
******************************************************************************/
#ifndef LINEARSUBSPACE_LinearAttributeComputationEngine_H
#define LINEARSUBSPACE_LinearAttributeComputationEngine_H


#include "LinearSubspace/config.h"

#include <sofa/helper/rmath.h>
#include <sofa/core/DataEngine.h>

#include <algorithm>
#include <iostream>
#include <string>
#include <limits>



namespace sofa
{
namespace component
{
namespace engine
{

template <typename DataTypes>
class SOFA_LinearSubspace_API LinearAttributeComputationEngine : public core::DataEngine
{

public:
    typedef core::DataEngine Inherited;
    SOFA_CLASS(SOFA_TEMPLATE(LinearAttributeComputationEngine, DataTypes) , Inherited);

    typedef typename DataTypes::Coord Coord;
    typedef typename DataTypes::VecCoord VecCoord;
    typedef typename DataTypes::Real Real;
    typedef typename DataTypes::VecReal VecReal;

    /// Input
    Data<VecCoord> d_position;
    Data<Coord> d_axis;
    Data<Real> d_minValue;
    Data<Real> d_maxValue;

    Data<VecReal> d_interPos;
    Data<VecReal> d_interValue;

    /// Output
    Data<VecReal> d_attribute;


    virtual std::string getTemplateName() const    { return templateName(this); }
    static std::string templateName(const LinearAttributeComputationEngine<DataTypes>* = NULL) { return DataTypes::Name(); }

    virtual void init()
    {
        addInput(&d_position);
        addInput(&d_axis);
        addInput(&d_minValue);
        addInput(&d_maxValue);
        addInput(&d_interPos);
        addInput(&d_interValue);

        addOutput(&d_attribute);

        setDirtyValue();
    }

    virtual void reinit()
    {
//        update();
    }

    void update()
    {

        helper::ReadAccessor< Data<VecCoord>> position_accessor(d_position);
        const VecCoord& position = position_accessor.ref();

        Coord axis = d_axis.getValue();
        Real minVal = d_minValue.getValue();
        Real maxVal = d_maxValue.getValue();


        helper::ReadAccessor< Data<VecReal>> interPos_accessor(d_interPos);
        const VecReal& interPos = interPos_accessor.ref();

        helper::ReadAccessor< Data<VecReal>> interVal_accessor(d_interValue);
        const VecReal& interVal = interVal_accessor.ref();

        helper::WriteAccessor< Data<VecReal>> attribute_accessor(d_attribute);
        VecReal& attribute = attribute_accessor.wref();

        attribute.resize(position.size());
        Real min_attrib = std::numeric_limits<Real>::infinity();
        Real max_attrib = -std::numeric_limits<Real>::infinity();
        for(uint i=0; i<position.size(); ++i)
        {
            attribute[i] = position[i] * axis;

            if(min_attrib  > attribute[i])
                min_attrib = attribute[i];

            if(max_attrib  < attribute[i])
                max_attrib = attribute[i];
        }

        assert(interPos.size() == interVal.size());


        bool has_intermediate = (interPos.size() != 0);

        VecReal interPos_;
        VecReal interVal_;
        if(has_intermediate)
        {
            interPos_.resize(interPos.size()+2);
            interVal_.resize(interVal.size()+2);
            interPos_[0] = 0;
            interPos_[interPos_.size()-1] = 1;
            std::copy(interPos.begin(), interPos.end(), interPos_.begin()+1);
            interVal_[0] = minVal;
            interVal_[interVal_.size()-1] = maxVal;
            std::copy(interVal.begin(), interVal.end(), interVal_.begin()+1);

            // TODO : sort positions and values
        }

        for(uint i=0; i<position.size(); ++i)
        {
            attribute[i] = (attribute[i]-min_attrib)/(max_attrib-min_attrib);

            if(!has_intermediate)
            {
                attribute[i] = attribute[i] * (maxVal-minVal) + minVal;
            }
            else
            {
                uint j;
                for(j=interPos_.size()-1; j>0; --j)
                {
                    if(attribute[i] > interPos_[j])
                    {
                        break;
                    }
                }

                assert(attribute[i] <= interPos_[j+1]);

                attribute[i] = (attribute[i]-interPos_[j])/(interPos_[j+1]-interPos_[j]) * (interVal_[j+1]-interVal_[j]) + interVal_[j];
            }
        }

        cleanDirty();
    }

protected:
    LinearAttributeComputationEngine() : Inherited()
      , d_position(initData(&d_position, VecCoord(), "position", "Positions where the attribute should be computed"))
      , d_axis(initData(&d_axis, Coord(1.0, 0.0, 0.0), "axis", "axis along which to compute the attribute"))
      , d_minValue(initData(&d_minValue, Real(0.0), "min", "minimal value of the attribute"))
      , d_maxValue(initData(&d_maxValue, Real(1.0), "max", "maximal value of the attribute"))
      , d_interPos(initData(&d_interPos, VecReal(), "interPos", "intermediate C0 locations (optional)"))
      , d_interValue(initData(&d_interValue, VecReal(), "interVal", "intermediate C0 values (optional)"))
      , d_attribute(initData(&d_attribute, VecReal(), "attribute", "attribute computed by the engine"))
    {
    }

    ~LinearAttributeComputationEngine()
    {

    }

};




}
}
}


#endif
