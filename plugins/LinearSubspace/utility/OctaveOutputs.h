

#include "LinearSubspace/config.h"

#include <Eigen/Core>
#include <Eigen/Sparse>
#include <Eigen/Dense>


//typedef Eigen::SparseMatrix<double> SpMat;
//typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> DsMat;

template <typename RealT>
void exportOctaveSparseMat(const Eigen::SparseMatrix<RealT>& M, const std::string& filename, const std::string& matrixName)
{
//    OUT << "Exporting sparse matrix into '" << filename << "'." << ENDL;
    std::ofstream fs;
    fs.open(filename);
    fs << "# File written by exportOctaveSparseMat() \n" ;
    fs << "# name: " << matrixName << " \n" ;
    fs << "# type: sparse matrix \n" ;
    fs << "# nnz: " << M.nonZeros() << " \n" ;
    fs << "# rows: " << M.rows() << " \n" ;
    fs << "# columns: " << M.cols() << " \n" ;

    for (int k=0; k<M.outerSize(); ++k)
    {
        for(typename Eigen::SparseMatrix<RealT>::InnerIterator it(M,k); it; ++it)
        {
            it.value();
            it.row();   // row index
            it.col();   // col index (here it is equal to k)
            it.index(); // inner index, here it is equal to it.row()
            fs << it.row()+1 << " " << it.col()+1 << " " << it.value() << " \n" ;

        }
    }
    fs.close();
}


template <typename RealT>
void exportOctaveDenseMat(const Eigen::Matrix<RealT, Eigen::Dynamic, Eigen::Dynamic>& M, const std::string& filename, const std::string& matrixName)
{
//    OUT << "Exporting sparse matrix into '" << filename << "'." << ENDL;
    std::ofstream fs;
    fs.open(filename);
    fs << "# File written by exportOctaveDenseMat() \n" ;
    fs << "# name: " << matrixName << " \n" ;
    fs << "# type: matrix \n" ;
    fs << "# rows: " << M.rows() << " \n" ;
    fs << "# columns: " << M.cols() << " \n" ;

    for (int i=0; i<M.rows(); ++i)
    {
        for (int j=0; j<M.cols(); ++j)
        {
            fs << M(i,j) << " " ;
        }
        fs << " \n" ;
    }
    fs.close();
}
