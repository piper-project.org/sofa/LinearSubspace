

#include "LinearSubspace/config.h"

#include <Eigen/Core>
#include <Eigen/Sparse>
#include <Eigen/Dense>



namespace Eigen {

template <typename RealT>
std::ostream& operator<<( std::ostream& out, const Eigen::SparseMatrix<RealT>& /*c*/ )
{
    msg_warning("Eigen::SparseMatrix<Real>") << "Eigen::SparseMatrix<Real> is not yet writeable";
//    out << c;
    return out;
}

template <typename RealT>
std::istream& operator>>( std::istream& in, Eigen::SparseMatrix<RealT>& /*c*/ )
{
    msg_warning("Eigen::SparseMatrix<Real>") << "Eigen::SparseMatrix<Real> is not yet readable";
    std::string bidon;
    in >> bidon;
    return in;
}


template <typename RealT>
std::ostream& operator<<( std::ostream& out, const Eigen::Matrix<RealT, Eigen::Dynamic, Eigen::Dynamic>& c )
{
    msg_warning("Eigen::Matrix<RealT, Eigen::Dynamic, Eigen::Dynamic>") << "Eigen::Matrix<RealT, Eigen::Dynamic, Eigen::Dynamic> is not yet writeable";
//    out << c;
    return out;
}

template <typename RealT>
std::istream& operator>>( std::istream& in, Eigen::Matrix<RealT, Eigen::Dynamic, Eigen::Dynamic>& /*c*/ )
{
    msg_warning("Eigen::Matrix<RealT, Eigen::Dynamic, Eigen::Dynamic>>") << "Eigen::Matrix<RealT, Eigen::Dynamic, Eigen::Dynamic> is not yet readable";
    std::string bidon;
    in >> bidon;
    return in;
}

}
