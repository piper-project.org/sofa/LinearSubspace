import Sofa
import SofaPython.Tools

def createSceneAndController(rootNode):

    rootNode.createObject("RequiredPlugin", pluginName="SofaTetGen")
    rootNode.createObject("MeshVTKLoader", name="loader", filename="mesh/Armadillo_Tetra_4406.vtu", createSubelements=True)

    tetraNode = rootNode.createChild("tetraNode")
    tetraNode.createObject("TetraMeshConverterEngine", name="meshConverter", in_position="@../loader.position", in_tetra="@../loader.tetrahedra")
    tetraNode.createObject("VisualModel", name="visu", position="@meshConverter.out_position", triangles="@meshConverter.out_triangles")
    tetraNode.createObject("MeshTopology", name="topology", position="@meshConverter.out_position", triangles="@meshConverter.out_triangles")
    tetraNode.createObject("MechanicalObject", src="@topology")
#    tetraNode.createObject("MeshExporter", position="@meshConverter.out_position", exportAtEnd=True, filename="/home/ulysse/Documents/temp/mesh.vtk", format=2)

    visuNode = tetraNode.createChild("visuNode")
    visuNode.createObject("DataDisplay", triangleData="@../meshConverter.out_attribute")
    visuNode.createObject("ColorMap", colorScheme="Blue to Red")
    visuNode.createObject("IdentityMapping", input="@..", output="@.")

