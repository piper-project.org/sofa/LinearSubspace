import Sofa
import SofaPython.Tools

def createSceneAndController(rootNode):
    rootNode.createObject("RequiredPlugin", pluginName="SofaTetGen")

    rootNode.createObject("MeshVTKLoader", name="loader", filename="mesh/Armadillo_Tetra_4406.vtu")

    visuInitNode = rootNode.createChild("visuInitNode")
    visuInitNode.createObject("MeshTopology", name="topology", src="@../loader", drawTetrahedra=True, drawTriangles=True)


    tetraFinalNode = rootNode.createChild("tetraFinalNode")
    tetraFinalNode.createObject("TetraMeshCoarseningEngine", template="Vec3", name="coarsenizer", in_position="@../loader.position", in_tetra="@../loader.tetrahedra")

    tetraFinalNode.createObject("MeshTopology", name="topology", position="@coarsenizer.out_position", tetrahedra="@coarsenizer.out_tetra", drawTetrahedra=True, drawTriangles=True)
