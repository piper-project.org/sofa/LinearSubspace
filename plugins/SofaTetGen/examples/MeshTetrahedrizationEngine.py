import Sofa
import SofaPython.Tools

def createSceneAndController(rootNode):
#    rootNode.createObject("RequiredPlugin", pluginName="Flexible")
#    rootNode.createObject("RequiredPlugin", pluginName="Image")
    rootNode.createObject("RequiredPlugin", pluginName="SofaTetGen")

    rootNode.createObject("MeshObjLoader", name="loader", filename="mesh/liver.obj")
#    rootNode.createObject("MeshObjLoader", name="loader", filename="mesh/Armadillo_verysimplified.obj")
#    rootNode.createObject("MeshObjLoader", name="loader", filename="/home/ulysse/Documents/temp/tetra/child_skin.obj")
#    rootNode.createObject("MeshObjLoader", name="loader", filename="/home/ulysse/Documents/temp/tetgen_test/input/icos.obj")
#    rootNode.createObject("MeshObjLoader", name="loader", filename="/home/ulysse/Documents/temp/tetgen_test/input/truc.obj")
#    rootNode.createObject("MeshObjLoader", name="loader", filename="/home/ulysse/Documents/temp/tetgen_test/input/truc_HD.obj")

    triNode = rootNode.createChild("triNode")
    triNode.createObject("VisualModel", src="@../loader")

    tetraNode = rootNode.createChild("tetraNode")
    tetraNode.createObject("MeshTetrahedrizationEngine", template="Vec3", name="tetrahedrizer", in_position="@../loader.position", in_triangle="@../loader.triangles", plc=True, quality=True, minratio=1.414, fixedvolume=True, maxvolume=0.1)
#    tetraNode.createObject("MeshTetrahedrizationEngine", template="Vec3", name="tetrahedrizer", in_position="@../loader.position", in_triangle="@../loader.triangles", plc=True, quality=True)

    tetraNode.createObject("MeshTopology", name="topology", position="@tetrahedrizer.out_position", tetrahedra="@tetrahedrizer.out_tetra", drawTetrahedra=True, drawTriangles=True)
