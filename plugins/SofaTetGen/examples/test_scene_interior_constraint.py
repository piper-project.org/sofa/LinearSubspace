import Sofa
import SofaPython.Tools
import random

def createSceneAndController(rootNode):
    rootNode.createObject("RequiredPlugin", pluginName="SofaTetGen")

    rootNode.createObject("MeshObjLoader", name="loader", filename="/home/ulysse/Documents/temp/tetgen_test/input/icos.obj")

    triNode = rootNode.createChild("triNode")
    triNode.createObject("VisualModel", src="@../loader")

#    constraints = []
#    constraints = [[0.0, 0.0, 0.0]]
    N=100
    constraints = [[random.uniform(-1,1) for j in range(3)] for i in range(N)]

    tetraNode = rootNode.createChild("tetraNode")
#    tetraNode.createObject("MeshTetrahedrizationEngine", template="Vec3", name="tetrahedrizer", in_position="@../loader.position", in_constraints=SofaPython.Tools.listListToStr(constraints), in_triangle="@../loader.triangles", plc=True, quality=True, insertaddpoints=True, quiet=False, verbose=True)
    tetraNode.createObject("MeshTetrahedrizationEngine", template="Vec3", name="tetrahedrizer", in_position="@../loader.position", in_constraints=SofaPython.Tools.listListToStr(constraints), in_triangle="@../loader.triangles", plc=True, quality=True, insertaddpoints=True, quiet=False, verbose=True, minratio=1.414, fixedvolume=True, maxvolume=0.1)

    tetraNode.createObject("TetraMeshConverterEngine", name="meshConverter", in_position="@tetrahedrizer.out_position", in_tetra="@tetrahedrizer.out_tetra", clip_plane="0 0 0 1 0 0")
    tetraNode.createObject("VisualModel", position="@meshConverter.out_position", triangles="@meshConverter.out_triangles")

