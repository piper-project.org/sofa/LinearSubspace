/******************************************************************************
*       SOFA, Simulation Open-Framework Architecture, development version     *
*                (c) 2006-2017 INRIA, USTL, UJF, CNRS, MGH                    *
*                                                                             *
* This program is free software; you can redistribute it and/or modify it     *
* under the terms of the GNU Lesser General Public License as published by    *
* the Free Software Foundation; either version 2.1 of the License, or (at     *
* your option) any later version.                                             *
*                                                                             *
* This program is distributed in the hope that it will be useful, but WITH *
* ANY WARRANTY; with even the implied warranty of MERCHANTABILITY or       *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License *
* for more details.                                                           *
*                                                                             *
* You should have received a copy of the GNU Lesser General Public License    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.        *
*******************************************************************************
* Authors: The SOFA Team and ernal contributors (see Authors.txt)          *
*                                                                             *
* Contact information: contact@sofa-framework.org                             *
******************************************************************************/
#ifndef SOFATETGEN_TetraMeshConverterEngine_H
#define SOFATETGEN_TetraMeshConverterEngine_H


#include "SofaTetGen/config.h"

#include "tetgen/tetgen.h"

#include <sofa/helper/rmath.h>
#include <sofa/helper/OptionsGroup.h>
#include <sofa/helper/RandomGenerator.h>
#include <sofa/core/DataEngine.h>

#include <sofa/core/topology/BaseMeshTopology.h>

#include <algorithm>
#include <iostream>
#include <map>
#include <string>
#include <iostream>
#include <fstream>


#define TetraMeshConverterEngine_VERBOSE true


namespace sofa
{
namespace component
{
namespace engine
{

/**
    This Engine computes a triangular mesh from a tetrahedrization for visualization purposes
*/
template <typename DataTypes>
class SOFA_SofaTetGen_API TetraMeshConverterEngine : public core::DataEngine
{

public:
    typedef core::DataEngine herited;
    SOFA_CLASS(SOFA_TEMPLATE(TetraMeshConverterEngine, DataTypes) , herited);

    typedef typename DataTypes::Real Real;
    typedef typename DataTypes::VecReal VecReal;
    typedef typename DataTypes::Coord Coord;
    typedef typename DataTypes::VecCoord VecCoord;

    typedef uint Idx;
//    typedef typename sofa::helper::fixed_array<Idx, 3> Triangle;
    typedef typename sofa::core::topology::Topology::Triangle Triangle;
    typedef sofa::helper::vector<Triangle> TriangleList;
    typedef sofa::helper::fixed_array<Idx, 4> Tetra;
    typedef sofa::helper::vector<Tetra> TetraList;

    typedef sofa::helper::fixed_array<Real, 6> Plane;


    Data<VecCoord> d_inputPosition;
    Data<TetraList> d_inputTetra;

    Data<bool> d_clip;
    Data<Plane> d_clipPlane;

    Data<VecCoord> d_outputPosition;
    Data<TriangleList> d_outputTriangles;
    Data<VecReal> d_outputAttribute;

    virtual std::string getTemplateName() const    { return templateName(this); }
    static std::string templateName(const TetraMeshConverterEngine<DataTypes>* = NULL) { return DataTypes::Name(); }


    virtual void init()
    {
        addInput(&d_inputPosition);
        addInput(&d_inputTetra);

        addInput(&d_clip);
        addInput(&d_clipPlane);

        addOutput(&d_outputPosition);
        addOutput(&d_outputTriangles);
        addOutput(&d_outputAttribute);

        setDirtyValue();
//        reinit();
    }

    virtual void reinit()
    {
        update();
    }

    void update()
    {
        if(TetraMeshConverterEngine_VERBOSE)
        {
            std::cout << "Initiated tet mesh conversion." << std::endl;
        }


        helper::ReadAccessor< Data<VecCoord>> in_position_accessor(d_inputPosition);
        const VecCoord& in_position = in_position_accessor.ref();

        helper::ReadAccessor< Data<TetraList>> in_tetra_accessor(d_inputTetra);
        const TetraList& in_tetra = in_tetra_accessor.ref();

        helper::ReadAccessor< Data<Plane>> clip_plane_accessor(d_clipPlane);
        const Plane& clip_plane = clip_plane_accessor.ref();

        helper::WriteOnlyAccessor< Data<VecCoord>> out_position_accessor(d_outputPosition);
        VecCoord& out_position = out_position_accessor.wref();

        helper::WriteOnlyAccessor< Data<TriangleList>> out_triangle_accessor(d_outputTriangles);
        TriangleList& out_triangle = out_triangle_accessor.wref();

        helper::WriteOnlyAccessor< Data<VecReal>> out_attribute_accessor(d_outputAttribute);
        VecReal& out_attribute = out_attribute_accessor.wref();


        Coord clip_plane_center(clip_plane[0], clip_plane[1], clip_plane[2]);
        Coord clip_plane_normal(clip_plane[3], clip_plane[4], clip_plane[5]);

        out_position.resize(in_tetra.size() * 4 * 3);
//        out_attribute.resize(in_tetra.size() * 4 * 3);
        for(uint i=0; i<in_tetra.size(); ++i)
        {
            Coord p0 = in_position[in_tetra[i][0]];
            Coord p1 = in_position[in_tetra[i][1]];
            Coord p2 = in_position[in_tetra[i][2]];
            Coord p3 = in_position[in_tetra[i][3]];

//            Coord c = 0.25*(p0+p1+p2+p3);
//            Real epsilon = 0.1;

//            p0  = c + (p0-c)*(1.0-epsilon);
//            p1  = c + (p1-c)*(1.0-epsilon);
//            p2  = c + (p2-c)*(1.0-epsilon);
//            p3  = c + (p3-c)*(1.0-epsilon);


            out_position[(i*4+0)*3+0] = p0;
            out_position[(i*4+0)*3+1] = p0;
            out_position[(i*4+0)*3+2] = p0;

            out_position[(i*4+1)*3+0] = p1;
            out_position[(i*4+1)*3+1] = p1;
            out_position[(i*4+1)*3+2] = p1;

            out_position[(i*4+2)*3+0] = p2;
            out_position[(i*4+2)*3+1] = p2;
            out_position[(i*4+2)*3+2] = p2;

            out_position[(i*4+3)*3+0] = p3;
            out_position[(i*4+3)*3+1] = p3;
            out_position[(i*4+3)*3+2] = p3;


//            out_attribute[(i*4+0)*3+0] = i;
//            out_attribute[(i*4+0)*3+1] = i;
//            out_attribute[(i*4+0)*3+2] = i;

//            out_attribute[(i*4+1)*3+0] = i;
//            out_attribute[(i*4+1)*3+1] = i;
//            out_attribute[(i*4+1)*3+2] = i;

//            out_attribute[(i*4+2)*3+0] = i;
//            out_attribute[(i*4+2)*3+1] = i;
//            out_attribute[(i*4+2)*3+2] = i;

//            out_attribute[(i*4+3)*3+0] = i;
//            out_attribute[(i*4+3)*3+1] = i;
//            out_attribute[(i*4+3)*3+2] = i;
        }

        uint nb_tetra_processed = 0;
        out_triangle.resize(in_tetra.size() * 4);
        out_attribute.resize(in_tetra.size() * 4);
        for(uint i=0; i<in_tetra.size(); ++i)
        {
            Coord p0 = in_position[in_tetra[i][0]];
            Coord p1 = in_position[in_tetra[i][1]];
            Coord p2 = in_position[in_tetra[i][2]];
            Coord p3 = in_position[in_tetra[i][3]];

            if(d_clip.getValue() && (0.25*(p0+p1+p2+p3) - clip_plane_center)*clip_plane_normal < 0)
            {
                continue;
            }

            Idx v00 = (i*4+0)*3+0;
            Idx v01 = (i*4+0)*3+1;
            Idx v02 = (i*4+0)*3+2;

            Idx v10 = (i*4+1)*3+0;
            Idx v11 = (i*4+1)*3+1;
            Idx v12 = (i*4+1)*3+2;

            Idx v20 = (i*4+2)*3+0;
            Idx v21 = (i*4+2)*3+1;
            Idx v22 = (i*4+2)*3+2;

            Idx v30 = (i*4+3)*3+0;
            Idx v31 = (i*4+3)*3+1;
            Idx v32 = (i*4+3)*3+2;

            if(computeTetraVolume(p0, p1, p2, p3) > 0)
            {
                std::swap(v20, v30);
                std::swap(v21, v31);
                std::swap(v22, v32);

//                v20 = (i*4+3)*3+0;
//                v21 = (i*4+3)*3+1;
//                v22 = (i*4+3)*3+2;

//                v30 = (i*4+2)*3+0;
//                v31 = (i*4+2)*3+1;
//                v32 = (i*4+2)*3+2;
            }

            out_triangle[nb_tetra_processed*4+0][0] = v00;
            out_triangle[nb_tetra_processed*4+0][1] = v20;
            out_triangle[nb_tetra_processed*4+0][2] = v10;

            out_triangle[nb_tetra_processed*4+1][0] = v01;
            out_triangle[nb_tetra_processed*4+1][1] = v11;
            out_triangle[nb_tetra_processed*4+1][2] = v30;

            out_triangle[nb_tetra_processed*4+2][0] = v02;
            out_triangle[nb_tetra_processed*4+2][1] = v31;
            out_triangle[nb_tetra_processed*4+2][2] = v21;

            out_triangle[nb_tetra_processed*4+3][0] = v12;
            out_triangle[nb_tetra_processed*4+3][1] = v22;
            out_triangle[nb_tetra_processed*4+3][2] = v32;

            out_attribute[nb_tetra_processed*4+0] = Real(nb_tetra_processed)/in_tetra.size();
            out_attribute[nb_tetra_processed*4+1] = Real(nb_tetra_processed)/in_tetra.size();
            out_attribute[nb_tetra_processed*4+2] = Real(nb_tetra_processed)/in_tetra.size();
            out_attribute[nb_tetra_processed*4+3] = Real(nb_tetra_processed)/in_tetra.size();


            nb_tetra_processed++;
        }

        out_triangle.resize(nb_tetra_processed*4);
        out_attribute.resize(nb_tetra_processed*4);


        if(TetraMeshConverterEngine_VERBOSE)
        {
            std::cout << "Converted tetra mesh to triangle mesh of " << out_position.size() << " points and " << out_triangle.size() << " triangles." << std::endl;
        }

        cleanDirty();
    }

    Real computeTetraVolume(const Coord& p0, const Coord& p1, const Coord& p2, const Coord& p3)
    {
        return p0[0]*p1[1]*p2[2] - p0[0]*p1[1]*p3[2] - p0[0]*p1[2]*p2[1] + p0[0]*p1[2]*p3[1] + p0[0]*p2[1]*p3[2] - p0[0]*p2[2]*p3[1] - p0[1]*p1[0]*p2[2] + p0[1]*p1[0]*p3[2] + p0[1]*p1[2]*p2[0] - p0[1]*p1[2]*p3[0] - p0[1]*p2[0]*p3[2] + p0[1]*p2[2]*p3[0] + p0[2]*p1[0]*p2[1] - p0[2]*p1[0]*p3[1] - p0[2]*p1[1]*p2[0] + p0[2]*p1[1]*p3[0] + p0[2]*p2[0]*p3[1] - p0[2]*p2[1]*p3[0] - p1[0]*p2[1]*p3[2] + p1[0]*p2[2]*p3[1] + p1[1]*p2[0]*p3[2] - p1[1]*p2[2]*p3[0] - p1[2]*p2[0]*p3[1] + p1[2]*p2[1]*p3[0];
    }

protected:
    TetraMeshConverterEngine() : herited()
      , d_inputPosition   (initData(&d_inputPosition ,  VecCoord()        , "in_position",   "Positions of the vertices of the input tet mesh"))
      , d_inputTetra      (initData(&d_inputTetra,      TetraList()       , "in_tetra",      "List of indices (starting at 0) defining tetra"))
      , d_clip            (initData(&d_clip,            false             , "clip",          "Perform clipping"))
      , d_clipPlane       (initData(&d_clipPlane,       Plane(0,0,0,1,0,0), "clip_plane",    "Clipping plane (px, py, pz, nx, ny, nz)"))
      , d_outputPosition  (initData(&d_outputPosition,  VecCoord()        , "out_position",  "Positions of the vertices of the put triangular mesh"))
      , d_outputTriangles (initData(&d_outputTriangles, TriangleList()    , "out_triangles", "List of indices (starting at 0) defining triangles"))
      , d_outputAttribute (initData(&d_outputAttribute, VecReal()         , "out_attribute", "Output attirbute (used for e.g. colorization)"))
    {
    }

    ~TetraMeshConverterEngine()
    {

    }

};




}
}
}


#endif
