/******************************************************************************
*       SOFA, Simulation Open-Framework Architecture, development version     *
*                (c) 2006-2017 INRIA, USTL, UJF, CNRS, MGH                    *
*                                                                             *
* This program is free software; you can redistribute it and/or modify it     *
* under the terms of the GNU Lesser General Public License as published by    *
* the Free Software Foundation; either version 2.1 of the License, or (at     *
* your option) any later version.                                             *
*                                                                             *
* This program is distributed in the hope that it will be useful, but WITHOUT *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License *
* for more details.                                                           *
*                                                                             *
* You should have received a copy of the GNU Lesser General Public License    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.        *
*******************************************************************************
* Authors: The SOFA Team and external contributors (see Authors.txt)          *
*                                                                             *
* Contact information: contact@sofa-framework.org                             *
******************************************************************************/
#ifndef SOFATETGEN_TetraMeshCoarseningEngine_H
#define SOFATETGEN_TetraMeshCoarseningEngine_H


#include "SofaTetGen/config.h"

#include "tetgen/tetgen.h"

#include <sofa/helper/rmath.h>
#include <sofa/helper/OptionsGroup.h>
#include <sofa/core/DataEngine.h>

#include <sofa/core/topology/BaseMeshTopology.h>

#include <algorithm>
#include <iostream>
#include <map>
#include <string>
#include <iostream>
#include <fstream>


#define TetraMeshCoarseningEngine_VERBOSE               true
#define TetraMeshCoarseningEngine_DISPLAY_CONSTRAINTS   false
#define TetraMeshCoarseningEngine_DISPLAY_RESULT        false


namespace sofa
{
namespace component
{
namespace engine
{

/**
    This Engine computes a tetrahedrization on an input triangular mesh
*/
template <typename DataTypes>
class SOFA_SofaTetGen_API TetraMeshCoarseningEngine : public core::DataEngine
{

public:
    typedef core::DataEngine Inherited;
    SOFA_CLASS(SOFA_TEMPLATE(TetraMeshCoarseningEngine, DataTypes) , Inherited);

    typedef typename DataTypes::Real Real;
    typedef typename DataTypes::Coord Coord;
    typedef typename DataTypes::VecCoord VecCoord;

    // TODO use typedefs from topology
    typedef uint Idx;
//    typedef typename sofa::helper::fixed_array<Idx, 4> Tetra;
    typedef typename core::topology::BaseMeshTopology::Tetra Tetra;
    typedef typename sofa::helper::vector<Tetra> TetraList;


    Data<VecCoord> d_inputPosition;
    Data<TetraList> d_inputTetra;

    Data<VecCoord> d_outputPosition;
    Data<TetraList> d_outputTetra;

    virtual std::string getTemplateName() const    { return templateName(this); }
    static std::string templateName(const TetraMeshCoarseningEngine<DataTypes>* = NULL) { return DataTypes::Name(); }


      // Switches of TetGen.
      Data<bool> d_plc;                                                         // '-p', 0.
      Data<bool> d_psc;                                                         // '-s', 0.
      Data<bool> d_refine;                                                      // '-r', 0.
      Data<bool> d_quality;                                                     // '-q', 0.
      Data<bool> d_nobisect;                                                    // '-Y', 0.
      Data<bool> d_coarsen;                                                     // '-R', 0.
      Data<bool> d_weighted;                                                    // '-w', 0.
      Data<bool> d_brio_hilbert;                                                // '-b', 1.
      Data<bool> d_incrflip;                                                    // '-l', 0.
      Data<bool> d_flipinsert;                                                  // '-L', 0.
      Data<bool> d_metric;                                                      // '-m', 0.
      Data<bool> d_varvolume;                                                   // '-a', 0.
      Data<bool> d_fixedvolume;                                                 // '-a', 0.
      Data<bool> d_regionattrib;                                                // '-A', 0.
      Data<bool> d_conforming;                                                  // '-D', 0.
      Data<bool> d_insertaddpoints;                                             // '-i', 0.
      Data<bool> d_diagnose;                                                    // '-d', 0.
      Data<bool> d_convex;                                                      // '-c', 0.
      Data<bool> d_nomergefacet;                                                // '-M', 0.
      Data<bool> d_nomergevertex;                                               // '-M', 0.
      Data<bool> d_noexact;                                                     // '-X', 0.
      Data<bool> d_nostaticfilter;                                              // '-X', 0.
      Data<bool> d_zeroindex;                                                   // '-z', 0.
      Data<bool> d_facesout;                                                    // '-f', 0.
      Data<bool> d_edgesout;                                                    // '-e', 0.
      Data<bool> d_neighout;                                                    // '-n', 0.
      Data<bool> d_voroout;                                                     // '-v', 0.
      Data<bool> d_meditview;                                                   // '-g', 0.
      Data<bool> d_vtkview;                                                     // '-k', 0.
      Data<bool> d_nobound;                                                     // '-B', 0.
      Data<bool> d_nonodewritten;                                               // '-N', 0.
      Data<bool> d_noelewritten;                                                // '-E', 0.
      Data<bool> d_nofacewritten;                                               // '-F', 0.
      Data<bool> d_noiterationnum;                                              // '-I', 0.
      Data<bool> d_nojettison;                                                  // '-J', 0.
      Data<bool> d_reversetetori;                                               // '-R', 0.
      Data<bool> d_docheck;                                                     // '-C', 0.
      Data<bool> d_quiet;                                                       // '-Q', 0.
      Data<bool> d_verbose;                                                     // '-V', 0.

      // Parameters of TetGen.
      Data<int> d_vertexperblock;                                           // '-x', 4092.
      Data<int> d_tetrahedraperblock;                                       // '-x', 8188.
      Data<int> d_shellfaceperblock;                                        // '-x', 2044.
      Data<int> d_nobisect_param;                                              // '-Y', 2.
      Data<int> d_addsteiner_algo;                                            // '-Y/', 1.
      Data<int> d_coarsen_param;                                               // '-R', 0.
      Data<int> d_weighted_param;                                              // '-w', 0.
      Data<int> d_fliplinklevel;                                                    // -1.
      Data<int> d_flipstarsize;                                                     // -1.
      Data<int> d_fliplinklevelinc;                                                 //  1.
      Data<int> d_reflevel;                                                    // '-D', 3.
      Data<int> d_optlevel;                                                    // '-O', 2.
      Data<int> d_optscheme;                                                   // '-O', 7.
      Data<int> d_delmaxfliplevel;                                                   // 1.
      Data<int> d_order;                                                       // '-o', 1.
      Data<int> d_steinerleft;                                                 // '-S', 0.
      Data<int> d_no_sort;                                                           // 0.
      Data<int> d_hilbert_order;                                           // '-b///', 52.
      Data<int> d_hilbert_limit;                                             // '-b//'  8.
      Data<int> d_brio_threshold;                                              // '-b' 64.

      Data<Real> d_brio_ratio;                                             // '-b/' 0.125.
      Data<Real> d_facet_ang_tol;                                          // '-p', 179.9.
      Data<Real> d_maxvolume;                                               // '-a', -1.0.
      Data<Real> d_minratio;                                                 // '-q', 0.0.
      Data<Real> d_mindihedral;                                              // '-q', 5.0.
      Data<Real> d_optmaxdihedral;                                               // 165.0.
      Data<Real> d_optminsmtdihed;                                               // 179.0.
      Data<Real> d_optminslidihed;                                               // 179.0.
      Data<Real> d_epsilon;                                               // '-T', 1.0e-8.
      Data<Real> d_minedgelength;                                                  // 0.0.
      Data<Real> d_coarsen_percent;                                         // -R1/#, 1.0.







    virtual void init()
    {
        addInput(&d_inputPosition);
        addInput(&d_inputTetra);

        addOutput(&d_outputPosition);
        addOutput(&d_outputTetra);

        setDirtyValue();
        reinit();
    }

    virtual void reinit()
    {
        update();
    }

    void update()
    {
        if(TetraMeshCoarseningEngine_VERBOSE)
        {
            std::cout << "Initiated mesh coarsening." << std::endl;
        }


        helper::ReadAccessor< Data<VecCoord>> in_position_accessor(d_inputPosition);
        const VecCoord& in_position = in_position_accessor.ref();

        helper::ReadAccessor< Data<TetraList>> in_tetra_accessor(d_inputTetra);
        const TetraList& in_tetra = in_tetra_accessor.ref();

        helper::WriteOnlyAccessor< Data<VecCoord>> out_position_accessor(d_outputPosition);
        VecCoord& out_position = out_position_accessor.wref();

        helper::WriteOnlyAccessor< Data<TetraList>> out_tetra_accessor(d_outputTetra);
        TetraList& out_tetra = out_tetra_accessor.wref();


        if(TetraMeshCoarseningEngine_VERBOSE)
        {
            std::cout << "Working on an input mesh of " << in_position.size() << " points and " << in_tetra.size() << " tetrahedra." << std::endl;
        }

        tetgenio in;

        in.firstnumber = 0;
        in.numberofpoints = in_position.size();
        in.pointlist = new Real[in.numberofpoints * 3];

        for(uint i=0; i<in.numberofpoints; ++i)
        {
            for(uint j=0; j<3; ++j)
            {
                in.pointlist[i*3+j] = in_position[i][j];
            }
        }

        in.numberoftetrahedra = in_tetra.size();
        in.tetrahedronlist = new int[in.numberoftetrahedra*4];

        for (uint i=0; i<in.numberoftetrahedra; ++i)
        {
            for (uint j=0; j<4; ++j)
            {
                in.tetrahedronlist[i * 4 + j] = in_tetra[i][j];
            }
        }

        tetgenbehavior behavior;
        setBehavior(behavior);
        if(TetraMeshCoarseningEngine_VERBOSE)
        {
            std::cout << "Using following parameters:" << std::endl;
            displayBehavior(behavior);
        }

//        tetgenio out;

        tetgenmesh m;
        m.b = &behavior;
        m.in = &in;
        return;
        m.meshcoarsening();


        tetgenio &out = in;


        if(TetraMeshCoarseningEngine_VERBOSE)
        {
            std::cout << "Created tetrahedral mesh with " << out.numberofpoints << " points and " << out.numberoftetrahedra << " tetrahedra." << std::endl;
        }


        out_position.resize(out.numberofpoints);

        for(uint i=0; i<out.numberofpoints; ++i)
        {
            if(TetraMeshCoarseningEngine_DISPLAY_RESULT)
            {
                std::cout << "v#" << i << " :  ";
            }

            for(uint j=0; j<3; ++j)
            {
                out_position[i][j] = out.pointlist[i*3+j];
                if(TetraMeshCoarseningEngine_DISPLAY_RESULT)
                {
                    std::cout << "  " << out_position[i][j];
                }
            }
            if(TetraMeshCoarseningEngine_DISPLAY_RESULT)
            {
                std::cout << std::endl;
            }
        }

        assert(out.numberofcorners == 4);

        out_tetra.resize(out.numberoftetrahedra);

        for (uint i=0; i<out.numberoftetrahedra; ++i)
        {
            for (uint j=0; j<4; ++j)
            {
                out_tetra[i][j] = out.tetrahedronlist[i * 4 + j];
            }
        }

        cleanDirty();
    }

    void setBehavior(tetgenbehavior& behavior)
    {
        behavior.plc                = d_plc.getValue();
        behavior.psc                = d_psc.getValue();
        behavior.refine             = d_refine.getValue();
        behavior.quality            = d_quality.getValue();
        behavior.nobisect           = d_nobisect.getValue();
        behavior.coarsen            = d_coarsen.getValue();
        behavior.weighted           = d_weighted.getValue();
        behavior.brio_hilbert       = d_brio_hilbert.getValue();
        behavior.incrflip           = d_incrflip.getValue();
        behavior.flipinsert         = d_flipinsert.getValue();
        behavior.metric             = d_metric.getValue();
        behavior.varvolume          = d_varvolume.getValue();
        behavior.fixedvolume        = d_fixedvolume.getValue();
        behavior.regionattrib       = d_regionattrib.getValue();
        behavior.conforming         = d_conforming.getValue();
        behavior.insertaddpoints    = d_insertaddpoints.getValue();
        behavior.diagnose           = d_diagnose.getValue();
        behavior.convex             = d_convex.getValue();
        behavior.nomergefacet       = d_nomergefacet.getValue();
        behavior.nomergevertex      = d_nomergevertex.getValue();
        behavior.noexact            = d_noexact.getValue();
        behavior.nostaticfilter     = d_nostaticfilter.getValue();
        behavior.zeroindex          = d_zeroindex.getValue();
        behavior.facesout           = d_facesout.getValue();
        behavior.edgesout           = d_edgesout.getValue();
        behavior.neighout           = d_neighout.getValue();
        behavior.voroout            = d_voroout.getValue();
        behavior.meditview          = d_meditview.getValue();
        behavior.vtkview            = d_vtkview.getValue();
        behavior.nobound            = d_nobound.getValue();
        behavior.nonodewritten      = d_nonodewritten.getValue();
        behavior.noelewritten       = d_noelewritten.getValue();
        behavior.nofacewritten      = d_nofacewritten.getValue();
        behavior.noiterationnum     = d_noiterationnum.getValue();
        behavior.nojettison         = d_nojettison.getValue();
        behavior.reversetetori      = d_reversetetori.getValue();
        behavior.docheck            = d_docheck.getValue();
        behavior.quiet              = d_quiet.getValue();
        behavior.verbose            = d_verbose.getValue();
        behavior.vertexperblock     = d_vertexperblock.getValue();
        behavior.tetrahedraperblock = d_tetrahedraperblock.getValue();
        behavior.shellfaceperblock  = d_shellfaceperblock.getValue();
        behavior.nobisect_param     = d_nobisect_param.getValue();
        behavior.addsteiner_algo    = d_addsteiner_algo.getValue();
        behavior.coarsen_param      = d_coarsen_param.getValue();
        behavior.weighted_param     = d_weighted_param.getValue();
        behavior.fliplinklevel      = d_fliplinklevel.getValue();
        behavior.flipstarsize       = d_flipstarsize.getValue();
        behavior.fliplinklevelinc   = d_fliplinklevelinc.getValue();
        behavior.reflevel           = d_reflevel.getValue();
        behavior.optlevel           = d_optlevel.getValue();
        behavior.optscheme          = d_optscheme.getValue();
        behavior.delmaxfliplevel    = d_delmaxfliplevel.getValue();
        behavior.order              = d_order.getValue();
        behavior.steinerleft        = d_steinerleft.getValue();
        behavior.no_sort            = d_no_sort.getValue();
        behavior.hilbert_order      = d_hilbert_order.getValue();
        behavior.hilbert_limit      = d_hilbert_limit.getValue();
        behavior.brio_threshold     = d_brio_threshold.getValue();
        behavior.brio_ratio         = d_brio_ratio.getValue();
        behavior.facet_ang_tol      = d_facet_ang_tol.getValue();
        behavior.maxvolume          = d_maxvolume.getValue();
        behavior.minratio           = d_minratio.getValue();
        behavior.mindihedral        = d_mindihedral.getValue();
        behavior.optmaxdihedral     = d_optmaxdihedral.getValue();
        behavior.optminsmtdihed     = d_optminsmtdihed.getValue();
        behavior.optminslidihed     = d_optminslidihed.getValue();
        behavior.epsilon            = d_epsilon.getValue();
        behavior.minedgelength      = d_minedgelength.getValue();
        behavior.coarsen_percent    = d_coarsen_percent.getValue();
    }


    void displayBehavior(const tetgenbehavior& behavior)
    {
        std::cout << "plc                : " << behavior.plc                << std::endl;
        std::cout << "psc                : " << behavior.psc                << std::endl;
        std::cout << "refine             : " << behavior.refine             << std::endl;
        std::cout << "quality            : " << behavior.quality            << std::endl;
        std::cout << "nobisect           : " << behavior.nobisect           << std::endl;
        std::cout << "coarsen            : " << behavior.coarsen            << std::endl;
        std::cout << "weighted           : " << behavior.weighted           << std::endl;
        std::cout << "brio_hilbert       : " << behavior.brio_hilbert       << std::endl;
        std::cout << "incrflip           : " << behavior.incrflip           << std::endl;
        std::cout << "flipinsert         : " << behavior.flipinsert         << std::endl;
        std::cout << "metric             : " << behavior.metric             << std::endl;
        std::cout << "varvolume          : " << behavior.varvolume          << std::endl;
        std::cout << "fixedvolume        : " << behavior.fixedvolume        << std::endl;
        std::cout << "regionattrib       : " << behavior.regionattrib       << std::endl;
        std::cout << "conforming         : " << behavior.conforming         << std::endl;
        std::cout << "insertaddpoints    : " << behavior.insertaddpoints    << std::endl;
        std::cout << "diagnose           : " << behavior.diagnose           << std::endl;
        std::cout << "convex             : " << behavior.convex             << std::endl;
        std::cout << "nomergefacet       : " << behavior.nomergefacet       << std::endl;
        std::cout << "nomergevertex      : " << behavior.nomergevertex      << std::endl;
        std::cout << "noexact            : " << behavior.noexact            << std::endl;
        std::cout << "nostaticfilter     : " << behavior.nostaticfilter     << std::endl;
        std::cout << "zeroindex          : " << behavior.zeroindex          << std::endl;
        std::cout << "facesout           : " << behavior.facesout           << std::endl;
        std::cout << "edgesout           : " << behavior.edgesout           << std::endl;
        std::cout << "neighout           : " << behavior.neighout           << std::endl;
        std::cout << "voroout            : " << behavior.voroout            << std::endl;
        std::cout << "meditview          : " << behavior.meditview          << std::endl;
        std::cout << "vtkview            : " << behavior.vtkview            << std::endl;
        std::cout << "nobound            : " << behavior.nobound            << std::endl;
        std::cout << "nonodewritten      : " << behavior.nonodewritten      << std::endl;
        std::cout << "noelewritten       : " << behavior.noelewritten       << std::endl;
        std::cout << "nofacewritten      : " << behavior.nofacewritten      << std::endl;
        std::cout << "noiterationnum     : " << behavior.noiterationnum     << std::endl;
        std::cout << "nojettison         : " << behavior.nojettison         << std::endl;
        std::cout << "reversetetori      : " << behavior.reversetetori      << std::endl;
        std::cout << "docheck            : " << behavior.docheck            << std::endl;
        std::cout << "quiet              : " << behavior.quiet              << std::endl;
        std::cout << "verbose            : " << behavior.verbose            << std::endl;
        std::cout << "vertexperblock     : " << behavior.vertexperblock     << std::endl;
        std::cout << "tetrahedraperblock : " << behavior.tetrahedraperblock << std::endl;
        std::cout << "shellfaceperblock  : " << behavior.shellfaceperblock  << std::endl;
        std::cout << "nobisect_param     : " << behavior.nobisect_param     << std::endl;
        std::cout << "addsteiner_algo    : " << behavior.addsteiner_algo    << std::endl;
        std::cout << "coarsen_param      : " << behavior.coarsen_param      << std::endl;
        std::cout << "weighted_param     : " << behavior.weighted_param     << std::endl;
        std::cout << "fliplinklevel      : " << behavior.fliplinklevel      << std::endl;
        std::cout << "flipstarsize       : " << behavior.flipstarsize       << std::endl;
        std::cout << "fliplinklevelinc   : " << behavior.fliplinklevelinc   << std::endl;
        std::cout << "reflevel           : " << behavior.reflevel           << std::endl;
        std::cout << "optlevel           : " << behavior.optlevel           << std::endl;
        std::cout << "optscheme          : " << behavior.optscheme          << std::endl;
        std::cout << "delmaxfliplevel    : " << behavior.delmaxfliplevel    << std::endl;
        std::cout << "order              : " << behavior.order              << std::endl;
        std::cout << "steinerleft        : " << behavior.steinerleft        << std::endl;
        std::cout << "no_sort            : " << behavior.no_sort            << std::endl;
        std::cout << "hilbert_order      : " << behavior.hilbert_order      << std::endl;
        std::cout << "hilbert_limit      : " << behavior.hilbert_limit      << std::endl;
        std::cout << "brio_threshold     : " << behavior.brio_threshold     << std::endl;
        std::cout << "brio_ratio         : " << behavior.brio_ratio         << std::endl;
        std::cout << "facet_ang_tol      : " << behavior.facet_ang_tol      << std::endl;
        std::cout << "maxvolume          : " << behavior.maxvolume          << std::endl;
        std::cout << "minratio           : " << behavior.minratio           << std::endl;
        std::cout << "mindihedral        : " << behavior.mindihedral        << std::endl;
        std::cout << "optmaxdihedral     : " << behavior.optmaxdihedral     << std::endl;
        std::cout << "optminsmtdihed     : " << behavior.optminsmtdihed     << std::endl;
        std::cout << "optminslidihed     : " << behavior.optminslidihed     << std::endl;
        std::cout << "epsilon            : " << behavior.epsilon            << std::endl;
        std::cout << "minedgelength      : " << behavior.minedgelength      << std::endl;
        std::cout << "coarsen_percent    : " << behavior.coarsen_percent    << std::endl;
    }

protected:
    TetraMeshCoarseningEngine() : Inherited()
      , d_inputPosition (initData(&d_inputPosition , VecCoord()    , "in_position","Positions of the vertices of the input mesh"))
      , d_inputTetra(initData(&d_inputTetra, TetraList(), "in_tetra","List of indices (starting at 0) defining input tetrahedron"))
      , d_outputPosition(initData(&d_outputPosition, VecCoord()    , "out_position","Positions of the vertices of the output mesh"))
      , d_outputTetra   (initData(&d_outputTetra   , TetraList()   , "out_tetra","List of indices (starting at 0) defining tetras"))

      , d_plc                 (initData(&d_plc                ,  true , "plc"               , "plc"               ))
      , d_psc                 (initData(&d_psc                ,  false, "psc"               , "psc"               ))
      , d_refine              (initData(&d_refine             ,  false, "refine"            , "refine"            ))
      , d_quality             (initData(&d_quality            ,  false , "quality"           , "quality"           ))
      , d_nobisect            (initData(&d_nobisect           ,  false, "nobisect"          , "nobisect"          ))
      , d_coarsen             (initData(&d_coarsen            ,  true , "coarsen"           , "coarsen"           ))
      , d_weighted            (initData(&d_weighted           ,  false, "weighted"          , "weighted"          ))
      , d_brio_hilbert        (initData(&d_brio_hilbert       ,  true , "brio_hilbert"      , "brio_hilbert"      ))
      , d_incrflip            (initData(&d_incrflip           ,  false, "incrflip"          , "incrflip"          ))
      , d_flipinsert          (initData(&d_flipinsert         ,  false, "flipinsert"        , "flipinsert"        ))
      , d_metric              (initData(&d_metric             ,  true , "metric"            , "metric"            ))
      , d_varvolume           (initData(&d_varvolume          ,  false, "varvolume"         , "varvolume"         ))
      , d_fixedvolume         (initData(&d_fixedvolume        ,  true , "fixedvolume"       , "fixedvolume"       ))
      , d_regionattrib        (initData(&d_regionattrib       ,  false, "regionattrib"      , "regionattrib"      ))
      , d_conforming          (initData(&d_conforming         ,  false, "conforming"        , "conforming"        ))
      , d_insertaddpoints     (initData(&d_insertaddpoints    ,  false, "insertaddpoints"   , "insertaddpoints"   ))
      , d_diagnose            (initData(&d_diagnose           ,  false, "diagnose"          , "diagnose"          ))
      , d_convex              (initData(&d_convex             ,  false, "convex"            , "convex"            ))
      , d_nomergefacet        (initData(&d_nomergefacet       ,  false, "nomergefacet"      , "nomergefacet"      ))
      , d_nomergevertex       (initData(&d_nomergevertex      ,  false, "nomergevertex"     , "nomergevertex"     ))
      , d_noexact             (initData(&d_noexact            ,  false, "noexact"           , "noexact"           ))
      , d_nostaticfilter      (initData(&d_nostaticfilter     ,  false, "nostaticfilter"    , "nostaticfilter"    ))
      , d_zeroindex           (initData(&d_zeroindex          ,  false, "zeroindex"         , "zeroindex"         ))
      , d_facesout            (initData(&d_facesout           ,  false, "facesout"          , "facesout"          ))
      , d_edgesout            (initData(&d_edgesout           ,  false, "edgesout"          , "edgesout"          ))
      , d_neighout            (initData(&d_neighout           ,  false, "neighout"          , "neighout"          ))
      , d_voroout             (initData(&d_voroout            ,  false, "voroout"           , "voroout"           ))
      , d_meditview           (initData(&d_meditview          ,  false, "meditview"         , "meditview"         ))
      , d_vtkview             (initData(&d_vtkview            ,  false, "vtkview"           , "vtkview"           ))
      , d_nobound             (initData(&d_nobound            ,  false, "nobound"           , "nobound"           ))
      , d_nonodewritten       (initData(&d_nonodewritten      ,  false, "nonodewritten"     , "nonodewritten"     ))
      , d_noelewritten        (initData(&d_noelewritten       ,  false, "noelewritten"      , "noelewritten"      ))
      , d_nofacewritten       (initData(&d_nofacewritten      ,  false, "nofacewritten"     , "nofacewritten"     ))
      , d_noiterationnum      (initData(&d_noiterationnum     ,  false, "noiterationnum"    , "noiterationnum"    ))
      , d_nojettison          (initData(&d_nojettison         ,  false, "nojettison"        , "nojettison"        ))
      , d_reversetetori       (initData(&d_reversetetori      ,  false, "reversetetori"     , "reversetetori"     ))
      , d_docheck             (initData(&d_docheck            ,  false, "docheck"           , "docheck"           ))
      , d_quiet               (initData(&d_quiet              ,  false, "quiet"             , "quiet"             ))
      , d_verbose             (initData(&d_verbose            ,  true , "verbose"           , "verbose"           ))
      , d_vertexperblock      (initData(&d_vertexperblock     ,   4092, "vertexperblock"    , "vertexperblock"    ))
      , d_tetrahedraperblock  (initData(&d_tetrahedraperblock ,   8188, "tetrahedraperblock", "tetrahedraperblock"))
      , d_shellfaceperblock   (initData(&d_shellfaceperblock  ,   4092, "shellfaceperblock" , "shellfaceperblock" ))
      , d_nobisect_param      (initData(&d_nobisect_param     ,      2, "nobisect_param"    , "nobisect_param"    ))
      , d_addsteiner_algo     (initData(&d_addsteiner_algo    ,      1, "addsteiner_algo"   , "addsteiner_algo"   ))
      , d_coarsen_param       (initData(&d_coarsen_param      ,      1, "coarsen_param"     , "coarsen_param"     ))
      , d_weighted_param      (initData(&d_weighted_param     ,      0, "weighteparam"      , "weighteparam"      ))
      , d_fliplinklevel       (initData(&d_fliplinklevel      ,     -1, "fliplinklevel"     , "fliplinklevel"     ))
      , d_flipstarsize        (initData(&d_flipstarsize       ,     -1, "flipstarsize"      , "flipstarsize"      ))
      , d_fliplinklevelinc    (initData(&d_fliplinklevelinc   ,      1, "fliplinklevelinc"  , "fliplinklevelinc"  ))
      , d_reflevel            (initData(&d_reflevel           ,      3, "reflevel"          , "reflevel"          ))
      , d_optlevel            (initData(&d_optlevel           ,      2, "optlevel"          , "optlevel"          ))
      , d_optscheme           (initData(&d_optscheme          ,      7, "optscheme"         , "optscheme"         ))
      , d_delmaxfliplevel     (initData(&d_delmaxfliplevel    ,      1, "delmaxfliplevel"   , "delmaxfliplevel"   ))
      , d_order               (initData(&d_order              ,      1, "order"             , "order"             ))
      , d_steinerleft         (initData(&d_steinerleft        ,     -1, "steinerleft"       , "steinerleft"       ))
      , d_no_sort             (initData(&d_no_sort            ,      0, "no_sort"           , "no_sort"           ))
      , d_hilbert_order       (initData(&d_hilbert_order      ,     52, "hilbert_order"     , "hilbert_order"     ))
      , d_hilbert_limit       (initData(&d_hilbert_limit      ,      8, "hilbert_limit"     , "hilbert_limit"     ))
      , d_brio_threshold      (initData(&d_brio_threshold     ,     64, "brio_threshold"    , "brio_threshold"    ))
      , d_brio_ratio          (initData(&d_brio_ratio         ,  0.125, "brio_ratio"        , "brio_ratio"        ))
      , d_facet_ang_tol       (initData(&d_facet_ang_tol      ,  179.9, "facet_ang_tol"     , "facet_ang_tol"     ))
      , d_maxvolume           (initData(&d_maxvolume          ,    0.2, "maxvolume"         , "maxvolume"         ))
      , d_minratio            (initData(&d_minratio           ,    0.0, "minratio"          , "minratio"          ))
      , d_mindihedral         (initData(&d_mindihedral        ,    0.0, "mindihedral"       , "mindihedral"       ))
      , d_optmaxdihedral      (initData(&d_optmaxdihedral     ,  165.0, "optmaxdihedral"    , "optmaxdihedral"    ))
      , d_optminsmtdihed      (initData(&d_optminsmtdihed     ,  179.0, "optminsmtdihed"    , "optminsmtdihed"    ))
      , d_optminslidihed      (initData(&d_optminslidihed     ,  179.0, "optminslidihed"    , "optminslidihed"    ))
      , d_epsilon             (initData(&d_epsilon            , 1.0e-8, "epsilon"           , "epsilon"           ))
      , d_minedgelength       (initData(&d_minedgelength      ,    0.0, "minedgelength"     , "minedgelength"     ))
      , d_coarsen_percent     (initData(&d_coarsen_percent    ,    0.5, "coarsen_percent"   , "coarsen_percent"   ))

    {
    }

    ~TetraMeshCoarseningEngine()
    {

    }

};




}
}
}


#endif
