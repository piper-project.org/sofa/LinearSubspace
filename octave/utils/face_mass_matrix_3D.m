function M = face_mass_matrix_3D(V,Tri,Tet)

  n = size(V,1);
  t = size(Tri,1);
  f = size(Tet,1);
  M = sparse(t, t);
  
  hash = Tri * [1;n+1;0];
  reverse_triangles = sparse(Tri(:,3), hash, (1:t)',n+1,(n+1)^2);
  
  for i=1:f
    
    % Vertex indices
    iV1 = Tet(i, 1);
    iV2 = Tet(i, 2);
    iV3 = Tet(i, 3);
    iV4 = Tet(i, 4);
    
    % Triangles
    T1 = sort([iV2, iV3, iV4]);
    T2 = sort([iV3, iV4, iV1]);
    T3 = sort([iV4, iV1, iV2]);
    T4 = sort([iV1, iV2, iV3]);
    
    % Triangle indices in Tri
    iT1 = reverse_triangles(T1(3),T1 * [1;n+1;0]);
    iT2 = reverse_triangles(T2(3),T2 * [1;n+1;0]);
    iT3 = reverse_triangles(T3(3),T3 * [1;n+1;0]);
    iT4 = reverse_triangles(T4(3),T4 * [1;n+1;0]);
  
    % Computing tetrahedron volume
    vol = det([ V(iV1, :),1; V(iV2, :),1; V(iV3, :),1; V(iV4, :),1]);
    
    % Filling mass matrix
%    M(iT1, iT1) = M(iT1, iT1) + vol/4.0;
%    M(iT2, iT2) = M(iT2, iT2) + vol/4.0;
%    M(iT3, iT3) = M(iT3, iT3) + vol/4.0;
%    M(iT4, iT4) = M(iT4, iT4) + vol/4.0;
    
    M(iT1, iT1) = M(iT1, iT1) + 4.0/vol;
    M(iT2, iT2) = M(iT2, iT2) + 4.0/vol;
    M(iT3, iT3) = M(iT3, iT3) + 4.0/vol;
    M(iT4, iT4) = M(iT4, iT4) + 4.0/vol;
    
  endfor
  
endfunction
