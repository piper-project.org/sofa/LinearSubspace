function Tri = faces(Tet)

  assert(size(Tet, 2) == 4);
  
  Tri = unique(sort( ...
    [Tet(:,2), Tet(:,3), Tet(:,4); ...
     Tet(:,3), Tet(:,4), Tet(:,1); ...
     Tet(:,4), Tet(:,1), Tet(:,2); ...
     Tet(:,1), Tet(:,2), Tet(:,3); ...
     ]')','rows');
  
  
endfunction
