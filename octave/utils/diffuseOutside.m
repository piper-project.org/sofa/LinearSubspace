function new_field = diffuseOutside(object, field)
%% object is a matrix containing only 1s and 0s (for the interior and the exterior of the object resp.)
%% field should have the same size than object, but arbitrary values


res = size(object);
nbPixels = prod(res);

index_fixed = reshape(object, 1, nbPixels) .* (1:nbPixels);
index_fixed = index_fixed(find(index_fixed));

index_free = reshape(1-object, 1, nbPixels) .* (1:nbPixels);
index_free = index_free(find(index_free));


nbFixedPixels = length(index_fixed);
nbFreePixels  = length(index_free );

A = sparse(nbFreePixels, nbFreePixels);  % Laplacian matrix for free pixels
B = sparse(nbFreePixels, nbFixedPixels);  % Projection matrix from fixed to free pixels

BB = zeros(nbFreePixels, 1);   % Neighbor counter for free pixels regarding fixed pixels

for k=1:nbFreePixels
  [i,j] = ind2sub(res, index_free(k));
  
  nbA = 0;  % Neighbor counter for free pixels regarding all pixels
  
  if i>1
    ii = sub2ind(res, i-1, j);
    nbA = nbA + 1;
    if object(ii) == 0
      ii = find(index_free-ii == 0)(1);
      A(k, ii) = A(k, ii) - 1;
    else
      ii = find(index_fixed-ii == 0)(1);
      B(k, ii) = B(k, ii) + 1;
      BB(k) = BB(k)+1;
    end
  end
  
  if i<res(1)
    ii = sub2ind(res, i+1, j);
    nbA = nbA + 1;
    if object(ii) == 0
      ii = find(index_free-ii == 0)(1);
      A(k, ii) = A(k, ii) - 1;
    else
      ii = find(index_fixed-ii == 0)(1);
      B(k, ii) = B(k, ii) + 1;
      BB(k) = BB(k)+1;
    end
  end
  
  if j>1
    ii = sub2ind(res, i, j-1);
    nbA = nbA + 1;
    if object(ii) == 0
      ii = find(index_free-ii == 0)(1);
      A(k, ii) = A(k, ii) - 1;
    else
      ii = find(index_fixed-ii == 0)(1);
      B(k, ii) = B(k, ii) + 1;
      BB(k) = BB(k)+1;
    end
  end
  
  if j<res(2)
    ii = sub2ind(res, i, j+1);
    nbA = nbA + 1;
    if object(ii) == 0
      ii = find(index_free-ii == 0)(1);
      A(k, ii) = A(k, ii) - 1;
    else
      ii = find(index_fixed-ii == 0)(1);
      B(k, ii) = B(k, ii) + 1;
      BB(k) = BB(k)+1;
    end
  end
  
  A(k,  k) = nbA;
end

%for k=1:nbFreePixels
%  if BB(k) > 0
%    B(k, :) = B(k, :) / BB(k);
%  end
%end

X = object.*field;
X = X(find(X));

I = A \ (B*X);

new_field = zeros(res);
new_field(index_free) = I;
