# INFORMATIONS

Author : Ulysse Vimont (ulysse.vimont@inria.fr)
Date : 03/07/2017



# OUTLINE

This is an octave prototype implementation of the following paper :
    Linear subspace design for real-time shape deformation
    Wang, Y.; Jacobson, A.; Barbič, J. & Kavan, L.
    ACM Transactions on Graphics (TOG), ACM, 2015, 34, 57

It is based on the following entry from Alec Jacobson's blog: www.alecjacobson.com/weblog/?p=4323



# ERRATUM

After exchanging e-mails with Alec Jacobson, it appeared that the original paper is wrong in describing the core energy term that allows to compute the subspace weights.
An erratum was published on 02/16/2017: http://www.cs.toronto.edu/~jacobson/images/error-in-linear-subspace-design-for-real-time-shape-deformation-2017-wang-et-al.pdf (see also ../plugins/LinearSubspace/doc/ of this repository).
This code implements both versions.



# ORGANIZATION

*scheme_2D.m* and *scheme_3D.m* are core programs that implement energy and weight computations on test shape, and display the results WITHOUT the erratum (ie. using laplacian and normal derivative defined on mesh vertices).
*scheme_2D_2.m* and *scheme_3D_2.m* do the same WITH the erratum.
*scheme_2D_3.m* and *scheme_3D_3.m* use the energy of the erratum and add the possibility of defining region handle (as in the original paper).

For clarity reasons, the rest of the code was ordered in different sub-folders. From octave, use 'addpath' appropriately for using the corresponding functionnalities.
 - *data* contains functions returning 2D (resp. 3D) triangular (resp. tetrahedral) meshes to be used as input for other functions. 
 - *gptoolbox_mesh* contains utility functions from gptoolbox -- mainly used for cotangent computations. It' is a sub-part of gptoolbox, see github.com/alecjacobson/gptoolbox for more information. Some file were slightly modified in order to be octave-compatible.
 - *io* contains a tetrahedral mesh VTK exporter.
 - *tests* contains test functions to check the equavalence of edge-based (resp. triangle-based) laplacian and normal derivative compared to vertex-based formulation. Use the 'symbolic' package (loaded with 'pkg load symbolic' -- has to be installed along octave).
 - *trash* contains things that are no more useful.
 - *utils* contains functions used for computing energy and weight terms on meshes.



# BIBLIOGRAPHY

    Linear subspace design for real-time shape deformation
    Yu Wang, Alec Jacobson, Jernej Barbič, Ladislav Kavan
    ACM Transactions on Graphics (TOG), ACM, 2015, 34, 57
    
    Error in “Linear Subspace Design for Real-Time Shape Deformation”
    Yu Wang, Alec Jacobson, Jernej Barbič, Ladislav Kavan
    Unpublished, 2017
    
    Discrete Quadratic Curvature Energies
    Miklos Bergou, Max Wardetzky, David Harmon, Denis Zorin, Eitan Grinspun
    Siggraph 2006 (course notes)
    
    




