clear all
close all

disp("")
disp("This program tests the computation of a deformation sub-space on a 2D grid.")

%% Initial data

disp("")
disp("Loading data...")

N=40;
res = [N,N];
data = zeros(res);
data_X = zeros(res);
data_Y = zeros(res);
nbPixels = prod(res);

dim = [0,0,1,1];

c = [0.5, 0.5]
r = 0.3
for i=1:res(1)
  tx = (i-1)/(res(1)-1);
  x = dim(1) + tx*dim(3);
  for j=1:res(2)
    ty = (j-1)/(res(2)-1);
    y = dim(2) + ty*dim(4);
    
    if norm([x,y]-c) < r
      data(i,j) = 1;
      data_X(i,j) = x;
      data_Y(i,j) = y;
    end
  end
end


disp(["... loaded model with resolution: ", num2str(res(1)), "x", num2str(res(2)), "."])


%% Energy matrix computation

disp("")
disp("Computing energy matrix...")
tic

L = sparse(nbPixels, nbPixels);

for i=1:res(1)
  for j=1:res(2)
%    d = data(i,j)
    if data(i, j) == 0;
      continue
    end
    
    if i>1
      if data(i-1, j) == 1
        L(sub2ind(res, i  , j), sub2ind(res, i  , j)) = L(sub2ind(res, i  , j), sub2ind(res, i  , j)) - 1;
        L(sub2ind(res, i  , j), sub2ind(res, i-1, j)) = L(sub2ind(res, i  , j), sub2ind(res, i-1, j)) + 1;
      end
    end
    
    if i<res(1)
      if data(i+1, j) == 1
        L(sub2ind(res, i  , j), sub2ind(res, i  , j)) = L(sub2ind(res, i  , j), sub2ind(res, i  , j)) - 1;
        L(sub2ind(res, i  , j), sub2ind(res, i+1, j)) = L(sub2ind(res, i  , j), sub2ind(res, i+1, j)) + 1;
      end
    end
    
    if j>1
      if data(i, j-1) == 1
        L(sub2ind(res, i  , j), sub2ind(res, i, j  )) = L(sub2ind(res, i  , j), sub2ind(res, i, j  )) - 1;
        L(sub2ind(res, i  , j), sub2ind(res, i, j-1)) = L(sub2ind(res, i  , j), sub2ind(res, i, j-1)) + 1;
      end
    end
    
    if j<res(2)
      if data(i, j+1) == 1
        L(sub2ind(res, i  , j), sub2ind(res, i, j  )) = L(sub2ind(res, i  , j), sub2ind(res, i, j  )) - 1;
        L(sub2ind(res, i  , j), sub2ind(res, i, j+1)) = L(sub2ind(res, i  , j), sub2ind(res, i, j+1)) + 1;
      end
    end
  end
end


K = L;
%K = L+N;
%K = L-N;

M = speye(nbPixels, nbPixels);
A = K' * (M\ K);

t=toc;
disp(["...done in ",num2str(t),"s."])


%% Energy matrix tests

disp("")
disp("Performing precision tests...")

%O = ones(nbPixels, 1);
O = reshape(data, nbPixels, 1);
O_result = A*O;
O_result_2 = O_result'*O_result
if(O_result_2 < 1e-5)
  disp("Constant precision : CHECK.")
else
  disp("Constant precision : FAILURE.")
endif


V_result_X = A*reshape(data_X, nbPixels, 1);
V_result_2_X = V_result_X'*V_result_X;
tr_V_result_2_X = trace(V_result_2_X)

V_result_Y = A*reshape(data_Y, nbPixels, 1);
V_result_2_Y = V_result_Y'*V_result_Y;
tr_V_result_2_Y = trace(V_result_2_Y)

if(and([tr_V_result_2_X < 1e-5, tr_V_result_2_Y < 1e-5]))
  disp("Linear precision : CHECK.")
else
  disp("Linear precision : FAILURE.")
endif

%return


%% Weights computation

disp("")
disp("Computing weights...")
tic

%constrained_pixels = [50,50;60,40;40,40];
m = 3;
constrained_pixels = floor(kron(res, ones(m,1))./[2.0, 2.0; 1.7,2.5; 2.5, 2.5]);
J = speye(m);

S = sparse(m, nbPixels);
for i=1:m
  S(i, sub2ind(res, constrained_pixels(i,1), constrained_pixels(i,2))) = 1;
endfor

T = sparse(nbPixels-m, nbPixels);

k=1;
for i=1:res(1)
  for j=1:res(2)
    if not(any(all(kron([i,j], ones(m,1))==constrained_pixels,2)))
      T(k, sub2ind(res, i, j)) = 1;
      k = k+1;
      continue
    endif
  end
end

%W = S'*J-T'*inverse(T*A*T')*T*A*S'*J;
W = S'*J-T'*((T*A*T')\T)*A*S'*J;

t=toc;
disp(["...done in ",num2str(t),"s."])


%% Weights visualization

disp("")
disp("Visualizing weights (see fig. 1)")

figure()
for i=1:m
  subplot(ceil(sqrt(m)), ceil(sqrt(m)), i)
  imagesc(reshape(W(:,i), res(1), res(2))')
  hold on
  plot(constrained_pixels(i,1), constrained_pixels(i,2), 'kx', 'markersize', 10, "linewidth", 3)
  axis image
  axis xy
%  axis square
  colorbar
  
  title(['handle #', num2str(i), " it at (", num2str(constrained_pixels(i,1)), ",", num2str(constrained_pixels(i,2)),")"])
end

%return


%% Rest pose reproduction test

disp("")
disp("Performing rest post reproduction test...")


%Ht = (constrained_pixels - ones(m,2))./(kron(res-[1,1], ones(m,1)));
%H = kron(dim(1:2), ones(m,1)) + Ht.*kron(dim(3:4), ones(m,1));
%%H = ones(m,1);

%data_reconstruct = W * H;

H_X = data_X(sub2ind(res, constrained_pixels(:,1),constrained_pixels(:,2)));
H_Y = data_Y(sub2ind(res, constrained_pixels(:,1),constrained_pixels(:,2)));

data_reconstruct_X = W * H_X;
data_reconstruct_Y = W * H_Y;

data_reconstruct_X = reshape(data_reconstruct_X, res(1), res(2));
data_reconstruct_Y = reshape(data_reconstruct_Y, res(1), res(2));

data_diff_X = data_X - data_reconstruct_X;
data_diff_X_2 = data_diff_X'*data_diff_X;
tr_data_diff_X_2 = trace(data_diff_X_2)

data_diff_Y = data_Y - data_reconstruct_Y;
data_diff_Y_2 = data_diff_Y'*data_diff_Y;
tr_data_diff_Y_2 = trace(data_diff_Y_2)

if(and([tr_data_diff_X_2 < 1e-5, tr_data_diff_Y_2 < 1e-5]))
  disp("Rest pose reproduciton : CHECK.")
else
  disp("Rest pose reproduciton : FAILURE.")
endif


figure()
subplot(2,1,1)
imagesc((data_reconstruct_X - data_X)')
hold on
for i=1:m
  plot(constrained_pixels(i,1), constrained_pixels(i,2), 'kx', 'markersize', 10, "linewidth", 3)
end
axis image
axis xy
%axis square
colorbar
title("Error on original shape")
xlabel("x")
ylabel("y")

subplot(2,1,2)
imagesc((data_reconstruct_Y-data_Y)')
hold on
for i=1:m
  plot(constrained_pixels(i,1), constrained_pixels(i,2), 'kx', 'markersize', 10, "linewidth", 3)
end
axis image
axis xy
%axis square
colorbar
title("Error on original shape")
xlabel("x")
ylabel("y")

disp("Displaying result (see fig. 2)")


%return


% Deformation test

disp("")
disp("Visualizing deformation (see fig. 3)")

deform_values = [];

fig = figure();
data_deform_X = zeros(res);
data_deform_Y = zeros(res);
H_deform_X = zeros(m,1);
H_deform_Y = zeros(m,1);
while isfigure(fig)
  hold off
  
  subplot(2,1,1)
  imagesc(data_deform_X')
  axis image
  
  subplot(2,1,2)
  imagesc(data_deform_Y')
  axis image
  
  [x, y, buttons] = ginput(2);
  
  [~,i_min]=min(sum((constrained_pixels - ([x(1);y(1)]*ones(1,m))').^ 2,2));
  
  H_deform_X(i_min) = H_deform_X(i_min) + x(2)-x(1);
  H_deform_Y(i_min) = H_deform_Y(i_min) + y(2)-y(1);
  
  data_deform_X = reshape(W * H_deform_X, res(1), res(2));
  data_deform_Y = reshape(W * H_deform_Y, res(1), res(2));
  
end


