function [] = export_tetra_mesh(V, F, filename)

  f_id = fopen(filename, 'w');
  
  format long g;
  #format long;
  
  ## version
  fdisp(f_id, "# vtk DataFile Version 2.0");
  
  ## header
  fdisp(f_id, "Unstructured Grid");
  
  ## file format
  fdisp(f_id, "ASCII");
  
  ## data structure
  fdisp(f_id, "DATASET UNSTRUCTURED_GRID");
  
  ## attirbutes
  fdisp(f_id, ["POINTS ", num2str(length(V)), " double"]);
  fdisp(f_id, V);
  fdisp(f_id, "");
  
  fdisp(f_id, ["CELLS ", num2str(length(F)), " ", num2str(5*length(F)) ]);
  fdisp(f_id, [4*ones(length(F),1), F-1]);
  fdisp(f_id, "");
  
  fdisp(f_id, ["CELL_TYPES ", num2str(length(F))]);
  fdisp(f_id, 10*ones(length(F),1));
  fdisp(f_id, "");
  
  fclose(f_id);
 
end
