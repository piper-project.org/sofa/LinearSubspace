clear all
close all

disp("")
disp("This program tests the computation of a deformation sub-space on a 3D tetrahedral mesh.")

% Initial data

disp("")
disp("Loading data...")

%[V, F, handle_indices] = icosahedron();
%[V, F, handle_indices] = armadillo();
%[V, F, handle_indices] = truc_LD();
[V, F, handle_indices] = truc_HD();

n = size(V,1);
f = size(F,1);

%handle_indices = int32(rand(1,100)*n);

disp(["... loaded model with ", num2str(n), " vertices and ", num2str(f), " faces."])


% Energy matrix computation

disp("")
disp("Computing energy matrix...")
tic

C = cotangent(V,F);
L = sparse(F(:,[2 3 1 4 4 4]),...
           F(:,[3 1 2 1 2 3]),...
           C,...
           n,n);
L = L+L';
L = L - diag(sum(L,2));
%if(all(diag(L)>0))
%  warning('Flipping sign of cotmatrix3, so that diag is negative');
%  L = -L;
%end

[~,on_b] = on_boundary(F); % for each vertex of each tetra, 1 if the face opposite the vertex is on boundary, 0 else
[on_b_f,on_b_c] = find(on_b); % indices of tetrahedra on border, and subscript index of the vertex opposed the triangle on border for each tetrahedron



% vertices of faces on the border
Pi = mod(on_b_c+1,4)+1;
Qi = mod(on_b_c+2,4)+1;
Ri = mod(on_b_c+0,4)+1;

P = F(sub2ind(size(F), on_b_f, Pi));
Q = F(sub2ind(size(F), on_b_f, Qi));
R = F(sub2ind(size(F), on_b_f, Ri));

% vertices opposed to the face on border
Si = on_b_c;
S = F(sub2ind(size(F),on_b_f,Si));

reverse_edge = [
  0, 3, 2, 4; 
  3, 0, 1, 5; 
  2, 1, 0, 6; 
  4, 5, 6, 0];

% Corresponding cotmatrix entries
Cpq = C(sub2ind(size(C),on_b_f,reverse_edge(sub2ind([4,4], Ri, Si))));
Cqr = C(sub2ind(size(C),on_b_f,reverse_edge(sub2ind([4,4], Pi, Si))));
Crp = C(sub2ind(size(C),on_b_f,reverse_edge(sub2ind([4,4], Qi, Si))));

N = sparse( ...
  [  P    P    P    P    P    P    Q    Q    Q    Q    Q    Q    R    R    R    R    R    R], ...
  [  P    S    Q    S    R    S    P    S    Q    S    R    S    P    S    Q    S    R    S], ...
  [Cqr -Cqr  Crp -Crp  Cpq -Cpq  Cqr -Cqr  Crp -Crp  Cpq -Cpq  Cqr -Cqr  Crp -Crp  Cpq -Cpq], ...
  n, n);


%K = L;
%K = L-N;
K = L+N;

%M = massmatrix(V,F);
M = speye(n,n);

A = K' * (M\ K);

t=toc;
disp(["...done in ",num2str(t),"s."])


% Energy matrix tests

disp("")
disp("Performing precision tests...")

O = ones(n, 1);
O_result = A*O;
O_result_2 = O_result'*O_result
if(O_result_2 < 1e-5)
  disp("Constant precision : CHECK.")
else
  disp("Constant precision : FAILURE.")
endif

V_result = A*V;
V_result_2 = V_result'*V_result;
tr_V_result_2 = trace(V_result_2)
if(tr_V_result_2 < 1e-5)
  disp("Linear precision : CHECK.")
else
  disp("Linear precision : FAILURE.")
endif



% Weights computation

disp("")
disp("Computing weights...")
tic

m = size(handle_indices,2);
H = V(handle_indices, :);

J = speye(m);

S = sparse(m, n);
for i=1:m
  S(i, handle_indices(i)) = 1;
endfor

T = sparse(n-m, n);

j=1;
for i=1:n
  if(size(find(handle_indices-i), 2) == m)
    T(j, i) = 1;
    j = j+1;
  endif
endfor

%W = S'*J-T'*inverse(T*A*T')*T*A*S'*J;
W = S'*J-T'*((T*A*T')\T)*A*S'*J;

t=toc;
disp(["...done in ",num2str(t),"s."])


%% Weights visualization

##{
disp("")
disp("Visualizing weights (see fig. 1)")

figure()
for i=1:m
  subplot(round(sqrt(m)), ceil(sqrt(m)), i)
  trisurf([P,Q,R], V(:, 1), V(:, 2), V(:, 3), W(:,i), "facecolor", "interp", "edgecolor", "k")
%  trisurf([P,Q,R], V(:, 1), V(:, 2), V(:, 3), min(max(W(:,i), -1.0),2.0), "facecolor", "interp", "edgecolor", "k")
%  shading("interp")
  hold on
  plot3(H(i, 1), H(i, 2), H(i, 3), 'kx', "linewidth", 2, "markersize", 10)
  axis square
  axis equal
  colorbar("EastOutside")
  title (["w_{",num2str(i), "}"]);
  view(3)
%  view (0, -90);
%  xlabel('x');
%  ylabel('y');
%  zlabel('z');
endfor
#}


% Rest pose reproduction test

disp("")
disp("Performing rest post reproduction test...")

V_reconstruct = W * H;
V_diff = V - V_reconstruct;
V_diff_2 = V_diff'*V_diff;
tr_V_diff_2 = trace(V_diff_2)

if(tr_V_diff_2 < 1e-5)
  disp("Rest pose reproduciton : CHECK.")
else
  disp("Rest pose reproduciton : FAILURE.")
endif


figure()
%{
subplot(1,2,1)
hold on
surf1 = trisurf([P,Q,R], V(:,1), V(:,2), V(:,3), sum(V_diff.^2,2).^(0.5));
colorbar("EastOutside")
plot3(H(:,1), H(:,2), H(:,3), 'go', "linewidth", 2, "markersize", 10)
axis square
axis equal
view(3)

subplot(1,2,2)
%}
hold on
surf2 = trisurf([P,Q,R], V_reconstruct(:,1), V_reconstruct(:,2), V_reconstruct(:,3), sum(V_diff.^2,2).^(0.5));
colorbar("EastOutside")
plot3(H(:,1), H(:,2), H(:,3), 'go', "linewidth", 2, "markersize", 10)
axis square
axis equal
view(3)
add_shadow();
title("Rest pose reproduction test")

%linkprop([surf1, surf2], 'view')

disp("Displaying result (see fig. 2)")



% Deformation test

disp("")
disp("Visualizing deformation (see fig. 3)")

m_deform = m;
deform_indices = 1:m_deform;
deform_values = 2*(rand(m_deform, 3)-0.5*ones(m, 3));



H_deform = H;
for i=1:m_deform
  H_deform(deform_indices(i), :) = H(deform_indices(i), :) + deform_values(i, :);
endfor

V_deform = W * H_deform;

export_tetra_mesh(V_deform, F, "output/test.vtk")


figure()
hold on

trisurf([P,Q,R], V_deform(:,1), V_deform(:,2), V_deform(:,3), sum((V-V_deform).^2,2).^(0.5))
plot3(H(:,1), H(:,2), H(:,3), 'go', "linewidth", 2, "markersize", 10)
plot3(H_deform(:,1), H_deform(:,2), H_deform(:,3), 'bo', "linewidth", 2, "markersize", 10)
plot3([H(:, 1)'; H_deform(:, 1)'], [H(:, 2)'; H_deform(:, 2)'], [H(:, 3)'; H_deform(:, 3)'], 'k', "linewidth", 2)
axis square
axis equal
view(3)
title("Deformation test")

