clear all
close all




V = gingerbread_LD_contour();
n = size(V,1);

N=40;
res = [N,N];

minX = min(V(:,1));
maxX = max(V(:,1));
minY = min(V(:,2));
maxY = max(V(:,2));

V = (V - kron([minX, minY], ones(n,1))) ./ kron([maxX, maxY]-[minX, minY], ones(n,1));
V = V .* kron(res-1, ones(n,1));

object = poly2mask(V(:,1), V(:,2), res(1), res(2));
field = rand(res) .* object;
new_field = diffuseOutside(object, field);



figure()
subplot(1,2,1)
imagesc(field)
axis image xy
colorbar

subplot(1,2,2)
imagesc(new_field)
axis image xy
colorbar
