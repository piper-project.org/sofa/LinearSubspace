function [V, F, h] = square_2D()

  V = [
    0 0
    0 1
    1 0
    1 1];

  F = [
    1 2 3
    3 2 4];
  
  h = [1 2 3];

end
