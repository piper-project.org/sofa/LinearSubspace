function [V, F, h] = tetrahedron()

  V = [
   1  0  1
  -1  0  1
   0 -1 -1
   0  1 -1
   ];


  F = [
  1 2 3 4
  ];
  
  
  h = [1, 2, 3, 4];

end
