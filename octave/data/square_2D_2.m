function [V, F, h] = square_2D_2()

  V = [
    0 0
    0 1
    1 0
    1 1
    0.5 0.5];

  F = [
    1 2 5
    2 4 5
    4 3 5
    3 1 5];
  
  h = [1 2 3];

end
