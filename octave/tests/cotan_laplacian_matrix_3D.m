function L = cotan_laplacian_matrix_3D(V,F)
  
  n = size(V,1);
  f = size(F,1);
  
  %  C = cotangent(V,F);
  %  L = sparse(F(:,[2 3 1 4 4 4]),...
  %             F(:,[3 1 2 1 2 3]),...
  %             C,...
  %             n,n);
  %  L = L+L';
  %  L = L - diag(sum(L,2));
  
  C = sym('c', [f, 6]);
  
  L = sym('l', [n, n]);
  L = 0*L;
  
  e0 = [2 3 1 4 4 4];
  e1 = [3 1 2 1 2 3];
  
  for i=1:f
    for j=1:6
      L(F(i, e0(j)), F(i, e1(j))) = L(F(i, e0(j)), F(i, e1(j))) + C(i, j);
    end
  end
  
  L = L+transpose(L);
  L = L - diag(sum(L));
  
end