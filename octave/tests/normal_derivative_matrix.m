function N = normal_derivative_matrix(V,F)
  
  n = size(V,1);
  
%  C = cotangent(V,F);
  C = sym('c', size(F));
  
  [~,on_b] = on_boundary(F);
  [on_b_f,on_b_c] = find(on_b);
  
%  P = F(sub2ind(size(F),on_b_f,mod(on_b_c+1,3)+1));
%  I = F(sub2ind(size(F),on_b_f,mod(on_b_c,3)+1));
%  Q = F(sub2ind(size(F),on_b_f,on_b_c));
%  
%  CP = C(sub2ind(size(F),on_b_f,mod(on_b_c+1,3)+1));
%  CI = C(sub2ind(size(F),on_b_f,mod(on_b_c,3)+1));
%  N = sparse( ...
%    [  P  P   P  P   I  I   I  I], ...
%    [  Q  P   Q  I   Q  I   Q  P], ...
%    [-CI CI -CP CP -CP CP -CI CI], ...
%    n, n);

  reverse_edge = [
    0, 3, 2; 
    3, 0, 1; 
    2, 1, 0];

%  N=zeros(n, n);
  N = sym('n', [n, n]);
  N(:) = 0;


  nb_tri_on_b = size(on_b_f, 1);
  for i=1:nb_tri_on_b

    % vertices on boundary subscript
    p_i = mod(on_b_c(i)+1,3)+1;
    q_i = mod(on_b_c(i)+0,3)+1;
    
    % vertices on boundary indices
    p = F(on_b_f(i), p_i);
    q = F(on_b_f(i), q_i);
    
    % opposite vertex
    s_i = on_b_c(i);
    s   = F(on_b_f(i), s_i);
    
    % collecting weights
    Cp = C(on_b_f(i), reverse_edge(p_i, s_i));
    Cq = C(on_b_f(i), reverse_edge(q_i, s_i));
    
    % setting weights
    N(p, p) = N(p, p) + Cp;
    N(p, s) = N(p, s) - Cp;
    
    N(p, q) = N(p, q) + Cq;
    N(p, s) = N(p, s) - Cq;
    
    N(q, p) = N(q, p) + Cp;
    N(q, s) = N(q, s) - Cp;
    
    N(q, q) = N(q, q) + Cq;
    N(q, s) = N(q, s) - Cq;
    
  endfor
  
end