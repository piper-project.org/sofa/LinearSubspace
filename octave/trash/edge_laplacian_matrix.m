function A = edge_laplacian_matrix(E)
  
  n = max(E(:));
  k = length(E);
  A = sparse(k, k);
  
  for i=1:(k-1)
    for j=(i+1):k
      if E(i, 1) == E(j,1) || E(i, 1) == E(j,2) || E(i, 2) == E(j,1) || E(i, 2) == E(j,2)
        A(i,j) = 1;
        A(j,i) = -1;
      endif
    endfor
  endfor

endfunction
