
F = [
   46   47
   35   36
   28   29
    9   17
   29   30
    8    9
   13   14
   21   27
   40   41
   20   25
   28   34
   50   51
    1    2
   39   40
   31   32
   14   15
    1    7
   11   13
   41   45
    4    5
   51   52
   22   23
   12   37
   21   22
   38   39
   31   33
   25   26
    4    6
   15   16
   11   12
   10   30
   19   24
   48   49
   36   42
   47   48
   32   35
   17   18
   33   34
   26   27
    5    8
   18   19
   37   38
    6    7
   42   43
   44   49
   16   20
    3   10
   46   52
    2    3
   43   44
   23   24
   45   50];

f = size(F,1);

seq=zeros(f,1);
mark=ones(f,1);

seq(1) = F(1,1);
seq(2) = F(1,2);
mark(1) = 0;

for i=3:size(F,1)
  next_i = find(F.*(mark*[1,1]) - seq(i-1) == 0)(1);
  next_j = mod(next_i-1 + size(F,1), 2*size(F,1))+1;
  seq(i) = F(next_j);
  mark(min(next_i, next_j)) = 0;
end